from patapon.utils import *
import numpy as np
from pathlib import Path


def test_float2str():
    assert float2str(1.) == '1.000000e+00_F'


def test_load_kernel():
    parameters = {'nx': 256,
                  'ny': 256,
                  'dx': 0.00390625,
                  'dy': 0.00390625,
                  'dt': 0.0005524271728019902,
                  'm': 2,
                  'wbord': 0.0,
                  'vx': 1.0,
                  'vy': 1.0}

    for np_type, precision in (np.float32, "single"), (np.float64, "double"):
        np_real, source = load_kernel('define.cl', parameters,
                                      precision=precision,
                                      module_file=__file__)
        assert np_real == np_type
        assert source == open(Path(__file__).parent /
                              f'define_{precision}.cl').read()


def test_get_ite_title():
    assert get_ite_title(2556, 2.000782778864909, 6.77991075300001) \
        == "ite = 2556, t = 2.000783, elapsed (s) = 6.779911"
