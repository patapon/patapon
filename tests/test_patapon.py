import os
import subprocess
from patapon.patapon import parse_args
from patapon.leapfrog import wave
from patapon.acoustic import m1_cl, kinetic_cl
from patapon.transport import transport_cl
from patapon.mhd import (lbm_cl_light, mhd_lbm, vortex_lbm,
                         profile_lbm_cl_light)


import matplotlib

matplotlib.use('Agg')  # Prevent from opening a window


def test_parse_args():
    args = parse_args(['wave'])
    assert args.problem == 'wave'
    assert args.no_display is False
    assert args.nx == 256
    assert args.ny == 256
    assert args.Lx == 1.0
    assert args.Ly == 1.0
    assert args.Tmax == 1.0
    assert args.precision == 'single'
    assert args.print_source is False
    assert args.savekineticdata is False
    assert args.savemacrodata is False
    assert args.animate is False

    args = parse_args(['transport', '-n', '128'])
    assert args.problem == 'transport'
    assert args.nx == 128
    assert args.ny == 128

    args = parse_args(['wave', '-L', '2.'])
    assert args.Lx == 2.
    assert args.Ly == 2.


def capture(cmd):
    """Capture command and return stdout, stderr and exit code"""
    proc = subprocess.Popen(cmd,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            text=True,
                            env=os.environ)
    try:
        output, error = proc.communicate()
    except subprocess.TimeoutExpired:
        proc.terminate()
        output, error = proc.communicate()
    return output, error, proc.returncode


# def test_cmd_wave():
#     command = ["patapon", "wave", "-nd"]
#     _, _, exitcode = capture(command)
#     assert exitcode == 0


# def test_cmd_acoustic_m1():
#     command = ["patapon", "acoustic_m1", "-nd"]
#     _, err, exitcode = capture(command)
#     print(err)
#     assert exitcode == 0


def test_wave():
    args = parse_args(['wave', '-nd'])
    wave.main(**vars(args))


def test_acoustic():
    args = parse_args(['acoustic_m1'])
    m1_cl.main(**vars(args))


def test_acoustic_kinetic():
    args = parse_args(['acoustic_kinetic'])
    kinetic_cl.main(**vars(args))


def test_mhd_lbm_cl_light():
    args = parse_args(['mhd_lbm_cl_light'])
    lbm_cl_light.main(**vars(args))


def test_mhd_lbm():
    args = parse_args(['mhd_lbm'])
    mhd_lbm.main(**vars(args))


def test_mhd_vortex_lbm():
    args = parse_args(['mhd_vortex_lbm'])
    vortex_lbm.main(**vars(args))


def test_mhd_profile():
    profile_lbm_cl_light.main()


def test_transport():
    args = parse_args(['transport'])
    transport_cl.main(**vars(args))
