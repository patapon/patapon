#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Resolution of a transport equation by the finite volume method on regular grid
Regular python implementation compared to a pyopencl version
"""

import pyopencl as cl
import numpy as np
import matplotlib.pyplot as plt
import time
from patapon.utils import load_kernel, get_ite_title

# Definition of default values

# number of conservative variables
_m = 2

# boundary value
_wbord = 0.

# grid size
_nx = 256
_ny = 256
Lx = 1.
Ly = 1.

_dx = Lx / _nx
_dy = Ly / _ny

# transport velocity
vel = np.array([1., 1.])

vmax = np.sqrt(vel[0]**2+vel[1]**2)

# time stepping
_Tmax = 0.5 / vmax
cfl = 0.8
_dt = cfl * (_dx * _dy) / (2 * _dx + 2 * _dy) / vmax


def fluxnum(wL, wR, vnorm):
    vdotn = vel[0] * vnorm[0] + vel[1] * vnorm[1]
    vnp = 0
    if vdotn > 0:
        vnp = vdotn
    vnm = vdotn - vnp
    flux = vnm * wR + vnp * wL
    return flux


def exact_sol(xy, t):
    x = xy[0] - t * vel[0] - 0.5
    y = xy[1] - t * vel[1] - 0.5
    d2 = x * x + y * y
    w = np.exp(-30*d2)
    return w


def solve_python(nx=_nx, ny=_ny, Tmax=_Tmax, dx=_dx, dy=_dy, dt=_dt, exact_sol=exact_sol, wbord=_wbord,
                 fluxnum=fluxnum, **kwargs):
    t = 0.
    wn = np.full((nx, ny), wbord, dtype='float32')
    print("\nstart Python computations...")
    for i in range(1, nx-1):
        for j in range(1, ny-1):
            xy = [i*dx+dx/2, j*dy+dy/2]
            wn[i, j] = exact_sol(xy, t)

    wnp1 = np.zeros((nx, ny), dtype='float32')
    np.copyto(wnp1, wn)
    ite = 0
    elapsed = 0
    while t < Tmax:
        t += dt
        ite += 1
        start = time.time()
        # Pure Python version
        # for i in range(1, nx-1):
        #     for j in range(1, ny-1):
        #         wnp1[i, j] -= dt / dx * fluxnum(wn[i, j], wn[i+1, j], np.array([1, 0]))
        #         wnp1[i, j] -= dt / dx * fluxnum(wn[i, j], wn[i-1, j], np.array([-1, 0]))
        #         wnp1[i, j] -= dt / dy * fluxnum(wn[i, j], wn[i, j+1], np.array([0, 1]))
        #         wnp1[i, j] -= dt / dy * fluxnum(wn[i, j], wn[i, j-1], np.array([0, -1]))

        # Numpy version
        wnp1[1:-1, 1:-1] -= dt / dx * fluxnum(wn[1:-1, 1:-1], wn[2:, 1:-1], np.array([1, 0])) \
            + dt / dx * fluxnum(wn[1:-1, 1:-1], wn[:-2, 1:-1], np.array([-1, 0])) \
            + dt / dy * fluxnum(wn[1:-1, 1:-1], wn[1:-1, 2:], np.array([0, 1])) \
            + dt / dy * \
            fluxnum(wn[1:-1, 1:-1], wn[1:-1, :-2], np.array([0, -1]))

        np.copyto(wn, wnp1)
        end = time.time()
        elapsed += end - start
        print(get_ite_title(ite, t, elapsed), end='\r')
    return wn


def solve_ocl(m=_m, nx=_nx, ny=_ny, Tmax=_Tmax, dx=_dx, dy=_dy, dt=_dt,
              print_source=False,
              wbord=_wbord, animate=True, **kwargs):

    # load and adjust C program
    parameters = {'nx': nx,
                  'ny': ny,
                  'dx': dx,
                  'dy': dy,
                  'dt': dt,
                  'm': m,
                  'wbord': wbord,
                  'vx': vel[0],
                  'vy': vel[1],
                  }
    np_real, source = load_kernel("transport_kernels.cl", parameters,
                                  precision="single",
                                  print_source=print_source,
                                  module_file=__file__)

    # OpenCL init
    ctx = cl.create_some_context()
    mf = cl.mem_flags

    # compile OpenCL C program
    prg = cl.Program(ctx, source).build(
        options="-cl-strict-aliasing -cl-fast-relaxed-math")

    # create OpenCL buffers
    wn_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=(
        m * nx * ny * np.dtype('float32').itemsize))
    wnp1_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=(
        m * nx * ny * np.dtype('float32').itemsize))

    # create a queue (for submitting opencl operations)
    queue = cl.CommandQueue(
        ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # init data
    event = prg.init_sol(queue, (nx * ny, ), (32, ), wn_gpu)
    event.wait()

    # number of animation frames
    nbplots = 10
    itermax = int(np.floor(Tmax / dt))
    iterplot = int(itermax / nbplots)

    # time loop
    t = 0
    ite = 0
    elapsed = 0.
    wn_cpu = np.empty((m * nx * ny, ), dtype=np_real)

    print("start OpenCL computations...")
    while t < Tmax:
        t += dt
        ite += 1
        #event = prg.time_step(queue, (nx * ny, ), (32, ), wn_gpu, wnp1_gpu)
        event = prg.time_step(queue, (nx * ny, ), (64, ), wn_gpu, wnp1_gpu)
        #event = prg.time_step(queue, (nx * ny, ), (32, ), wn_gpu, wnp1_gpu, wait_for = [event])
        event.wait()
        elapsed += 1e-9 * (event.profile.end - event.profile.start)
        # exchange buffer references for avoiding a copy
        wn_gpu, wnp1_gpu = wnp1_gpu, wn_gpu
        ite_title = "ite = {}, t = {:f}, elapsed (s) = {:f}".format(
            ite, t, elapsed)
        print(ite_title, end='\r')
        if ite % iterplot == 0 and animate:
            cl.enqueue_copy(queue, wn_cpu, wn_gpu).wait()
            wplot = np.reshape(wn_cpu, (m, nx, ny))
            plt.clf()
            plt.imshow(wplot[1, :, :], vmin=0, vmax=1)
            plt.gca().invert_yaxis()
            plt.colorbar()
            plt.pause(0.01)

    # copy OpenCL data to CPU and return the results
    cl.enqueue_copy(queue, wn_cpu, wn_gpu).wait()

    wplot_gpu = np.reshape(wn_cpu, (m, nx, ny))
    return wplot_gpu


def main(**kwargs):

    # gpu solve
    wplot_gpu = solve_ocl(**kwargs)
    plt.clf()
    plt.imshow(wplot_gpu[1, :, :], vmin=0, vmax=1)
    plt.gca().invert_yaxis()
    plt.colorbar()
    plt.show()

    # cpu solve
    wplot_cpu = solve_python(**kwargs)
    plt.clf()
    plt.imshow(wplot_cpu, vmin=0, vmax=1)
    plt.gca().invert_yaxis()
    plt.colorbar()
    plt.show()

    # check difference
    plt.clf()
    plt.imshow(wplot_cpu[:, :] - wplot_gpu[1, :, :])
    plt.gca().invert_yaxis()
    plt.colorbar()
    plt.show()


if __name__ == '__main__':
    main()
