#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _DT _dt_
#define _M _m_

#define _NewtonIter 50
#define _NewtonTol 1e-6f
#define _M_2_PI 6.283186f
#define _M_PI 3.141593f

#define _MAX_B 90.0f

#define _VOL (_DX * _DY)

__constant int dir[4][2] = { {1, 0}, {-1, 0}, {0, 1}, {0, -1}};

__constant float ds[4] = { _DY, _DY, _DX, _DX };

#define _NGAUSS 32
__constant float x32[32] = { 
-0.997263861849481563545,
-0.9856115115452683354002,
-0.9647622555875064307738,
-0.9349060759377396891709,
-0.8963211557660521239653,
-0.8493676137325699701337,
-0.794483795967942406963,
-0.7321821187402896803874,
-0.6630442669302152009751,
-0.5877157572407623290408,
-0.5068999089322293900238,
-0.421351276130635345364,
-0.3318686022821276497799,
-0.2392873622521370745446,
-0.1444719615827964934852,
-0.0483076656877383162348,
0.048307665687738316235,
0.1444719615827964934852,
0.2392873622521370745446,
0.33186860228212764978,
0.4213512761306353453641,
0.5068999089322293900238,
0.5877157572407623290408,
0.6630442669302152009751,
0.7321821187402896803874,
0.7944837959679424069631,
0.8493676137325699701337,
0.8963211557660521239653,
0.9349060759377396891709,
0.9647622555875064307738,
0.9856115115452683354002,
0.997263861849481563545,
};

__constant float w32[32] = {
0.0070186100094700966004,
0.0162743947309056706052,
0.0253920653092620594558,
0.0342738629130214331027,
0.0428358980222266806569,
0.050998059262376176196,
0.0586840934785355471453,
0.065822222776361846838,
0.072345794108848506225,
0.0781938957870703064717,
0.0833119242269467552222,
0.087652093004403811143,
0.091173878695763884713,
0.09384439908080456563918,
0.0956387200792748594191,
0.0965400885147278005668,
0.0965400885147278005668,
0.0956387200792748594191,
0.0938443990808045656392,
0.091173878695763884713,
0.0876520930044038111428,
0.083311924226946755222,
0.078193895787070306472,
0.072345794108848506225,
0.065822222776361846838,
0.0586840934785355471453,
0.0509980592623761761962,
0.0428358980222266806569,
0.0342738629130214331027,
0.0253920653092620594558,
0.0162743947309056706052,
0.0070186100094700966004
};
    


/*Gauss-Legendre Quadrature */
/* n = 32 */
// #define _NGAUSS 16
// __constant float x32[16] = {0.0483076656877383162348126,
// 0.1444719615827964934851864,0.2392873622521370745446032,
// 0.3318686022821276497799168,0.4213512761306353453641194,
// 0.5068999089322293900237475,0.5877157572407623290407455,
// 0.6630442669302152009751152,0.7321821187402896803874267,
// 0.7944837959679424069630973,0.8493676137325699701336930,
// 0.8963211557660521239653072,0.9349060759377396891709191,
// 0.9647622555875064307738119,0.9856115115452683354001750,
// 0.9972638618494815635449811};

// __constant float w32[16] = {0.0965400885147278005667648,
// 0.0956387200792748594190820,0.0938443990808045656391802,
// 0.0911738786957638847128686,0.0876520930044038111427715,
// 0.0833119242269467552221991,0.0781938957870703064717409,
// 0.0723457941088485062253994,0.0658222227763618468376501,
// 0.0586840934785355471452836,0.0509980592623761761961632,
// 0.0428358980222266806568786,0.0342738629130214331026877,
// 0.0253920653092620594557526,0.0162743947309056706051706,
// 0.0070186100094700966004071};

float f_vx_pdf(float x, float a, float *b){
    return a * cos(x) * exp( b[0] * cos(x) + b[1] * sin(x) );
}
 
float f_vy_pdf(float x, float a, float *b){
    return a * sin(x) * exp( b[0] * cos(x) + b[1] * sin(x) );
} 

float I_gauss_legendre32(float a , float *b, float xmin, float xmax, float *Ix, float * Iy)
/* Gauss-Legendre quadrature order 32 */
{
    float s1, s2;
	float A;
	float B;
	float Ax;

    s1 = 0.0f;
    s2 = 0.0f;
	A = 0.5f * (xmax - xmin);
	B = 0.5f * (xmax + xmin);

    for (unsigned int i = 0; i < _NGAUSS; i++) {
			Ax = A * x32[i];
			s1 += w32[i] * (f_vx_pdf(B + Ax, a, b) + f_vx_pdf(B - Ax, a, b));			
			s2 += w32[i] * (f_vy_pdf(B + Ax, a, b) + f_vy_pdf(B - Ax, a, b));			
    }
	
    *Ix = A * s1;
    *Iy = A * s2;
}


__kernel void extract_rcp( __global float *wn,	__global float *w_rcp){
    
    
    int id = get_global_id(0);

	int i = id % _NX;
	int j = id / _NX;
	float xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};


	int ngrid = _NX * _NY;


    float xm = 0.8;
	float ym = 0.5;
    float r = 0.05;

	float d = sqrt((xy[0] - xm) * (xy[0] - xm) + (xy[1] - ym) * (xy[1] - ym));


    int iv = 0;
    int imem = i + j * _NX + iv * ngrid;
    if (d < r){
        w_rcp[imem] = wn[imem];
    } else{
        w_rcp[imem] = 0;
    }
}




/* Modified Bessel functions of the first kind */
float bessi0(float x)
/* Compute BesselI0(x) for any real x. */
{
    float ax;
    float ans;
    float y; 

    if ((ax = fabs(x)) < 3.75) {
        y = x / 3.75;
        y *= y;
        ans = 1.0 + y * (3.5156229 + y * (3.0899424 + y * (1.2067492 
            + y * (0.2659732 + y * (0.360768e-1 + y * 0.45813e-2)))));
    } else {
        y = 3.75 / ax;
        ans = (exp(ax) / sqrt(ax)) * (0.39894228 + y * (0.1328592e-1 
            + y * (0.225319e-2 + y * (-0.157565e-2 + y * (0.916281e-2 
            + y * (-0.2057706e-1 + y * (0.2635537e-1 + y * (-0.1647633e-1 
            + y * 0.392377e-2))))))));
    }
    return ans;
}


float bessi1(float x)
/* Compute BessselI1(x) for any real x. */
{
    float ax;
    float ans;
    float y;
    if ((ax = fabs(x)) < 3.75) {
        y = x / 3.75;
        y *= y;
        ans = ax * (0.5 + y * (0.87890594 + y * (0.51498869 
            + y * (0.15084934 + y * (0.2658733e-1 
            + y * (0.301532e-2 + y * 0.32411e-3))))));
    } else {
        y = 3.75 / ax;
        
        ans = 0.2282967e-1 + y * (-0.2895312e-1 
            + y * (0.1787654e-1 - y*0.420059e-2));
            
        ans = 0.39894228 + y * (-0.3988024e-1 + y * (-0.362018e-2
            + y * (0.163801e-2 + y * (-0.1031555e-1 + y * ans))));
        ans *= (exp(ax) / sqrt(ax));
    }
    return x < 0.0 ? -ans : ans;
}


float bi1_over_bi0(float x)
/* Compute (BesselI1(x)/BesselI0(x)) for any real x > 0 */
{
    float t1 = x * x;
    float t2 = t1 * t1;
    float t3 = t2 * t2;
    float t5 = t1 * x;
    float t6 = t2 * t5;
    float t8 = t2 * t1;
    float t10 = t2 * x;
    
    if (x < 3.75) {
        float t4 = t3 * t2;
        float t7 = t3 * t5;
        float t9 = t3 * t1;
        float t11 = t3 * x;
        float t24 = 0.8440273e9f + 0.7074629473e-1f * t4 
        + t7 * 0.251341329011722764e-15f + t9 * 0.925565614048674412e1f 
        + t11 * 0.482575351702507707e-13f + t3 * 0.114765423795251832e4f 
        + t6 * 0.617696450179209865e-11f + t8 * 0.915676963748728012e5f 
        + t10 * 0.790651456229388627e-9f + t2 * 0.439601434421872627e7f 
        + t5 * 0.168672310662269574e-7f + t1 * 0.105503372985039026e9f 
        + x * 0.101203386397361744e-6f;

        float t36 = 0.1688055e10f + t4 + t7 * 0.355271367880050093e-14f 
        + t9 * 0.110739309745002402e3f + t11 * 0.341060513164848089e-12f 
        + t3 * 0.114808546059141827e5f + t6 * 0.145519152283668518e-10f 
        + t8 * 0.732513939663389931e6f + t10 * (-0.931322574615478516e-9f) 
        + t2 * 0.263761736446225569e8f + t1 * 0.422013388659591973e9f 
        + x * (-0.298023223876953125e-7f);
        return (x * t24 / t36);
        
    } else {
        float t26 = (-0.411765893050994634e3f + 0.1000000000e1f * t3 
        + t6 * (-0.374868514813721632e0f) + t8 * (-0.127609390636661324e0f) 
        + t10 * 0.216521130776113679e0f + t2 * (-0.511337205973752695e1f) 
        + t5 * 0.424371204275464251e2f + t1 * (-0.201824047528880271e3f) 
        + x * 0.467296226160836625e3f) / (0.384630410945663925e3f + t3 
        + t6 * 0.124885735349985261e0f + t8 * 0.794237311222061315e-1f 
        + t10 * (-0.208278044508087845e0f) + t2 * 0.454196398787312461e1f 
        + t5 * (-0.382498377208852887e2f) + t1 * 0.183715863583072633e3f 
        + x * (-0.430694464873465051e3f));
        return t26;
    }
}

float bi1_over_bi0_der(float x)
/* Compute (Diff(BesselI1(x)/BesselI0(x),x)) for any real x > 0*/
/* Use of  half_powr (positivity checked) */
{   
    float t1 = x * x;
    float t2 = t1 * t1;
    float t3 = t2 * t2;
    float t4 = t3 * t3;
    float t10 = t1 * t2;
    if (x < 3.75) { 
        float t11 = t3 * t1;
        float t27 = 0.5e0f + 0.415539843e-8f * t3 * t2 
        + 0.520855784e-2f * t2 + 0.271197469e-3f * t10 
        + 0.949687181e-5f * t3 + 0.2393965e-6f * t11 
        + 0.62499798e-1f * t1 + 0.701969197e-10f * t3 * t10 
        + 0.138983486e-11f * t4 - 0.1089247e-14f * t4 * t1 
        - 0.6880717e-15f * t4 * t2 - 0.1532295e-16f * t4 * t10 
        - 0.1334793e-18f * t4 * t3 - 0.1756445e-20f * t11 * t4;
        float t30 = half_powr(0.7903894e2f 
        + t1 + x * 0.120869550336504865e2f, 0.2e1f);
        float t35 = half_powr(0.4162678e2f 
        + t1 + x * 0.415247505642657355e1f, 0.2e1f);
        float t39 = half_powr(0.2696162e2f 
        + t1, 0.2e1f);
        float t42 = half_powr(0.5783793e1f + t1, 0.2e1f);
        float t47 = half_powr(0.4162678e2f 
        + t1 + x * (-0.415247505642656911e1f), 0.2e1f);
        float t51 = half_powr(0.7903894e2f 
        + t1 + x * (-0.120869550336504883e2f), 0.2e1f);
        return (0.2849528207e19f * t27 / t30 / t35 / t39 / t42 / t47 / t51);
    } else {
        float t6 = t1 * x;
        float t7 = t2 * t6;
        float t13 = t2 * x;
        float t32 = 0.1583777e6f - 0.4915003e-3f * t4 
        + t3 * t7 * 0.539119066304029837e0f 
        + t3 * t10 * (-0.830432664388546793e0f) 
        + t13 * t3 * 0.194484750177690238e2f 
        + t3 * t2 * (-0.163574545797908854e3f) 
        + t3 * t6 * 0.790865902029719564e3f 
        + t1 * t3 * (-0.189683162827931301e4f) 
        + t3 * x * 0.185245170155044616e4f 
        + t3 * (-0.177251288735982513e3f) 
        + t7 * (-0.116655671909182888e4f) 
        + t10 * 0.771173872709972238e4f 
        + t13 * (-0.344218181830821995e5f) 
        + t2 * 0.112963337573462239e6f 
        + t6 * (-0.262597958534803591e6f) 
        + t1 * 0.408054408662734553e6f + x * (-0.378692228024255484e6f);
        float t35 = half_powr(0.7791393e1f + t1 
        + x * 0.487174864930003437e1f, 0.2e1f);
        float t40 = half_powr(0.5513739e1f + t1 
        + x * 0.589435886745173332e0f, 0.2e1f);
        float t45 = half_powr(0.3542920e1f + t1 
        + x * (-0.228892392327561245e1f), 0.2e1f);
        float t49 = half_powr(0.2527091e1f + t1 
        + x * (-0.304737487741960988e1f), 0.2e1f);
        return (t32 / t35 / t40 / t45 / t49 / x);
    }
}


bool m1_get_b_newton(const float r1, float *bn)
/* Search Lagrange multiplier b by Newton-Raphson method */
{	
    float xk;
    float xkp1;
    float b1;
    float b0;
    float der;
    float f;
    float t0;

    if( r1 == 0) { 
        *bn = 0.0f;
        return true;
    }
    /* Initial Guess */
    if( r1 > 0) xk = r1 / (1.0f - 0.9f * r1 * r1);

    xkp1 = xk;
    
    for(unsigned int i = 0; i < _NewtonIter; i++) {
        
        /* Crop for float precision */
        if (xk > _MAX_B){
            *bn = _MAX_B;
            return true;
        }
        /* Compute function and derivative */
        b0 = bessi0(xk);
        b1 = bessi1(xk);
        // f = (b1 / b0 - r1);
        f = bi1_over_bi0(xk) -r1;
        
        // t0 = b0 * b0 * xk;
        // der = (-b1 * b1 * xk + t0 - b1 * b0) / t0;
        der = bi1_over_bi0_der(xk);
        xkp1 = xk - f / der;
        
        /* Minimum found */
        // || fabs(f) < _NewtonTol
        if (fabs(xkp1 - xk) < _NewtonTol * fabs(xkp1) || fabs(f) < _NewtonTol ){
            *bn = xkp1;
            return true;
        }
        
        xk = xkp1;
    }
    
    // if( m1_get_b_bissec(r1, xkp1, _MAX_B, bn) ){
        // printf("[NEWTON] bn = %f \n",*bn);
        // return true;
    // }
    
    // *bn = 0.0f;

    return false;
};


float get_chi_interp( float x )

{
    float x2 = x * x;
    float x4 = x2 * x2;
    float x6 = x4 * x2;
    float chi6 = -0.278845727790037 * x2 + 0.0950024263663781 * x4 -0.315901649950613 * x6 + 0.5;
    if(x > 1-1e-8) return 0;
    return chi6;  
}



void gaussian_2D(float *x, float t, float *w)
{
    float center[2] = {0.5, 0.5};
    float sig[2] = {0.10, 0.10};
    float A = 1.0 /( 6.283185308 * sig[0] * sig[1] );
    float X = (x[0] - center[0]) * (x[0] - center[0]) / (sig[0] * sig[0]);
    float Y = (x[1] - center[1]) * (x[1] - center[1]) / (sig[1] * sig[1]);

    float r = 0.1;
    float boost = 2;
    w[0] = boost * A * exp(-0.5 * (X + Y));
    w[1] = +0.9*boost * A * exp(-0.5 * (X + Y));;
    w[2] = 0;
}

void circle_2D(float *x, float t, float *w)
{
     float xm = 0.5f;
     float ym = 0.5f;
     float r = 0.05f;

    w[0] = 0.f;
    w[1] = 0.f;
    w[2] = 0.f;
        
    float d = sqrt((x[0] - xm) * (x[0] - xm) + (x[1] - ym) * (x[1] - ym));
        
    if (d < r){
        w[0] = 10.f;
        w[1] = 0.f;
        w[2] = 0.f;
    }
}





float get_R1(float *w, float tol)
{
    float I_norm = sqrt(w[1] * w[1] + w[2] * w[2]);
    float R1;
    
    if(I_norm < tol) {
        R1 = 0.f;
    } else if ( w[0] < tol ) {
      R1 = 0.f; 
    } else {
      R1 = I_norm / w[0]; 
    }
    return R1;
}


float get_chi(float R1, float b, float tol)
{
    float chi;
    
    if(R1 < tol) {
        chi = 0.5f;
    } else if (R1 > 1 - tol) {
        chi = 0.0f;
    } else {
        chi = R1 / b;
    }
    
    return chi;
    
}

void fluxphy(float *wn, float* n, float *flux){

    float w =  wn[0];
    float Ix = wn[1];
    float Iy = wn[2];
    float R1, b, chi, I2;
    float P11, P12, P21, P22;
    float IxIx, IxIy, IyIy;
    
    float tol = 1e-8;

    R1 = get_R1(wn, tol);
    // m1_get_b_newton(R1, &b);
    // chi = get_chi(R1, b, tol);
    chi = get_chi_interp(R1);

    I2 = Ix * Ix + Iy * Iy;
    
    if (I2 < tol) {
      IxIx = 0.f;  
      IxIy = 0.f;  
      IyIy = 0.f;  
    } else {
      IxIx = Ix * Ix / I2;
      IxIy = Ix * Iy / I2;
      IyIy = Iy * Iy / I2;
    }
    
    /* Termes diag de P */
    P11 = w * chi + w * (1.f - 2.f * chi) * IxIx; 
    P22 = w * chi + w * (1.f - 2.f * chi) * IyIy;
    
    /* Termes extra-diag de P*/
    P12 = w * (1.f - 2.f * chi) * IxIy;
    P21 = P12;

    flux[0] = Ix  * n[0] + Iy  * n[1];
    flux[1] = P11 * n[0] + P12 * n[1];
    flux[2] = P21 * n[0] + P22 * n[1];
}


void fluxnum_rus(float *wL, float *wR, float* vnorm, float* flux){
    float fL[_M];
    float fR[_M];
    float lambda = 1;

    fluxphy(wL, vnorm, fL);
    fluxphy(wR, vnorm, fR);


    for(int iv = 0; iv < _M; iv++){
        flux[iv] = 0.5f * (fL[iv] + fR[iv]) - 0.5f * lambda * (wR[iv] - wL[iv]);
    }
}


// void exact_sol(float* xy, float t, float* w){

	// for (unsigned int iv = 0; iv < _M ; iv++) {
		// w[iv] = 1;
	// }

// }

__kernel void init_sol(__global  float *wn){

	int id = get_global_id(0);

	int i = id % _NX;
	int j = id / _NX;

	int ngrid = _NX * _NY;

	float wnow[_M];

	float t = 0;
	float xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};

    // gaussian_2D(xy, t, wnow);
    circle_2D(xy, t, wnow);
    
	for(int iv = 0; iv < _M; iv++){
		int imem = i + j * _NX + iv * ngrid;
        wn[imem] = wnow[iv] ;
	}
}




// void source(float *x, float t, float s[_M]){

        // float xm = 1;
        // xm /= 2;

        // float ym = 1;
        // ym /= 2;

        // float r = 1;
        // r /= 20;

        // s[0] = 0;
        // s[1] = 0;
        // s[2] = 0;
        
        // float d = sqrt((x[0] - xm) * (x[0] - xm) + (x[1] - ym) * (x[1] - ym));
        
        // if (d < r){
            // s[0] = 10;
            // s[1] = 0.001;
            // s[2] = 0.001;
        // }
   
// }



void boundaryfluxnum(float *wL, float *wR, float* n, float* flux)
{
   
    float In;
    float abs = 1.f-0.05;
    // wR[0] = 0.f;
    // wR[1] = 0.f;
    // wR[2] = 0.f;
    

    
    // float w =  wL[0];
    // float Ix = wL[1];
    // float Iy = wL[2];
    // float R1, a, b, bx, by, chi, I2;
    // float IxIx, IxIy, IyIy;
    // float Inorm;
    // float tol = 1e-8;


    // Inorm = sqrt(Ix * Ix + Iy * Iy );
    // R1 = get_R1(wL, tol);
    // m1_get_b_newton(R1, &b);
    // a = w / (2 * _M_PI * bessi0(b));
    

    
    
    // if (Inorm < tol) {
     // bx = 0; 
     // by = 0;
    // } else {
     // bx = b * Ix / Inorm;   
     // by = b * Iy / Inorm;
    // }
    
    // float bn = bx * n[0] + by * n[1];
    // bx = bx - 2 * bn * n[0];
    // by = by - 2 * bn * n[1];
    
    // float b_vec[2] = { bx, by};
    // float Ix_int, Iy_int;
    // I_gauss_legendre32(a , b_vec, -_M_PI / 2, _M_PI /2, &Ix_int, &Iy_int);
    
    // if( wL[1] > tol ) {
    // printf("L(%f, %f), K(%f, %f)\n", wL[1], wL[2], Ix_int, Iy_int);
    // }
    In = wL[1] * n[0] + wL[2] * n[1];
    wR[0] = abs * wL[0];
    wR[1] = abs * (wL[1] - 2 * In * n[0]) ;
    wR[2] = abs * (wL[2] - 2 * In * n[1]);
    
    fluxnum_rus(wL, wR, n, flux);
}

__kernel void time_step(
		float tnow,
		__global float *wn,
		__global float *wnp1)
{

	int id = get_global_id(0);

	int i = id % _NX;
	int j = id / _NX;
	float xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};

	int ngrid = _NX * _NY;

	float wnow[_M];
	float wnext[_M];
	float s[_M];

	/* Load middle value */
	for (int iv = 0; iv < _M; iv++) {
		int imem = i + j * _NX + iv * ngrid;
		wnow[iv] = wn[imem];
		wnext[iv] = wnow[iv];
	}

	/* Only compute internal cells */
	if (i > 0 && i < _NX - 1 && j > 0 && j < _NY - 1) {
		
		float flux[_M];

		/*  Loop on all directions:
			idir = 0 (east)
			idir = 1 (west)
			idir = 2 (north)
			idir = 3 (south)
		*/
		
		for(int idir = 0; idir < 4; idir++){
			float vn[2];
			vn[0] = dir[idir][0];
			vn[1] = dir[idir][1];
			int iR = i + dir[idir][0];
			int jR = j + dir[idir][1];
			
			/* Load neighbour values */
			float wR[_M];
			for(int iv = 0; iv < _M; iv++){
				int imem = iv * ngrid + iR + jR * _NX;
				wR[iv] = wn[imem];
			}
			
			/* If we are on a boundary */
			if (iR == 0 || iR == _NX - 1 || jR == 0 || jR == _NY - 1){
                boundaryfluxnum(wnow, wR, vn, flux);

			} else {
                fluxnum_rus(wnow, wR, vn, flux);
            }
			// time evolution
			for(int iv = 0; iv < _M; iv++){
				wnext[iv] -= _DT * ds[idir] / _VOL * flux[iv];
				// source(xy, tnow, s);
				// wnext[iv] += s[iv] * _DT;
 
			}
            if(wnext[0] < 0){
                // if (iR == 0 || iR == _NX - 1 || jR == 0 || jR == _NY - 1){
                    // printf("Boundary vacum wnext[0] = %f\n",wnext[0]);
                // } else {
                    // printf("vacum wnext[0] = %f\n",wnext[0]);
                // }

                wnext[0] = 1e-4;
            }
        }
	}   // end test for internal cells

	// copy wnext into global memory
	// (including boundary cells)

	for(int iv = 0; iv < _M; iv++){
		int imem = iv * ngrid + i + j * _NX;
		wnp1[imem] = wnext[iv];
	}
}


// TODO ajouter les kernels pour calculer les grandeurs macro
