# -*- coding: utf-8 -*-

from patapon.utils import Figure, load_kernel, get_ite_title
import pyopencl as cl
import numpy as np
import matplotlib.pyplot as plt

# Default values

# number of conservative variables
_m = 3

# grid size
_nxy = 128
_nx = _nxy
_ny = _nxy
_Lx = 1.0
_Ly = 1.0

# time stepping
cfl = 0.9
_Tmax = 0.2


def solve_ocl(m=_m, nx=_nx, ny=_ny, Lx=_Lx, Ly=_Ly, Tmax=_Tmax, animate=True,
              precision="single", print_source=False, **kwargs):

    dx = _Lx / _nx
    dy = _Ly / _ny
    dt = cfl * (dx * dy) / (2 * dx + 2 * dy)

    # For plotting
    x = np.linspace(0.0, Lx, num=nx)
    y = np.linspace(0.0, Ly, num=ny)

    # load and adjust  C program
    parameters = {'nx': nx,
                  'ny': ny,
                  'dx': dx,
                  'dy': dy,
                  'dt': dt,
                  'm': m
                  }
    np_real, source = load_kernel("m1_kernels.cl", parameters, precision=precision,
                                  print_source=print_source,
                                  module_file=__file__)

    # OpenCL init
    ctx = cl.create_some_context()
    mf = cl.mem_flags

    # compile OpenCL C program
    prg = cl.Program(ctx, source).build(options="-cl-fast-relaxed-math")
    # prg = cl.Program(ctx, source).build(options = "")

    k_time_step = prg.time_step
    k_time_step.set_scalar_arg_dtypes([np_real, None, None])

    print(k_time_step)
    # create OpenCL buffers
    buffer_size = m * nx * ny * np.dtype(np_real).itemsize
    wn_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    wnp1_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    wn_rcp_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)

    # create a queue (for submitting opencl operations)
    queue = cl.CommandQueue(
        ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # init data
    event = prg.init_sol(queue, (nx * ny,), (32,), wn_gpu)
    event.wait()

    # number of animation frames
    nbplots = 60
    itemax = int(np.floor(Tmax / dt))
    iteplot = max(1, int(itemax / nbplots))

    # iteplot = 1
    # time loop
    t = 0
    ite = 10
    elapsed = 0.0
    wn_cpu = np.empty((m * nx * ny,), dtype=np_real)
    wn_rcp_cpu = np.empty((m * nx * ny,), dtype=np_real)
    t_val = []
    w_val = []

    if animate:
        fig = Figure(title=r"$n_x = {}, n_y = {}$".format(nx, ny), levels=64)

    print("start OpenCL computations...")
    while t < Tmax:
        t += dt

        event = k_time_step(queue, (nx * ny, ), (64, ), t, wn_gpu, wnp1_gpu)
        event.wait()
        elapsed += 1e-9 * (event.profile.end - event.profile.start)
        event = prg.extract_rcp(queue, (nx * ny,), (64,), wn_gpu, wn_rcp_gpu)
        event.wait()
        event = cl.enqueue_copy(queue, wn_rcp_cpu, wn_rcp_gpu)
        event.wait()
        wplot_rcp = np.reshape(wn_rcp_cpu, (m, nx, ny))

        # print(wplot_rcp[0, :, :])
        # np.savetxt('w.txt', x, delimiter=',')
        # exit()
        w = np.sum(wplot_rcp[0, :, :], dtype=np_real)
        # print(t, w)
        t_val.append(t)
        w_val.append(w)

        # exchange buffer references for avoiding a copy
        wn_gpu, wnp1_gpu = wnp1_gpu, wn_gpu
        ite_title = get_ite_title(ite, t, elapsed)
        print(ite_title, end="\r")
        if ite % iteplot == 0 and animate:
            cl.enqueue_copy(queue, wn_cpu, wn_gpu).wait()
            wplot = np.reshape(wn_cpu, (m, nx, ny))

            w = np.sum(wplot, axis=0, dtype=np_real)
            fig.update(x, y, w, suptitle=ite_title, cb=ite == 0)

        ite += 1

    # copy OpenCL data to CPU and return the results
    cl.enqueue_copy(queue, wn_cpu, wn_gpu).wait()

    wplot_gpu = np.reshape(wn_cpu, (m, nx, ny))
    # w = np.sum(wplot_gpu, axis=0, dtype=np_real)
    plt.plot(t_val, w_val)
    plt.show()
    return x, y, wplot_gpu, w


def main(**kwargs):

    # gpu solve
    x, y, wplot_gpu, w = solve_ocl(**kwargs)

    print("\nFinal plot")
    fig = Figure(title=r"$w$")
    ivplot = 0
    fig.update(x, y, wplot_gpu[ivplot, :, :], cb=True, show=True)


if __name__ == '__main__':
    main()
