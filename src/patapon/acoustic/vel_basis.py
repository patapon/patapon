
from __future__ import absolute_import, print_function
import pyopencl as cl
import numpy as np
import matplotlib.pyplot as plt






N = 4

#N velocity basis
vel_N = 2**N
dtetha = 2*np.pi/(vel_N)


vel = np.zeros((2**N, 2))
coord_x = []
coord_y = []

for i in range(0,vel_N):

  vel[i][0] = np.cos(i*dtetha)
  vel[i][1] = np.sin(i*dtetha)
  coord_x.append(np.cos(i*dtetha))
  coord_y.append(np.sin(i*dtetha))

vel[np.abs(vel)  < 1e-12 ] = 0.0 
print(vel)

# print("SORTED")
# vel.sort(key=lambda x: x[0])
# vel = vel[vel[:,0].argsort(kind='mergesort')]
# print(vel)


fig = plt.figure(figsize=(10,10),dpi=50)
ax = fig.add_subplot(111)
ax.set_xlim(-1.1,1.1)
ax.set_ylim(-1.1,1.1)
ax.scatter(vel[:,0],vel[:,1],s=200)


for i in range(2**N):
    ax.annotate(str(i), (vel[i][0], vel[i][1]),ha='left', va='top', size=30)
    
plt.show()
#Generate openCL
buffer = "__constant float vel[" + str((2*vel_N)) + "] = {"
for i in range(0,vel_N -1):
  buffer += str(vel[i][0]) + "f, " +  str(vel[i][1]) + "f, " 
  
buffer +=str(vel[vel_N-1][0]) + "f, " +  str(vel[vel_N-1][1])  + "f};" 

print(buffer)

