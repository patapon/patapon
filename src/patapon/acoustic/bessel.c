#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <float.h>

#define _NewtonInitGuess 0.3
#define _NewtonIter 50
#define _NewtonTol 1e-6f
#define _M_PI 3.141593f
#define _M_2_PI 6.283186f
#define _MAX_B 90.0f
#define _TEST_TOL_ 1e-2f


/*Gauss-Legendre Quadrature */
/* n = 32 */
static float x32[16] = {0.0483076656877383162348126,
0.1444719615827964934851864,0.2392873622521370745446032,
0.3318686022821276497799168,0.4213512761306353453641194,
0.5068999089322293900237475,0.5877157572407623290407455,
0.6630442669302152009751152,0.7321821187402896803874267,
0.7944837959679424069630973,0.8493676137325699701336930,
0.8963211557660521239653072,0.9349060759377396891709191,
0.9647622555875064307738119,0.9856115115452683354001750,
0.9972638618494815635449811};

static float w32[16] = {0.0965400885147278005667648,
0.0956387200792748594190820,0.0938443990808045656391802,
0.0911738786957638847128686,0.0876520930044038111427715,
0.0833119242269467552221991,0.0781938957870703064717409,
0.0723457941088485062253994,0.0658222227763618468376501,
0.0586840934785355471452836,0.0509980592623761761961632,
0.0428358980222266806568786,0.0342738629130214331026877,
0.0253920653092620594557526,0.0162743947309056706051706,
0.0070186100094700966004071};


float gauss_legendre32(float (*f)(float, float*), float* data, float a, float b)
/* Gauss-Legendre quadrature order 32 */
{
	float* x = x32;
	float* w = w32;
    int n = 16;
    
    float s;
	float A;
	float B;
	float Ax;

    s = 0.0f;
	A = 0.5f * (b - a);
	B = 0.5f * (b + a);

    for (unsigned int i = 0; i < n; i++) {
			Ax = A * x[i];
			s += w[i] * ((*f)(B + Ax, data)+(*f)(B - Ax, data));			
    }
	return A * s;
}


/* Modified Bessel functions of the first kind */
float bessi0(float x)
/* Compute BesselI0(x) for any real x. */
{
    float ax;
    float ans;
    float y; 

    if ((ax = fabs(x)) < 3.75) {
        y = x / 3.75;
        y *= y;
        ans = 1.0 + y * (3.5156229 + y * (3.0899424 + y * (1.2067492 
            + y * (0.2659732 + y * (0.360768e-1 + y * 0.45813e-2)))));
    } else {
        y = 3.75 / ax;
        ans = (exp(ax) / sqrt(ax)) * (0.39894228 + y * (0.1328592e-1 
            + y * (0.225319e-2 + y * (-0.157565e-2 + y * (0.916281e-2 
            + y * (-0.2057706e-1 + y * (0.2635537e-1 + y * (-0.1647633e-1 
            + y * 0.392377e-2))))))));
    }
    return ans;
}


float bessi1(float x)
/* Compute BessselI1(x) for any real x. */
{
    float ax;
    float ans;
    float y;
    if ((ax = fabs(x)) < 3.75) {
        y = x / 3.75;
        y *= y;
        ans = ax * (0.5 + y * (0.87890594 + y * (0.51498869 
            + y * (0.15084934 + y * (0.2658733e-1 
            + y * (0.301532e-2 + y * 0.32411e-3))))));
    } else {
        y = 3.75 / ax;
        
        ans = 0.2282967e-1 + y * (-0.2895312e-1 
            + y * (0.1787654e-1 - y*0.420059e-2));
            
        ans = 0.39894228 + y * (-0.3988024e-1 + y * (-0.362018e-2
            + y * (0.163801e-2 + y * (-0.1031555e-1 + y * ans))));
        ans *= (exp(ax) / sqrt(ax));
    }
    return x < 0.0 ? -ans : ans;
}


float bi1_over_bi0(float x)
/* Compute (BesselI1(x)/BesselI0(x)) for any real x > 0 */
{
    float t1 = x * x;
    float t2 = t1 * t1;
    float t3 = t2 * t2;
    float t5 = t1 * x;
    float t6 = t2 * t5;
    float t8 = t2 * t1;
    float t10 = t2 * x;
    
    if (x < 3.75) {
        float t4 = t3 * t2;
        float t7 = t3 * t5;
        float t9 = t3 * t1;
        float t11 = t3 * x;
        float t24 = 0.8440273e9f + 0.7074629473e-1f * t4 
        + t7 * 0.251341329011722764e-15f + t9 * 0.925565614048674412e1f 
        + t11 * 0.482575351702507707e-13f + t3 * 0.114765423795251832e4f 
        + t6 * 0.617696450179209865e-11f + t8 * 0.915676963748728012e5f 
        + t10 * 0.790651456229388627e-9f + t2 * 0.439601434421872627e7f 
        + t5 * 0.168672310662269574e-7f + t1 * 0.105503372985039026e9f 
        + x * 0.101203386397361744e-6f;

        float t36 = 0.1688055e10f + t4 + t7 * 0.355271367880050093e-14f 
        + t9 * 0.110739309745002402e3f + t11 * 0.341060513164848089e-12f 
        + t3 * 0.114808546059141827e5f + t6 * 0.145519152283668518e-10f 
        + t8 * 0.732513939663389931e6f + t10 * (-0.931322574615478516e-9f) 
        + t2 * 0.263761736446225569e8f + t1 * 0.422013388659591973e9f 
        + x * (-0.298023223876953125e-7f);
        return (x * t24 / t36);
        
    } else {
        float t26 = (-0.411765893050994634e3f + 0.1000000000e1f * t3 
        + t6 * (-0.374868514813721632e0f) + t8 * (-0.127609390636661324e0f) 
        + t10 * 0.216521130776113679e0f + t2 * (-0.511337205973752695e1f) 
        + t5 * 0.424371204275464251e2f + t1 * (-0.201824047528880271e3f) 
        + x * 0.467296226160836625e3f) / (0.384630410945663925e3f + t3 
        + t6 * 0.124885735349985261e0f + t8 * 0.794237311222061315e-1f 
        + t10 * (-0.208278044508087845e0f) + t2 * 0.454196398787312461e1f 
        + t5 * (-0.382498377208852887e2f) + t1 * 0.183715863583072633e3f 
        + x * (-0.430694464873465051e3f));
        return t26;
    }
}

/* TODO : USE half_sqrt on OpenCl */
float bi1_over_bi0_der(float x)
/* Compute (Diff(BesselI1(x)/BesselI0(x),x)) for any real x > 0*/
{   
    float t1 = x * x;
    float t2 = t1 * t1;
    float t3 = t2 * t2;
    float t4 = t3 * t3;
    float t10 = t1 * t2;
    if (x < 3.75) { 
        float t11 = t3 * t1;
        float t27 = 0.5e0f + 0.415539843e-8f * t3 * t2 
        + 0.520855784e-2f * t2 + 0.271197469e-3f * t10 
        + 0.949687181e-5f * t3 + 0.2393965e-6f * t11 
        + 0.62499798e-1f * t1 + 0.701969197e-10f * t3 * t10 
        + 0.138983486e-11f * t4 - 0.1089247e-14f * t4 * t1 
        - 0.6880717e-15f * t4 * t2 - 0.1532295e-16f * t4 * t10 
        - 0.1334793e-18f * t4 * t3 - 0.1756445e-20f * t11 * t4;
        float t30 = pow(0.7903894e2f 
        + t1 + x * 0.120869550336504865e2f, 0.2e1f);
        float t35 = pow(0.4162678e2f 
        + t1 + x * 0.415247505642657355e1f, 0.2e1f);
        float t39 = pow(0.2696162e2f 
        + t1, 0.2e1f);
        float t42 = pow(0.5783793e1f + t1, 0.2e1f);
        float t47 = pow(0.4162678e2f 
        + t1 + x * (-0.415247505642656911e1f), 0.2e1f);
        float t51 = pow(0.7903894e2f 
        + t1 + x * (-0.120869550336504883e2f), 0.2e1f);
        return (0.2849528207e19f * t27 / t30 / t35 / t39 / t42 / t47 / t51);
    } else {
        float t6 = t1 * x;
        float t7 = t2 * t6;
        float t13 = t2 * x;
        float t32 = 0.1583777e6f - 0.4915003e-3f * t4 
        + t3 * t7 * 0.539119066304029837e0f 
        + t3 * t10 * (-0.830432664388546793e0f) 
        + t13 * t3 * 0.194484750177690238e2f 
        + t3 * t2 * (-0.163574545797908854e3f) 
        + t3 * t6 * 0.790865902029719564e3f 
        + t1 * t3 * (-0.189683162827931301e4f) 
        + t3 * x * 0.185245170155044616e4f 
        + t3 * (-0.177251288735982513e3f) 
        + t7 * (-0.116655671909182888e4f) 
        + t10 * 0.771173872709972238e4f 
        + t13 * (-0.344218181830821995e5f) 
        + t2 * 0.112963337573462239e6f 
        + t6 * (-0.262597958534803591e6f) 
        + t1 * 0.408054408662734553e6f + x * (-0.378692228024255484e6f);
        float t35 = pow(0.7791393e1f + t1 
        + x * 0.487174864930003437e1f, 0.2e1f);
        float t40 = pow(0.5513739e1f + t1 
        + x * 0.589435886745173332e0f, 0.2e1f);
        float t45 = pow(0.3542920e1f + t1 
        + x * (-0.228892392327561245e1f), 0.2e1f);
        float t49 = pow(0.2527091e1f + t1 
        + x * (-0.304737487741960988e1f), 0.2e1f);
        return (t32 / t35 / t40 / t45 / t49 / x);
    }
}


/* Root Finding */


bool m1_get_b_bissec(const float r1, float a, float b, float *bn)
/* Search Lagrange multiplier b by bissection method */
{
    float b1;
    float b0;
    float fa;
    float fb;
    float fc;
    
    b0 = bessi0(a);
    b1 = bessi1(a);
    fa = (b1 / b0 - r1);
    
    b0 = bessi0(b);
    b1 = bessi1(b);
    fb = (b1 / b0 - r1);
    
     if (fa * fb >= 0) { 
        printf("[error] Interval doesn't contain a solution\n"); 
        return false; 
    } 
    printf("[Bissec] bn %f fa = %f, fb = %f\n",*bn,fa,fb);
    float c = a; 
    while ((b-a) >= _NewtonTol) { 
        
        c = (a + b) / 2.0f; 
        
        b0 = bessi0(c);
        b1 = bessi1(c);
        fc = (b1 / b0 - r1);

        if ( fabs(fc) < _NewtonTol) {
            *bn = c;
            return true;
        }
        
        if (fc * fa < 0.0f) b = c; 
        else a = c;
        
    } 
    *bn = c;
    return true;
}


bool m1_get_b_newton(const float r1, float *bn, int *iter)
/* Search Lagrange multiplier b by Newton-Raphson method */
{	
    float xk;
    float xkp1;
    float b1;
    float b0;
    float der;
    float f;
    float t0;

    if( r1 == 0) { 
        *bn = 0.0f;
        *iter = 0;
        return true;
    }
    /* Initial Guess */
    if( r1 > 0) xk = r1 / (1.0f - 0.9f*r1*r1);

    xkp1 = xk;
    
    for(unsigned int i = 0; i < _NewtonIter; i++) {
        
        /* Crop for float precision */
        if (xk > _MAX_B){
            *bn = _MAX_B;
            return true;
        }
        /* Compute function and derivative */
        b0 = bessi0(xk);
        b1 = bessi1(xk);
        // f = (b1 / b0 - r1);
        f = bi1_over_bi0(xk) -r1;
        
        // t0 = b0 * b0 * xk;
        // der = (-b1 * b1 * xk + t0 - b1 * b0) / t0;
        der = bi1_over_bi0_der(xk);
        xkp1 = xk - f / der;
        
        /* Minimum found */
        // || fabs(f) < _NewtonTol
        if (fabs(xkp1 - xk) < _NewtonTol * fabs(xkp1) || fabs(f) < _NewtonTol ){
            *iter = i;
            *bn = xkp1;
            return true;
        }
        
        xk = xkp1;
    }
    printf("[Warning] Newton method didn't converge after %d iterations\n",_NewtonIter);
    
    // if( m1_get_b_bissec(r1, xkp1, _MAX_B, bn) ){
        // printf("[NEWTON] bn = %f \n",*bn);
        // return true;
    // }
   
   
    // printf("[Error] Bisection method didn't converge\n");
    *bn = -1.0f;

    return false;
};


float f_pdf(float x, float *data)
/* Reconstruct w (density) using pdf */
{
	return data[0] * exp(data[1] * cos(x));
}


float fv_pdf(float x, float *data)
/* Reconstruct |I| (density) using pdf */
{   
    float cx = cos(x);
	return data[0] * cx * exp(data[1] * cx);
}


bool test_bessel(float xmin, float xmax, int N, bool verbose)
{
    float x = xmin;
    float b0;
    float b1;
    float bi1_over_bi0_direct;
    float bi1_over_bi0_optim;
    float bi1_over_bi0_der_direct;
    float bi1_over_bi0_der_optim;
    float t0;
    
    float dx = fabs(xmax - xmin) / N;
    for ( unsigned int i = 0; i < N; i++) {
        x += dx;
        
        /* Direct method */
        b0 = bessi0(x);
        b1 = bessi1(x);
        bi1_over_bi0_direct = b1 / b0;
        t0 = b0 * b0 * x;
        bi1_over_bi0_der_direct = (-b1 * b1 * x + t0 - b1 * b0) / t0;
        
        /* Optimized version */
        bi1_over_bi0_optim = bi1_over_bi0(x);
        bi1_over_bi0_der_optim = bi1_over_bi0_der(x);

        /* Test Floating point error */
        if( isnan(b0) || isnan(b1) || !isfinite(b0) || !isfinite(b1) ||
            isnan(bi1_over_bi0_optim) || isnan(bi1_over_bi0_der_optim) ||
            !isfinite(bi1_over_bi0_optim) || !isfinite(bi1_over_bi0_der_optim)){
            
            printf("# Run [%d/%d]. x=%f :\n", i, N-1, x);
            printf(" - Direct b0 = %f, b1 = %f\n", x, b0, b1);
            printf(" - Direct b1/b0 = %f\n", bi1_over_bi0_direct);
            printf(" - Optimized b1/b0 = %f\n", bi1_over_bi0_optim);
            printf(" - Direct D(b1/b0) = %f\n", bi1_over_bi0_der_direct);
            printf(" - Optimized D(b1/b0) = %f\n", bi1_over_bi0_der_optim);
            return false;
        }
            
        if( verbose ){
            printf("# Run [%d/%d]. x=%f :\n", i, N-1, x);
            printf(" - Direct b0 = %f, b1 = %f\n", x, b0, b1);
            printf(" - Direct b1/b0 = %f\n", bi1_over_bi0_direct);
            printf(" - Optimized b1/b0 = %f\n", bi1_over_bi0_optim);
            printf(" - Direct D(b1/b0) = %f\n", bi1_over_bi0_der_direct);
            printf(" - Optimized D(b1/b0) = %f\n", bi1_over_bi0_der_optim);
        }
    }
    return true;
}

bool test_method(float In_min, float In_max, int N, bool verbose){
    /* Density */
    float w_ref;
    float w_r;
    float w_err;
    
    /* Intensity */
    float In;
    float In_r;
    float In_err;
    
    /* Step */
    float dr1;
    
    /*Lagrange Multipliers */
    float bn;
    float a;
    float data[2];
    
    dr1 = (In_max - In_min) / N;
    w_ref = 1.0f;
    int iter;
    for ( unsigned int i = 0; i < N; i++) {
        
        In = In_min + i * dr1;
        /* Get Lagrange Multipliers */
        if(!m1_get_b_newton(In, &bn, &iter)){
            printf("# Run [%d/%d]. R1=%f :\n", i, N-1, In);
            return false;
        }
        a = w_ref / ( 2.0f * _M_PI * bessi0(bn) );
        
        /* Reconstruction with Gauss-Legendre Quadrature */
        data[0] = a;
        data[1] = bn;
        
        w_r = fabs(gauss_legendre32(f_pdf, data, 0.0f, _M_2_PI));
        In_r = fabs(gauss_legendre32(fv_pdf, data, 0.0f, _M_2_PI));
        
        /* Compute errors */
        w_err = fabs(w_ref - w_r);
        In_err = fabs(In - In_r);
        
        if ( (w_err > _TEST_TOL_) || (In_err > _TEST_TOL_) ) { 
            printf("# Run [%d/%d]. R1=%f :\n", i, N-1, In);
            printf(" - Found a=%f, b=%f, iter=%d\n", a, bn, iter);
            printf(" - Error on w = %f, In = %f \n", w_err, In_err);
            return false;
        }
        if(verbose) {
            printf("# Run [%d/%d]. R1=%f\n :", i, N-1, In);
            printf(" - Found a=%f, b=%f, iter=%d\n", a, bn, iter);
            printf(" - Error on w = %f, In = %f \n", w_err, In_err);
        }
         
    }
    return true;
}

int main(int argc, char *argv[])

{   
    float In_min = 0.0f;
    float In_max = 1.0f;
    int N = 1000;
    bool verbose = false;
    
    
    
    if (test_bessel(0.0f, _MAX_B, N, verbose)) {
        printf("Test 1/2 OK\n");
    } else {
        printf("Test 1/2 Failed\n");
    }
    
    if (test_method(In_min, In_max, N, verbose)) {
        printf("Test 2/2 OK\n");
    } else {
        printf("Test 2/2 Failed\n");
    }
    return EXIT_SUCCESS;

}