import lbm_cl_light as mhd


L = 20.
Tmax = 5.
resolutions = 128, 256, 512, #1024, 2048, 4096, 8192

results = []
for n in resolutions:
    print(">>> Computing lbm_cl_light for nx = ny =", n)
    x, y, wplot_gpu, bandwidth = mhd.solve_ocl(nx=n, ny=n, Lx=L, Ly=L, Tmax=Tmax, precision='single')
    print(">>> bandwidth time [Mb/s]: {:.2f}".format(bandwidth))
    results.append((n, bandwidth))

# Format title according to max resolution number of digits
width = len(str(max(resolutions)))
title = "{n:<{width}} bandwidth [Mb/s]".format(n="n", width=width)
line = "-"*len(title)
print(">>> Summary")
print(line)
print(title)
print(line)
for result in results:
    print("{n:<{width}d} {bw:.2f}".format(width=width, n=result[0], bw=result[1]))
print(line)
