//  Pieced together from Boost C++ and Cephes by
//  Andreas Kloeckner (C) 2012
//
//  Pieces from:
//
//  Copyright (c) 2006 Xiaogang Zhang, John Maddock
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See
//  http://www.boost.org/LICENSE_1_0.txt)
//
// Cephes Math Library Release 2.8:  June, 2000
// Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
// What you see here may be used freely, but it comes with no support or
// guarantee.

//#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#define real _real_

#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _DT _dt_
#define _M _m_
#define _N _n_
#define _VX _vx_
#define _VY _vy_
#define _NCPR _ncpr_

#define _LAMBDA _lambda_
#define _LAMBDA2 _lambda2_

#define a _a_
#define b _b_

#ifndef M_PI
#define M_PI (3.14159265358979323846264338328)
#endif

#define _VOL (_DX * _DY)

//#define eRho 0
//#define eU 1
//#define eE 2
//#define eV 3
//#define eW 4
//#define eBy 5
//#define eBz 6
//#define eBx 7
//#define ePsi 8
//#define eP 2

#define eX 0
#define eY 1
#define eZ 2

//#define dirichlet
//#define dirichlet_updown
//#define dirichlet_leftright

__constant int dir[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

__constant real ds[4] = {_DY, _DY, _DX, _DX};

//#define _NEW_DIV_CLEAN

void flux_phy(const real *W, const real *vn, real *flux) {
  flux[0] = a*W[0]*vn[0]+b*W[0]*vn[1];
  flux[1] = (_LAMBDA2/2-a)*W[1]*vn[0]+(-_LAMBDA2/2-a)*W[2]*vn[1]; 
  flux[2] = (-b*W[1]-_LAMBDA2/2*W[2])*vn[0]+(-_LAMBDA2/2*W[1]-b*W[2])*vn[1];
}



// equilibrium "maxwellian" from macro data w
void w2f(const real *w, real *f) {
  for (int d = 0; d < 4; d++) {
    real flux[_M];
    real vnorm[3] = {(real)dir[d][0], (real)dir[d][1], (real)0};
    flux_phy(w, vnorm, flux);
    for (int iv = 0; iv < _M; iv++) {
      f[d * _M + iv] =  w[iv] / 4 + flux[iv] / 2 / _LAMBDA;
    }
  }
}

// macro data w from micro data f
void f2w(const real *f, real *w) {
  for (int iv = 0; iv < _M; iv++)
    w[iv] = 0;
  for (int d = 0; d < 4; d++) {
    for (int iv = 0; iv < _M; iv++) {
      w[iv] += f[d * _M + iv];
    }
  }
}




real gauss(real r){

  return exp(-r*r/2);
  
}

void exact_smooth_vortex(real *x, real t, real *w) {

  const real sigma = 0.05;
  const real udrift[2] = {a,b};
  const real xstart[2] = {0.25, 0.25};
  //
  real xrel[3] = {x[0] - udrift[0] * t - xstart[0],
                  x[1] - udrift[1] * t - xstart[1], 0.0};
  //
  real r = sqrt(xrel[0] * xrel[0] + xrel[1] * xrel[1])/sigma;
  real expf = gauss(r);

  w[0] = expf;

  const real sigma_y = 0.05;
  const real udrift_y[2] = {-a,-b};
  const real xstart_y[2] = {0.5, 0.5};
  //
  real xrel_y[3] = {x[0] - udrift_y[0] * t - xstart_y[0],
                  x[1] - udrift_y[1] * t - xstart_y[1], 0.0};
  //
  real r_y = sqrt(xrel_y[0] * xrel_y[0] + xrel_y[1] * xrel_y[1])/sigma_y;
  real expf_y = gauss(r_y);

  w[1] = expf_y;
  w[2] = expf_y;
}



// initial condition on the macro data
__kernel void init_sol(__global real *fn) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real wnow[_M];

  real t = 0;
  real xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};

  //exact_sol(xy, t, wnow);
  exact_smooth_vortex(xy, t, wnow);


  real fnow[_N];
  w2f(wnow, fnow);

  // printf("x=%f, y=%f \n",xy[0],xy[1]);
  // load middle value
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fn[imem] = fnow[ik];
    // fn[imem] = j;
  }
}




// one time step of the LBM scheme
__kernel void time_step(__global const real *fn, __global real *fnp1) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real fnow[_N];

  // shift of values in domain
  for (int d = 0; d < 4; d++) {
    int iR = (i - dir[d][0] + _NX) % _NX;
    int jR = (j - dir[d][1] + _NY) % _NY;

    for (int iv = 0; iv < _M; iv++) {
      int ik = d * _M + iv;
      int imem = iR + jR * _NX + ik * ngrid;
// #ifdef dirichlet_updown
//       // dirichlet condition on up and down borders
//       // (values of border cells are unchanged)
//       if ((j == 0) || (j == _NY - 1)) {
//         imem = i + j * _NX + ik * ngrid;
//       }
// #elif defined dirichlet_leftright
//       // dirichlet condition on left and right borders
//       // (values of border cells are unchanged)
//       if ((i == 0) || (i == _NX - 1)) {
//         imem = i + j * _NX + ik * ngrid;
//       }
// #elif defined dirichlet
//       // dirichlet condition on all borders
//       // (values of border cells are unchanged)
//       if ((i == 0) || (i == _NX - 1) || (j == 0) || (j == _NY - 1)) {
//         imem = i + j * _NX + ik * ngrid;
//       }
// #endif
      fnow[ik] = fn[imem];
    }
  }

  real wnow[_M];
  f2w(fnow + 0, wnow + 0);

  real fnext[_N];
  // first order relaxation
  w2f(wnow + 0, fnext + 0);

  real om = 2.;
  // second order relaxation
  for (int iv = 0; iv < _M; iv++) {
    for (int d = 0; d < 4; d++) {
      int ik = d * _M + iv;
      fnext[ik] = om * fnext[ik] - (om - 1) * fnow[ik];
    }
  }

  // save
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fnp1[imem] = fnext[ik];
  }
}

// numerical divergence of B
__kernel void div_b(__global real *fn, __global real *div_b) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real fR[_N];

  real flux[_M];

  div_b[i + j * _NX] = 0;
  for (int idir = 0; idir < 4; idir++) {
    real vn[3];
    vn[eX] = dir[idir][0];
    vn[eY] = dir[idir][1];
    vn[eZ] = 0;
    int iR = (i + dir[idir][0] + _NX) % _NX;
    int jR = (j + dir[idir][1] + _NY) % _NY;
    // load neighbour values
    real wR[_M];
    for (int ik = 0; ik < _N; ik++) {
      int imem = ik * ngrid + iR + jR * _NX;
      fR[ik] = fn[imem];
    }
    f2w(fR + 0, wR + 0);

    //real bn = wR[eBx] * vn[eX] + wR[eBy] * vn[eY] + wR[eBz] * vn[eZ];
    // flux_phy(wR, vn, flux);

    // update div b
    //div_b[i + j * _NX] += bn / 2 / ds[idir];
  }
}

// kinetic error
__kernel void kinetic(const real t, __global real *fn, __global real *kin) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;
  int ngrid = _NX * _NY;

  real floc[_N];

  // save
  int imemk = i + j * _NX;
  kin[imemk] = 0;

  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    floc[ik] = fn[imem];
  }
  real wn[_M];

  f2w(floc + 0, wn + 0);
  real xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};
  real wnow[_M];
  exact_smooth_vortex(xy, t, wnow);
  //real kinloc = (wn[eU]-wnow[eU]) * (wn[eU]-wnow[eU]);
  real kinloc = fabs(wn[0]-wnow[0]);
  //kinloc += wn[eV] * wn[eV];
  //kinloc *= 0.5 / wn[eRho];
  kin[imemk] += kinloc * _DX * _DY;
}
