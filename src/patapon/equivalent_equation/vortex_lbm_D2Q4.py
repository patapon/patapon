#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Resolution of a transport equation by the finite volume method on regular grid
"""


from __future__ import absolute_import, print_function
import h5py
import sys
sys.path.append('..')
from pylab import contour, cm, clabel, streamplot
import matplotlib.pyplot as plt
import matplotlib
from time import sleep
import numpy as np
import pyopencl as cl
from utils import Figure, float2str, load_kernel, parse_cl_args, get_ite_title

# matplotlib.use('Agg')
#from matplotlib import rcParams


# Default values

# number of conservative variables
_m = 4 #2

# number of kinetic variables
_n = 4 * _m

_ivplot = 3

_minplot = -1
_maxplot = 1


# grid size
_nx = 800
_ny = 800

_Lx = 1.
_Ly = 1.


_Tmax = 0.06

#eq_eq = 1   #0 for y obtained with w, 1 for y obtained with the equivalent equations



def solve_ocl(m=_m, n=_n, nx=_nx, ny=_ny, Lx=_Lx, Ly=_Ly, Tmax=_Tmax,
              animate=False, precision="single", savekineticdata="False",
              savemacrodata="False"):




    dx = Lx / nx
    dy = Ly / ny


    # x and y velocities
    a = 1.#0.5#/Tmax 
    b = 1.#0.5#0.5/Tmax

    c= np.sqrt(a*a+b*b)


    # time stepping
    cfl = 1.

    # 
    lam = 2.

    # lattice speed
    
    if eq_eq == 0:
        vmax = lam
    if eq_eq == 1:
        vmax = 2*max(c,lam*lam)

    dt = cfl * dx / vmax 


    print("Tmax",Tmax)
    print("a",a)
    print("b",b)
    print("vmax",vmax)
    print("dx",dx)
    print("dt",dt)

    # compression for output data
    ncpr = 1000 #10000  # 1000 #1024
    testname = "smooth-vortex_"
    #testname = "orszag_"
    #testname = "tilt-p1_"
    foldername = "../h5/"
    ncprx = nx
    ncpry = ny
    if (nx > ncpr):
        ncprx = ncpr
        ncpry = ncpr

    # For plotting
    x = np.linspace(0., Lx, num=ncprx)
    y = np.linspace(0., Ly, num=ncpry)

    parameters = {'nx': nx,
                  'ny': ny,
                  'dx': dx,
                  'dy': dy,
                  'dt': dt,
                  'm': m,
                  'n': n,
                  'lambda' : vmax,
                  'lambda2': lam,
                  'ncpr': ncpr,
                  'a': a,
                  'b': b,
                  'eq_eq': eq_eq
                  }

    np_real, source = load_kernel("vortex_kernels_D2Q4.cl", parameters, precision=precision, print_source=False,
                                  module_file=__file__)

    dt = np_real(dt)
    Tmax = np_real(Tmax)

    # OpenCL init
    ctx = cl.create_some_context()
    mf = cl.mem_flags

    #opt = "-cl-denorms-are-zero -cl-fast-relaxed-math"
    opt = ""
    
    if precision == "single":
        opt = opt + " -cl-single-precision-constant"

    print(opt)
    # compile OpenCL C program
    prg = cl.Program(ctx, source).build(options= opt)

    # create a queue (for submitting opencl operations)
    queue = cl.CommandQueue(
        ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)



    # number of animation frames
    nbplots = 10  # 120
    itermax = int(np.floor(Tmax / dt))
    iterplot = int(itermax / nbplots)    
    print("Number of iterations :",itermax)
    macrotobedump = [0, 2, 5, 7, 8]  # [0,1,2,3,5,7,8]

    # time loop
    t = np_real(0)
    ite = 0
    elapsed = 0.

    fn_cpu = np.zeros((4 * m * ncprx * ncpry, ), dtype=np_real)
    fnp1_cpu = np.zeros((4 * m * ncprx * ncpry, ), dtype=np_real)
    kinetic_cpu = np.zeros((1 * ncprx * ncpry, ), dtype=np_real)

    # create OpenCL buffers
    #buffer_size = 4 * m * nx * ny * np.dtype(np_real).itemsize
    #kinetic_buffer_size = ncprx * ncpry * np.dtype(np_real).itemsize
    fn_gpu = cl.Buffer(ctx, mf.READ_WRITE| mf.COPY_HOST_PTR, hostbuf = fn_cpu)
    fnp1_gpu = cl.Buffer(ctx, mf.READ_WRITE| mf.COPY_HOST_PTR, hostbuf = fnp1_cpu)
    kinetic_gpu = cl.Buffer(ctx, mf.READ_WRITE| mf.COPY_HOST_PTR, hostbuf = kinetic_cpu)
    #fnp1_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    #divb_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)

    # init data
    event = prg.init_sol(queue, (nx * ny, ), None, fn_gpu)
    event.wait()

    #event = prg.init_smooth_vortex(queue, (nx * ny, ), None, fnp1_gpu)
    #event.wait()
    #queue.finish()



    #divb_cpu = np.zeros((1 * nx * ny, ), dtype=np_real)

    if True:
        plot_title0 = r"$n_x = {}, n_y = {}, w$".format(nx, ny)
        plot_title1 = r"$n_x = {}, n_y = {}, y_{}$".format(nx, ny, _ivplot)+" obtained with w"
        plot_title2 = r"$n_x = {}, n_y = {}, y_{}$".format(nx, ny, _ivplot)+" obtained with the equivalent equations"

        plot_title0 = ''
        plot_title1 = ''
        plot_title2 = ''

        #outputname = "fig/"+testname+str(nx)+"_0.png"
        # plt.savefig(outputname)
        #fig0 = Figure(title=plot_title0,levels=np.linspace(_minplot, _maxplot, 16))
        if eq_eq == 0:
            #fig1 = Figure(title=plot_title1,levels=np.linspace(_minplot, _maxplot, 16))
            fig1 = Figure(title=plot_title1,levels=14)
        if eq_eq == 1 :
            #fig2 = Figure(title=plot_title2,levels=np.linspace(_minplot, _maxplot, 16))
            fig2 = Figure(title=plot_title2,levels=14)
            
    print("start OpenCL computations...")
    #while t < Tmax :#+ dt:
    while ite <= itermax + itermax%2:

        #print(itermax, iterplot, ite, " :: ", t)
        #ite_title = get_ite_title(ite, t, elapsed)
        ite_title = ''

        # plt.ioff()

        if animate:
            if ite % iterplot == 0:
                event = cl.enqueue_copy(queue, fn_cpu, fn_gpu)
                event.wait()
                event = prg.kinetic(queue, (nx * ny, ), None,
                                    t, fn_gpu, kinetic_gpu).wait()
                cl.enqueue_copy(queue, kinetic_cpu, kinetic_gpu).wait()

                wplot = np.reshape(fn_cpu, (4, m, ncprx, ncpry))

                #fig0.update(x, y, np.sum(wplot[:, 0, :, :], axis=0), suptitle=ite_title, cb=ite == 0)
                if eq_eq == 0:
                    yplot = np.empty(np.shape(wplot))
                    if _ivplot==1: #y1
                        yplot = (-wplot[0, 0, :, :]+wplot[1, 0, :, :])*lam-a*np.sum(wplot[:, 0, :, :], axis=0)
                    if _ivplot==2: #y2
                        yplot = (-wplot[2, 0, :, :]+wplot[3, 0, :, :])*lam-b*np.sum(wplot[:, 0, :, :], axis=0)

                    fig1.update(x, y, yplot, suptitle=ite_title, cb=ite == 0)
                if eq_eq == 1:
                    fig2.update(x, y, np.sum(wplot[:, _ivplot, :, :], axis=0), suptitle=ite_title, cb=ite == 0)


                if np.isnan(np.sum(kinetic_cpu)):
                    exit("Nan in kinetic_cpu at ite {ite}")
                else:
                    #print("t=",t," L2 norm =", np.sqrt(np.sum(kinetic_cpu)/Lx/Ly))
                    print("t=",t," L1 norm =", np.sum(kinetic_cpu))

        else:
            print(ite_title, end='\r')

        t += dt
        #event = prg.time_step(queue, (nx * ny, ), None, wn_gpu, wnp1_gpu)
        event = prg.time_step(queue, (nx * ny, ), None, fn_gpu, fnp1_gpu)
        #event = prg.time_step(queue, (nx * ny, ), None, wn_gpu, wnp1_gpu, wait_for = [event])
        event.wait()
        queue.finish()
        elapsed += 1e-9 * (event.profile.end - event.profile.start)
        # exchange buffer references for avoiding a copy

        #cl.enqueue_copy(queue, fn_gpu, fnp1_gpu).wait()

        fn_gpu, fnp1_gpu = fnp1_gpu, fn_gpu
        queue.finish()

        #fn_sauv = fnp1_gpu
        #fnp1_gpu = fn_gpu
        #fn_gpu = fn_sauv

        ite += 1

    # copy OpenCL data to CPU and return the results
    cl.enqueue_copy(queue, fn_cpu, fnp1_gpu).wait()
    queue.finish()
    print("fin")

    wplot_gpu = np.reshape(fn_cpu, (4, m, ncprx, ncpry))
    #fig0.update(x, y, np.sum(wplot_gpu[:, 0, :, :], axis=0), suptitle=ite_title, cb=True)
    yplot_gpu = np.empty([ncprx,ncpry])
    if eq_eq==0:
        #yplot_gpu = np.empty(np.shape(wplot_gpu))
        if _ivplot==0: #w
            yplot_gpu = wplot_gpu[0, 0, :, :] + wplot_gpu[1, 0, :, :]
        if _ivplot==1: #y1
            yplot_gpu = (-wplot_gpu[0, 0, :, :]+wplot_gpu[1, 0, :, :])*lam-a*np.sum(wplot_gpu[:, 0, :, :], axis=0)
        if _ivplot==2: #y2
            yplot_gpu = (-wplot_gpu[2, 0, :, :]+wplot_gpu[3, 0, :, :])*lam-b*np.sum(wplot_gpu[:, 0, :, :], axis=0)
        if _ivplot==3: #z3
            yplot_gpu = (wplot_gpu[0, 0, :, :]+wplot_gpu[1, 0, :, :]-wplot_gpu[2, 0, :, :]-wplot_gpu[3, 0, :, :])*lam**2
    
        fig1.update(x, y, yplot_gpu, suptitle=ite_title, cb=True)
    if eq_eq==1:
        yplot_gpu = np.sum(wplot_gpu[:, _ivplot, :, :], axis=0)
        fig2.update(x, y, yplot_gpu, suptitle=ite_title, cb=True)   


    plt.show()

    return x, y, yplot_gpu


if __name__ == '__main__':

    args = parse_cl_args(n=_nx, L=_Lx, tmax=_Tmax,
                         description='Solve orszag-tang using LBM on PyOpenCL')


    # y obtained with w 
    eq_eq = 0   
    x, y, y_w = solve_ocl(**vars(args))

    # y obtained with the equivalent equations
    eq_eq = 1   
    x, y, y_eq = solve_ocl(**vars(args))

    fig3 = Figure(levels=14)
    fig3.update(x, y, np.abs(y_w-y_eq), cb=True)   
    plt.show()

    err=np.sqrt(_Lx / _nx * _Ly / _ny * np.sum(np.sum(np.square(y_w-y_eq),axis=1),axis=0))
    print("erreur L2 : ", err)
