//  Pieced together from Boost C++ and Cephes by
//  Andreas Kloeckner (C) 2012
//
//  Pieces from:
//
//  Copyright (c) 2006 Xiaogang Zhang, John Maddock
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See
//  http://www.boost.org/LICENSE_1_0.txt)
//
// Cephes Math Library Release 2.8:  June, 2000
// Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
// What you see here may be used freely, but it comes with no support or
// guarantee.

//#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#pragma once

#include <pyopencl-airy.cl>
#include <pyopencl-eval-tbl.cl>

#define real _real_

#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _DT _dt_
#define _M _m_
#define _N _n_
#define _VX _vx_
#define _VY _vy_
#define _NCPR _ncpr_

#define _LAMBDA _lambda_

#ifndef M_PI
#define M_PI (3.14159265358979323846264338328_F)
#endif

#define _VOL (_DX * _DY)

#define eRho 0
#define eU 1
#define eE 2
#define eV 3
#define eW 4
#define eBy 5
#define eBz 6
#define eBx 7
#define ePsi 8
#define eP 2

#define eX 0
#define eY 1
#define eZ 2

//#define dirichlet
//#define dirichlet_updown
//#define dirichlet_leftright

typedef double bessel_j_scalar_type;
// FIXME: T is really a bad name
typedef bessel_j_scalar_type T;

// {{{ bessel_j0

__constant const bessel_j_scalar_type bessel_j0_P1[] = {
    -4.1298668500990866786e+11, 2.7282507878605942706e+10,
    -6.2140700423540120665e+08, 6.6302997904833794242e+06,
    -3.6629814655107086448e+04, 1.0344222815443188943e+02,
    -1.2117036164593528341e-01};
__constant const bessel_j_scalar_type bessel_j0_Q1[] = {
    2.3883787996332290397e+12,
    2.6328198300859648632e+10,
    1.3985097372263433271e+08,
    4.5612696224219938200e+05,
    9.3614022392337710626e+02,
    1.0,
    0.0};
__constant const bessel_j_scalar_type bessel_j0_P2[] = {
    -1.8319397969392084011e+03, -1.2254078161378989535e+04,
    -7.2879702464464618998e+03, 1.0341910641583726701e+04,
    1.1725046279757103576e+04,  4.4176707025325087628e+03,
    7.4321196680624245801e+02,  4.8591703355916499363e+01};
__constant const bessel_j_scalar_type bessel_j0_Q2[] = {
    -3.5783478026152301072e+05, 2.4599102262586308984e+05,
    -8.4055062591169562211e+04, 1.8680990008359188352e+04,
    -2.9458766545509337327e+03, 3.3307310774649071172e+02,
    -2.5258076240801555057e+01, 1.0};
__constant const bessel_j_scalar_type bessel_j0_PC[] = {
    2.2779090197304684302e+04, 4.1345386639580765797e+04,
    2.1170523380864944322e+04, 3.4806486443249270347e+03,
    1.5376201909008354296e+02, 8.8961548424210455236e-01};
__constant const bessel_j_scalar_type bessel_j0_QC[] = {
    2.2779090197304684318e+04, 4.1370412495510416640e+04,
    2.1215350561880115730e+04, 3.5028735138235608207e+03,
    1.5711159858080893649e+02, 1.0};
__constant const bessel_j_scalar_type bessel_j0_PS[] = {
    -8.9226600200800094098e+01, -1.8591953644342993800e+02,
    -1.1183429920482737611e+02, -2.2300261666214198472e+01,
    -1.2441026745835638459e+00, -8.8033303048680751817e-03};
__constant const bessel_j_scalar_type bessel_j0_QS[] = {
    5.7105024128512061905e+03, 1.1951131543434613647e+04,
    7.2642780169211018836e+03, 1.4887231232283756582e+03,
    9.0593769594993125859e+01, 1.0};

bessel_j_scalar_type bessel_j0(bessel_j_scalar_type x) {
  const bessel_j_scalar_type x1 = 2.4048255576957727686e+00,
                             x2 = 5.5200781102863106496e+00, x11 = 6.160e+02,
                             x12 = -1.42444230422723137837e-03,
                             x21 = 1.4130e+03, x22 = 5.46860286310649596604e-04;

  bessel_j_scalar_type value, factor, r, rc, rs;

  if (x < 0) {
    x = -x; // even function
  }
  if (x == 0) {
    return 1;
  }
  if (x <= 4) // x in (0, 4]
  {
    bessel_j_scalar_type y = x * x;
    r = boost_evaluate_rational(bessel_j0_P1, bessel_j0_Q1, y);
    factor = (x + x1) * ((x - x11 / 256) - x12);
    value = factor * r;
  } else if (x <= 8.0) // x in (4, 8]
  {
    bessel_j_scalar_type y = 1 - (x * x) / 64;
    r = boost_evaluate_rational(bessel_j0_P2, bessel_j0_Q2, y);
    factor = (x + x2) * ((x - x21 / 256) - x22);
    value = factor * r;
  } else // x in (8, \infty)
  {
    bessel_j_scalar_type y = 8 / x;
    bessel_j_scalar_type y2 = y * y;
    bessel_j_scalar_type z = x - 0.25f * M_PI;
    rc = boost_evaluate_rational(bessel_j0_PC, bessel_j0_QC, y2);
    rs = boost_evaluate_rational(bessel_j0_PS, bessel_j0_QS, y2);
    factor = sqrt(2 / (x * M_PI));
    value = factor * (rc * cos(z) - y * rs * sin(z));
  }

  return value;
}

// }}}

// {{{ bessel_j1

__constant const bessel_j_scalar_type bessel_j1_P1[] = {
    -1.4258509801366645672e+11, 6.6781041261492395835e+09,
    -1.1548696764841276794e+08, 9.8062904098958257677e+05,
    -4.4615792982775076130e+03, 1.0650724020080236441e+01,
    -1.0767857011487300348e-02};
__constant const bessel_j_scalar_type bessel_j1_Q1[] = {
    4.1868604460820175290e+12,
    4.2091902282580133541e+10,
    2.0228375140097033958e+08,
    5.9117614494174794095e+05,
    1.0742272239517380498e+03,
    1.0,
    0.0};
__constant const bessel_j_scalar_type bessel_j1_P2[] = {
    -1.7527881995806511112e+16, 1.6608531731299018674e+15,
    -3.6658018905416665164e+13, 3.5580665670910619166e+11,
    -1.8113931269860667829e+09, 5.0793266148011179143e+06,
    -7.5023342220781607561e+03, 4.6179191852758252278e+00};
__constant const bessel_j_scalar_type bessel_j1_Q2[] = {
    1.7253905888447681194e+18, 1.7128800897135812012e+16,
    8.4899346165481429307e+13, 2.7622777286244082666e+11,
    6.4872502899596389593e+08, 1.1267125065029138050e+06,
    1.3886978985861357615e+03, 1.0};
__constant const bessel_j_scalar_type bessel_j1_PC[] = {
    -4.4357578167941278571e+06,
    -9.9422465050776411957e+06,
    -6.6033732483649391093e+06,
    -1.5235293511811373833e+06,
    -1.0982405543459346727e+05,
    -1.6116166443246101165e+03,
    0.0};
__constant const bessel_j_scalar_type bessel_j1_QC[] = {
    -4.4357578167941278568e+06,
    -9.9341243899345856590e+06,
    -6.5853394797230870728e+06,
    -1.5118095066341608816e+06,
    -1.0726385991103820119e+05,
    -1.4550094401904961825e+03,
    1.0};
__constant const bessel_j_scalar_type bessel_j1_PS[] = {
    3.3220913409857223519e+04,
    8.5145160675335701966e+04,
    6.6178836581270835179e+04,
    1.8494262873223866797e+04,
    1.7063754290207680021e+03,
    3.5265133846636032186e+01,
    0.0};
__constant const bessel_j_scalar_type bessel_j1_QS[] = {
    7.0871281941028743574e+05,
    1.8194580422439972989e+06,
    1.4194606696037208929e+06,
    4.0029443582266975117e+05,
    3.7890229745772202641e+04,
    8.6383677696049909675e+02,
    1.0};

bessel_j_scalar_type bessel_j1(bessel_j_scalar_type x) {
  const bessel_j_scalar_type x1 = 3.8317059702075123156e+00,
                             x2 = 7.0155866698156187535e+00, x11 = 9.810e+02,
                             x12 = -3.2527979248768438556e-04, x21 = 1.7960e+03,
                             x22 = -3.8330184381246462950e-05;

  bessel_j_scalar_type value, factor, r, rc, rs, w;

  w = fabs(x);
  if (x == 0) {
    return 0;
  }
  if (w <= 4) // w in (0, 4]
  {
    bessel_j_scalar_type y = x * x;
    r = boost_evaluate_rational(bessel_j1_P1, bessel_j1_Q1, y);
    factor = w * (w + x1) * ((w - x11 / 256) - x12);
    value = factor * r;
  } else if (w <= 8) // w in (4, 8]
  {
    bessel_j_scalar_type y = x * x;
    r = boost_evaluate_rational(bessel_j1_P2, bessel_j1_Q2, y);
    factor = w * (w + x2) * ((w - x21 / 256) - x22);
    value = factor * r;
  } else // w in (8, \infty)
  {
    bessel_j_scalar_type y = 8 / w;
    bessel_j_scalar_type y2 = y * y;
    bessel_j_scalar_type z = w - 0.75f * M_PI;
    rc = boost_evaluate_rational(bessel_j1_PC, bessel_j1_QC, y2);
    rs = boost_evaluate_rational(bessel_j1_PS, bessel_j1_QS, y2);
    factor = sqrt(2 / (w * M_PI));
    value = factor * (rc * cos(z) - y * rs * sin(z));
  }

  if (x < 0) {
    value *= -1; // odd function
  }
  return value;
}

// }}}

__constant int dir[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

__constant real ds[4] = {_DY, _DY, _DX, _DX};

//#define _NEW_DIV_CLEAN

// physical flux of the hyperbolic system
/* void flux_phy(const real *w, const real* vnorm, real* flux){ */
/*   real vdotn = _VX * vnorm[0] + _VY * vnorm[1]; */
/*   int signe = 1; */
/*   for(int iv = 0; iv < _M; iv++){ */
/*     flux[iv] = vdotn * w[iv] * signe; */
/*     signe = -signe; */
/*   } */
/* } */

void flux_phy(const real *W, const real *vn, real *flux) {

  real gam = 1.6666666666_F;

  real un = (W[eU] * vn[eX] + W[eV] * vn[eY] + W[eW] * vn[eZ]) / W[eRho];
  real bn = W[eBx] * vn[eX] + W[eBy] * vn[eY] + W[eBz] * vn[eZ];

  real p =
      (gam - 1) *
      (W[eE] - (W[eU] * W[eU] + W[eV] * W[eV] + W[eW] * W[eW]) / 2 / W[eRho] -
       (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2);

  flux[eRho] = W[eRho] * un;
  flux[eU] =
      un * W[eU] +
      (p + (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2) * vn[eX] -
      bn * W[eBx];
  flux[eE] =
      (W[eE] + p + (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2) *
          un -
      (W[eBx] * W[eU] + W[eBy] * W[eV] + W[eBz] * W[eW]) * bn / W[eRho];
  flux[eV] =
      un * W[eV] +
      (p + (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2) * vn[1] -
      bn * W[eBy];
  flux[eW] =
      un * W[eW] +
      (p + (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2) * vn[2] -
      bn * W[eBz];

#ifdef _NEW_DIV_CLEAN

  flux[eBy] = -bn * W[eV] / W[eRho] + un * W[eBy];
  flux[eBz] = -bn * W[eW] / W[eRho] + un * W[eBz];
  flux[eBx] = -bn * W[eU] / W[eRho] + un * W[eBx];

  flux[ePsi] = bn;
#else

  flux[eBy] = -bn * W[eV] / W[eRho] + un * W[eBy] + W[ePsi] * vn[eY];
  flux[eBz] = -bn * W[eW] / W[eRho] + un * W[eBz] + W[ePsi] * vn[eZ];
  flux[eBx] = -bn * W[eU] / W[eRho] + un * W[eBx] + W[ePsi] * vn[eX];

  real c_h = 6;

  flux[ePsi] = c_h * c_h * bn;
  // flux[8] = 0.0; //!!!! TEST DEBUG

#endif
}

// equilibrium "maxwellian" from macro data w
void w2f(const real *w, real *f) {
  for (int d = 0; d < 4; d++) {
    real flux[_M];
    real vnorm[3] = {(real)dir[d][0], (real)dir[d][1], 0};
    flux_phy(w, vnorm, flux);
    for (int iv = 0; iv < _M; iv++) {
#ifdef _NEW_DIV_CLEAN
      int c = (iv != ePsi);
#else
      int c = 1;
#endif
      f[d * _M + iv] = c * w[iv] / 4 + flux[iv] / 2 / _LAMBDA;
    }
  }
}

// macro data w from micro data f
void f2w(const real *f, real *w) {
  for (int iv = 0; iv < _M; iv++)
    w[iv] = 0;
  for (int d = 0; d < 4; d++) {
    for (int iv = 0; iv < _M; iv++) {
      w[iv] += f[d * _M + iv];
    }
  }
}

// exact macro data w
// transport
/* void exact_sol(real* xy, real t, real* w){ */
/*   int signe = 1; */
/*   for(int iv = 0; iv < _M; iv++){ */
/*     real x = xy[0] - t * _VX * signe - 0.5_F; */
/*     real y = xy[1] - t * _VY * signe - 0.5_F; */
/*     real d2 = x * x + y *y; */
/*     w[iv] = exp(-30*d2); */
/*     signe = -signe; */
/*   } */
/* } */

// mhd
void conservatives(real *y, real *w) {
  real gam = 1.6666666666_F;

  w[eRho] = y[eRho];       // rho
  w[eU] = y[eRho] * y[eU]; // rho u
  w[eE] = y[eE] / (gam - 1) +
          y[eRho] * (y[eU] * y[eU] + y[eV] * y[eV] + y[eW] * y[eW]) / 2 +
          (y[eBx] * y[eBx] + y[eBy] * y[eBy] + y[eBz] * y[eBz]) / 2; // rho E
  w[eV] = y[eRho] * y[eV];                                           // rho v
  w[eW] = y[eRho] * y[eW];                                           // rho w
  w[eBy] = y[eBy];                                                   // By
  w[eBz] = y[eBz];                                                   // Bz
  w[eBx] = y[eBx];                                                   // Bx
  w[ePsi] = y[ePsi];                                                 // psi
}

void exact_sol(real *x, real t, real *w) {
  real gam = 1.6666666666_F;
  real yL[_M];

  yL[eRho] = gam * gam;             // rho
  yL[eU] = -sin(2 * M_PI * x[eY]);  // u
  yL[eP] = gam;                     // p
  yL[eV] = sin(2 * M_PI * x[eX]);   // v
  yL[eW] = 0;                       // w
  yL[eBy] = sin(4 * M_PI * x[eX]);  // By
  yL[eBz] = 0;                      // Bz
  yL[eBx] = -sin(2 * M_PI * x[eY]); // Bx
  yL[ePsi] = 0;                     // psi

  conservatives(yL, w);
}

// initial condition on the macro data
__kernel void init_sol(__global real *fn) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real wnow[_M];

  real t = 0;
  real xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};

  exact_sol(xy, t, wnow);

  real fnow[_N];
  w2f(wnow, fnow);

  // printf("x=%f, y=%f \n",xy[0],xy[1]);
  // load middle value
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fn[imem] = fnow[ik];
    // fn[imem] = j;
  }
}

real rr(double s) { return 8 * s < 1 ? 1 + (4096 * s * s - 128) * s * s : 0; }

void exact_smooth_vortex(real *x, real t, real *w) {


#define MHD2D_VORTEX_KAPPA 1.0_F
#define MHD2D_VORTEX_UREF 0.2_F
#define MHD2D_VORTEX_MU 1.0_F
#define MHD2D_VORTEX_BREF 0.2_F
  const real gam = 1.6666666666_F;
  const real rref2 = 1.0_F;
  const real sigmam2 = 0.5_F;
  const real kappa = MHD2D_VORTEX_KAPPA;
  const real uref = MHD2D_VORTEX_UREF;
  const real bref = MHD2D_VORTEX_BREF;
  const real mu = MHD2D_VORTEX_MU;
  const real udrift[2] = {1.0_F, 1.0_F};
  const real xstart[2] = {5._F, 5._F};
  //
  real xrel[3] = {x[0] - uref * udrift[0] * t - xstart[0],
                  x[1] - uref * udrift[1] * t - xstart[1], 0.0_F};
  //
  real r = sqrt(xrel[0] * xrel[0] + xrel[1] * xrel[1]);
  real expf = exp(sigmam2 * (1.0_F - r * r));

  // rho
  w[eRho] = 1.0_F;
  // qx = rho * ux
  w[eU] = uref * (udrift[0] - xrel[1] * kappa * expf);
  // qy = rho * uy
  w[eV] = uref * (udrift[1] + xrel[0] * kappa * expf);
  // Bx
  w[eBx] = -bref * xrel[1] * mu * expf;
  // By
  w[eBy] = bref * xrel[0] * mu * expf;
  // psi
  w[ePsi] = 0.0_F;
  //
  // real T = 1.0;
  // real e = T/(gam-1.0);
  //
  real p = 1.0_F - 0.5_F * bref * bref * mu * mu * r * r * expf * expf;
  real qdotq = w[1] * w[1] + w[2] * w[2];
  real BdotB = w[4] * w[4] + w[5] * w[5];
  //
  w[eE] = p / (gam - 1.0_F) + 0.5_F * (qdotq / w[0] + BdotB);

  w[eW] = 0._F;
  w[eBz] = 0._F;
}

// initial condition on the macro data
__kernel void init_smooth_vortex(__global real *fn) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real wnow[_M];

  real t = 0;
  real xy[3] = {i * _DX + _DX / 2, j * _DY + _DY / 2};

  exact_smooth_vortex(xy, t, wnow);

  real fnow[_N];
  w2f(wnow, fnow);

  // printf("x=%f, y=%f \n",xy[0],xy[1]);
  // load middle value
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fn[imem] = fnow[ik];
    // fn[imem] = j;
  }
}

void exact_nappes(real *x, real t, real *w) {
  // real gam = 1.6666666666_F;
  real yL[_M];

  yL[eRho] = 1.0_F;                     // rho
  yL[eU] = 0.1 * sin(2 * M_PI * x[eY]); // u
  yL[eP] = 0.1_F;                       // p
  yL[eV] = 0;                           // v
  yL[eW] = 0;                           // w
  yL[eBx] = 0;                          // Bx
  yL[eBz] = 0;                          // Bz
  if ((x[eX] > 1.0 / 4.0) && (x[eX] < 3.0 / 4.0)) {
    yL[eBy] = 1.0_F; // By
  } else {
    yL[eBy] = -1.0_F; // By
  }

  yL[ePsi] = 0; // psi

  conservatives(yL, w);
}

// initial condition for oscillaing on the macro data
__kernel void init_nappes(__global real *fn) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real wnow[_M];

  real t = 0;
  real xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};

  exact_nappes(xy, t, wnow);

  real fnow[_N];
  w2f(wnow, fnow);

  // printf("x=%f, y=%f \n",xy[0],xy[1]);
  // load middle value
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fn[imem] = fnow[ik];
    // fn[imem] = j;
  }
}

void exact_tilt(real *x, real t, real *w) {
  real yL[_M];

  real rad2 = x[eX] * x[eX] + x[eY] * x[eY];
  real p0 = 1.0_F;
  real eps = 0.001_F;

  yL[eRho] = 1.0_F;                       // rho
  yL[eU] = -2 * eps * x[eY] * exp(-rad2); // u
  yL[eV] = 2 * eps * x[eX] * exp(-rad2);  // v
  yL[eW] = 0;                             // w
  yL[eBz] = 0;                            // Bz
  yL[ePsi] = 0;                           // psi

  if (rad2 > 1.0) {
    yL[eP] = p0;                                                   // p
    yL[eBy] = 2 * x[eX] * x[eY] / (rad2 * rad2);                   // By
    yL[eBx] = (x[eX] * x[eX] - x[eY] * x[eY]) / (rad2 * rad2) - 1; // Bx
  } else {
    real rad = sqrt(rad2);
    real smallk = 3.8317059702;
    real j0 = bessel_j0(smallk * rad);
    real j1 = bessel_j1(smallk * rad);
    real bigk = -2 / (smallk * bessel_j0(smallk));
    real psi = bigk * bessel_j1(smallk * rad) * x[eY] / rad;

    yL[eP] = p0 + smallk * smallk / 2 * psi * psi; // p

    yL[eBx] = smallk * x[eY] * x[eY] / rad2 * j0; // Bx
    yL[eBx] += (x[eX] * x[eX] - x[eY] * x[eY]) / (rad2 * rad) * j1;
    yL[eBx] *= bigk;

    yL[eBy] = smallk * x[eX] * x[eY] / rad2 * j0; // By
    yL[eBy] -= 2 * x[eX] * x[eY] / (rad2 * rad) * j1;
    yL[eBy] *= -bigk;
  }

  conservatives(yL, w);
}

// initial condition for oscillaing on the macro data
__kernel void init_tilt(__global real *fn) {

  // domain here must be [-3,3]x[-3,3]

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real wnow[_M];

  real t = 0;
  real xy[2] = {-3 + i * _DX + _DX / 2, -3 + j * _DY + _DY / 2};
  // real xy[2] = {-6 + i * _DX + _DX / 2, -6 + j * _DY + _DY / 2};

  exact_tilt(xy, t, wnow);

  real fnow[_N];
  w2f(wnow, fnow);

  // printf("x=%f, y=%f \n",xy[0],xy[1]);
  // load middle value
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fn[imem] = fnow[ik];
    // fn[imem] = j;
  }
}

// one time step of the LBM scheme
__kernel void time_step(__global const real *fn, __global real *fnp1) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real fnow[_N];

  // shift of values in domain
  for (int d = 0; d < 4; d++) {
    int iR = (i - dir[d][0] + _NX) % _NX;
    int jR = (j - dir[d][1] + _NY) % _NY;

    for (int iv = 0; iv < _M; iv++) {
      int ik = d * _M + iv;
      int imem = iR + jR * _NX + ik * ngrid;
// #ifdef dirichlet_updown
//       // dirichlet condition on up and down borders
//       // (values of border cells are unchanged)
//       if ((j == 0) || (j == _NY - 1)) {
//         imem = i + j * _NX + ik * ngrid;
//       }
// #elif defined dirichlet_leftright
//       // dirichlet condition on left and right borders
//       // (values of border cells are unchanged)
//       if ((i == 0) || (i == _NX - 1)) {
//         imem = i + j * _NX + ik * ngrid;
//       }
// #elif defined dirichlet
//       // dirichlet condition on all borders
//       // (values of border cells are unchanged)
//       if ((i == 0) || (i == _NX - 1) || (j == 0) || (j == _NY - 1)) {
//         imem = i + j * _NX + ik * ngrid;
//       }
// #endif
      fnow[ik] = fn[imem];
    }
  }

  real wnow[_M];
  f2w(fnow, wnow);

  real fnext[_N];
  // first order relaxation
  w2f(wnow, fnext);

  // second order relaxation
  for (int iv = 0; iv < _M; iv++) {
    for (int d = 0; d < 4; d++) {
      int ik = d * _M + iv;
      if (iv == ePsi) {
        fnext[ik] = 1.0_F * fnext[ik];
      } else {
        fnext[ik] = 1.9_F * fnext[ik] - 0.9_F * fnow[ik];
        // fnext[ik] = 2.0_F * fnext[ik] - 1.0_F * fnow[ik];
        // fnext[ik] = 1.0_F * fnext[ik] - 0.0_F * fnow[ik];
      }
    }
  }

  // save
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fnp1[imem] = fnext[ik];
    // fnp1[imem] = fnow[ik];
  }
}

// numerical divergence of B
__kernel void div_b(__global real *fn, __global real *div_b) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real fR[_N];

  float flux[_M];

  // loop on all directions:
  // idir = 0 (east)
  // idir = 1 (west)
  // idir = 2 (north)
  // idir = 3 (south)
  div_b[i + j * _NX] = 0;
  for (int idir = 0; idir < 4; idir++) {
    float vn[3];
    vn[eX] = dir[idir][0];
    vn[eY] = dir[idir][1];
    vn[eZ] = 0;
    int iR = (i + dir[idir][0] + _NX) % _NX;
    int jR = (j + dir[idir][1] + _NY) % _NY;
    // load neighbour values
    float wR[_M];
    for (int ik = 0; ik < _N; ik++) {
      int imem = ik * ngrid + iR + jR * _NX;
      fR[ik] = fn[imem];
    }
    f2w(fR, wR);

    real bn = wR[eBx] * vn[eX] + wR[eBy] * vn[eY] + wR[eBz] * vn[eZ];
    // flux_phy(wR, vn, flux);

    // update div b
    div_b[i + j * _NX] += bn / 2 / ds[idir];
  }
}

// kinetic energy
__kernel void kinetic(__global real *fn, __global real *kin) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;
  int ngrid = _NX * _NY;
  int NLOC = _NX;
  if (_NX > _NCPR) {
    NLOC = _NCPR;
  }

  real floc[_N];

  int ratio = _NX / NLOC; // assume that _NX = _NY
  int ngridnew = NLOC * NLOC;

  // save
  if ((i % ratio == 0) && (j % ratio == 0)) {
    int inew = i / ratio;
    int jnew = j / ratio;
    int imemnew = inew + jnew * NLOC;
    kin[imemnew] = 0.0;

    for (int iloc = i; iloc < i + ratio; iloc++) {
      for (int jloc = j; jloc < j + ratio; jloc++) {

        for (int ik = 0; ik < _N; ik++) {
          int imem = iloc + jloc * _NX + ik * ngrid;
          floc[ik] = fn[imem];
        }
        float wn[_M];
        f2w(floc, wn);
        real kinloc = wn[eU] * wn[eU];
        kinloc += wn[eV] * wn[eV];
        kinloc *= 0.5 / wn[eRho];
        kin[imemnew] += 1.0 / (ratio * ratio) * kinloc;  /// bug: race condition
      }
    }
  }
}

// compression for output files (if resolution on too large grid)
__kernel void compress(__global const real *fn, __global real *fncpr) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  int ratio = _NX / _NCPR; // assume that _NX = _NY
  int ngridnew = _NCPR * _NCPR;

  // save
  if ((i % ratio == 0) && (j % ratio == 0)) {
    int inew = i / ratio;
    int jnew = j / ratio;
    for (int ik = 0; ik < _N; ik++) {
      int imemnew = inew + jnew * _NCPR + ik * ngridnew;
      fncpr[imemnew] = 0.0;
      for (int iloc = i; iloc < i + ratio; iloc++) {
        for (int jloc = j; jloc < j + ratio; jloc++) {
          int imem = iloc + jloc * _NX + ik * ngrid;
          fncpr[imemnew] += 1.0 / (ratio * ratio) * fn[imem];  /// bug: race condition...
        }
      }
    }
  }
}
