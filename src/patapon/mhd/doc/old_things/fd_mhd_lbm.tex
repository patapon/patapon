% !TeX document-id = {b2f719dd-15e9-4da9-bfb8-3275215cc85d}
%% LyX 2.3.0 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
% !TeX TXS-program:compile = txs:///lualatex/[--shell-escape]

\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\setlength{\parskip}{\smallskipamount}
\setlength{\parindent}{0pt}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}

\makeatletter

\newcommand{\bu}{\mathbf{u}}
\newcommand{\bZero}{\mathbf{0}}
\newcommand{\bB}{\mathbf{B}}
\newcommand{\bC}{\mathbf{C}}
\newcommand{\Ekin}{E_{\text{kin}}}
\newcommand{\Scipy}{\texttt{Scipy}}
\newcommand{\linregress}{\texttt{linregress}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\newcommand{\lyxaddress}[1]{
	\par {\raggedright #1
	\vspace{1.4em}
	\noindent\par}
}

\makeatother

\usepackage{babel}

\usepackage{todonotes}



% TiKZ
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{placeins}

\usetikzlibrary{calc}
\usetikzlibrary{patterns}
%\usetikzlibrary{external}


%\tikzexternalize[prefix=tikz-to-pdf/]

%\makeatletter
%\renewcommand{\todo}[2][]{\tikzexternaldisable\@todo[#1]{#2}\tikzexternalenable}
%\makeatother
\newcommand{\todofd}[1][]{
	\todo[backgroundcolor=green!50!blue!20, bordercolor=green!50!blue, linecolor=green!50!blue!20, #1]
}
\newcommand{\todoph}[1][]{
	\todo[backgroundcolor=orange!80!yellow!20, bordercolor=orange!80!yellow, linecolor=orange!80!yellow!20, #1]
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%====================================================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{A kinetic method for solving the MHD equations. Application to the
computation of tilt instability on uniform fine meshes.}

\author{Hubert Baty, Florence Drui, Philippe Helluy, Christian Klingenberg, Lukas Tannhaüser}
\maketitle

\lyxaddress{Observatoire de Strasbourg, IRMA, Inria Tonus, University of Würzburg}
\begin{abstract}
This paper is devoted to the simulation of MHD flows with complex
structures. This kind flows present instabilities that generate shock
waves. We propose a robust and precise numerical method based on the
Lattice Boltzmann methodology. This method can handle shock wave and
is almost second order. It is also very well adapted to GPU computing.
We also give results for a tilt instability test case on very fine
meshes. 
\end{abstract}

\section{Introduction}

\todofd[inline]{
	\protect\begin{itemize}
		\protect\item objectives of the work: perform high-resolution simulations (ensuring also stability) of MHD physics with
		a simple approach and massively parallel code.
	\protect\end{itemize}
	}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mathematical model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\global\long\def\v#1{\mathbf{#1}}

We consider the MHD equations with divergence cleaning
\[
\partial_{t}\left({\begin{array}{c}
\rho\\
{\rho{\mathbf{u}}}\\
Q\\
{\mathbf{B}}\\
\psi
\end{array}}\right)+\nabla\cdot\left({\begin{array}{c}
{\rho{\mathbf{u}}}\\
{\rho{\mathbf{u}}\otimes{\mathbf{u}}+(p+\frac{{{\mathbf{B}}\cdot{\mathbf{B}}}}{2})\mathbf{I}-{\mathbf{B}}\otimes{\mathbf{B}}}\\
{(Q+p+\frac{{{\mathbf{B}}\cdot{\mathbf{B}}}}{2}){\mathbf{u}}-({\mathbf{B}}\cdot{\mathbf{u}}){\mathbf{B}}}\\
{{\mathbf{u}}\otimes{\mathbf{B}}-{\mathbf{B}}\otimes{\mathbf{u}}}+\psi\mathbf{I}\\
c_{h}^{2}{\mathbf{B}}
\end{array}}\right)=\left(\begin{array}{c}
0\\
0\\
0\\
0\\
0
\end{array}\right).
\]
The velocity and magnetic field are noted 
\[
\v u=(u_{1},u_{2},u_{3})^{T},\quad\v B=(B_{1},B_{2},B_{3})^{T},
\]
the pressure
\[
p=(\gamma-1)(Q-\rho\frac{\v u\cdot\v u}{2}-\frac{\v B\cdot\v B}{2}).
\]

The other variables are the density $\rho$, the total energy $Q$,
the divergence cleaning potential $\psi$. The velocity $c_{h}>0$
is here to ensure that
\[
\nabla\cdot\v B\simeq0.
\]
It has to be larger than all the wave speeds of the MHD system (see
\cite{dedner2002hyperbolic}).

We introduce the conservative variables
\[
\v w=\v w(\v x,t)=\left({\begin{array}{c}
\rho\\
{\rho{\mathbf{u}}}\\
Q\\
{\mathbf{B}}\\
\psi
\end{array}}\right)\in\mathbb{R}^{m},\quad m=9.
\]
and the flux ($\v n$ is a vector of $\mathbb{R}^{3})$
\[
\v F(\v w,\v n)=\left(\begin{array}{c}
\rho\mathbf{u}\cdot\v n\\
\rho\mathbf{u}\cdot\v n\mathbf{u}+(p+\frac{\mathbf{B}\cdot\mathbf{B}}{2})\mathbf{n}-\mathbf{B}\cdot\v n\mathbf{B}\\
(Q+p+\frac{\mathbf{B}\cdot\mathbf{B}}{2})\mathbf{u}\cdot\v n-(\mathbf{B}\cdot\mathbf{u})\mathbf{B}\cdot\v n\\
\mathbf{u}\cdot\v n\mathbf{B}-\mathbf{B}\cdot\v n\mathbf{u}+\psi\mathbf{n}\\
c_{h}^{2}\mathbf{B}\cdot\v n
\end{array}\right)\in\mathbb{R}^{m}.
\]
In this work, we assume that all the fields do not depend on the $x_{3}$
space variable. We are thus computing two-dimensional solutions. If
we set
\[
\v n_{1}=(1,0,0)^{T},\quad\v n_{2}=(0,1,0)^{T},\quad\v F_{1}=\v F(\v w,\v n_{1}),\quad\v F_{2}=\v F(\v w,\v n_{2}),
\]
the MHD can also be written as a two-dimensional system of nine conservation
laws
\begin{equation}
\partial_{t}\v w+\partial_{1}\v F_{1}+\partial_{2}\v F_{2}=0.\label{eq:mhd}
\end{equation}

For more details, see \cite{dedner2002hyperbolic}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Divergence cleaning}
\label{sec:div-clean}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\todofd[inline]{
	talk about the divergence-free constraint and divergence cleaning here ;
	say that the non-zero waves of $\psi$ are moving away at velocity
	$c_h$ \cite{dedner2002hyperbolic} ; analysis of the system wave structure \\
	also used in \cite{dumbserJCP2016}
	}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Kinetic interpretation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\todofd[inline]{
	some bibliography words on relaxation schemes and kinetic vectorial
	schemes...
	}

We consider a real number $\lambda>0$ and four velocities $\v v_{k}=(v_{k}^{1},v_{k}^{2})^{T}$,
$k=1\ldots4,$ defined by 
\[
\v v_{1}=\left(\begin{array}{c}
-\lambda\\
0
\end{array}\right),\quad\v v_{2}=\left(\begin{array}{c}
\lambda\\
0
\end{array}\right),\quad\v v_{3}=\left(\begin{array}{c}
0\\
-\lambda
\end{array}\right),\quad\v v_{4}=\left(\begin{array}{c}
0\\
\lambda
\end{array}\right).
\]
We have four vectorial distribution functions $\v f_{k}(\v x,t)\in\mathbb{R}^{m}$,
$k=1\ldots4$. The conservative variable $\v w$ is related to the
kinetic data by
\[
\v w=\sum_{k=1}^{4}\v f_{k}.
\]
The kinetic system reads (with $\tau>0$)
\begin{equation}
\partial_{t}\v f_{k}+\v v_{k}\cdot\nabla\v f_{k}=\frac{1}{\tau}(\v f_{k}^{eq}-\v f_{k}),\quad k=1\ldots4.\label{eq:kin}
\end{equation}
Equilibrium function
\[
\v f_{1}^{eq}(\v w)=\frac{1}{4}\v w-\frac{1}{2\lambda}\v F_{1}(\v w),\quad\v f_{2}^{eq}(\v w)=\frac{1}{4}\v w+\frac{1}{2\lambda}\v F_{1}(\v w),
\]
\[
\v f_{3}^{eq}(\v w)=\frac{1}{4}\v w-\frac{1}{2\lambda}\v F_{2}(\v w),\quad\v f_{4}^{eq}(\v w)=\frac{1}{4}\v w+\frac{1}{2\lambda}\v F_{2}(\v w).
\]
One can check that
\begin{equation}
\v w=\sum_{k=1}^{4}\v f_{k}^{eq},\quad\v F_{i}=\sum_{k=1}^{4}v_{k}^{i}\v f_{k}^{eq}.\label{eq:maxwellian}
\end{equation}
When the relaxation time $\tau\to0$ then from (\ref{eq:kin}) we
see that
\[
\v f_{k}\simeq\v f_{k}^{eq},
\]
and thus from (\ref{eq:maxwellian}) we recover the MHD equations
(\ref{eq:mhd}).

\todoph[inline]{Question: perform a Chapman-Enskog analysis of (\ref{eq:kin}) as
in \cite{aregba2000discrete}. Verify that if $\lambda$ is large
enough then the model is stable.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical method}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For solving (\ref{eq:kin}) numerically we first construct a regular
grid of the square
\[
\Omega=]0,L[\times]0,L[.
\]
The space step
\[
\Delta x=\frac{L}{N}.
\]
Grid points
\[
\v x_{i,j}=\left(\begin{array}{c}
(i+\frac{1}{2})\Delta x\\
(j+\frac{1}{2})\Delta x
\end{array}\right),\quad i,j\in\frac{\mathbb{Z}}{N\mathbb{Z}}.
\]
We assume periodic conditions, therefore 
\[
i+N=i,\quad j+N=j.
\]
We denote by $\v w_{i,j}^{n}$ and $\v f_{k,i,j}^{n}$ the approximation
of $\v w$ and $\v f_{k}$ at the grid points $\v x_{i,j}$ and time
$t_{n}=n\Delta t$. For solving the kinetic system (\ref{eq:kin})
we treat separately the transport and the relaxation terms.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Transport solver}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The first method is to assume that the time step satisfies
\[
\Delta t=\frac{\Delta x}{\lambda}.
\]
The characteristic method gives
\[
\v f_{k}(\v x,t+\Delta t)=\v f_{k}(\v x-\Delta t\v v_{k},t).
\]
With the above choice of time step the transport is a simple shift
\[
\v f_{1,i,j}^{n+1,-}=\v f_{1,i+1,j}^{n},\quad\v f_{2,i,j}^{n+1,-}=\v f_{2,i-1,j}^{n},\quad\v f_{3,i,j}^{n+1,-}=\v f_{3,i,j+1}^{n},\quad\v f_{4,i,j}^{n+1,-}=\v f_{4,i,j-1}^{n}.
\]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Relaxation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

At the end of the transport step, we can compute the conservative
data
\[
\v w_{i,j}^{n+1}=\sum_{k=1}^{4}\v f_{k,i,j}^{n+1,-}.
\]

If $\tau\to0$, one method would be to write
\[
\v f_{k,i,j}^{n+1}=\v f_{k}^{eq}(\v w_{i,j}^{n+1}).
\]

In \cite{dellar2013interpretation} it is shown that it is more precise
to write

\[
\v f_{k,i,j}^{n+1}=2\v f_{k}^{eq}(\v w_{i,j}^{n+1})-\v f_{k,i,j}^{n+1,-}.
\]

It is possible to add a small dissipation by keeping $\tau>0.$ See
\cite{graille2014approximation} or \cite{coulette2016palindromic}
for the relaxation scheme in the case $\tau>0.$
This dissipation provides a better stability in numerical cases, when
sharp fronts are generated. 

Thus, one can show that the relaxation step reads
\[
\v f_{k,i,j}^{n+1}= \omega \v f_{k}^{eq}(\v w_{i,j}^{n+1})- (\omega -1)\v f_{k,i,j}^{n+1,-},
\]
where $\omega = \frac{2 \Delta t}{2 \tau + \Delta t}$. Typically, one will 
choose $\omega = 1.9$ in our simulations.

\todofd[inline]{
		talk about differencing the values of $\omega$ acording to the 
		macro variables (for instance using $\omega = 1$ for variable $\psi$...)
	}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Boundary conditions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\todofd[inline]{
	\protect\begin{itemize}
		\protect\item periodic boundary conditions,
		\protect\item Dirichlet boundary conditions
	\protect\end{itemize}	
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical application}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{GPU implementation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\todofd[inline]{
	\protect\begin{itemize}
		\protect\item Python and OpenCL,
		\protect\item data structures,
		\protect\item some examples of computation times
		\protect\item performance --> made on the smooth vortex
	\protect\end{itemize}
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Smooth vortex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\todofd[inline]{
	\protect\begin{itemize}
		\protect\item objective of the test: performance (see previous section), \\ study of the convergence order
		of the scheme, \\ influence of omega, \\ influence of the divergence cleaning?
	\protect\end{itemize}
}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Orszag-Tang vortex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\todofd[inline]{
	\protect\begin{itemize}
		\protect\item presentation of the test case (references, studies, results),
		\protect\item initial and boundary conditions,
		\protect\item numerical illustrations for one grid at times $0.1$, $0.2$, $0.3$, $0.4$, $0.5$,
		\protect\item visual comparisons between grids,
		%\protect\item convergence study at time $t=0.1$ (before singularities appear)
	\protect\end{itemize}
}

The Orszag Tang test case \cite{orszag1979small,dahlburg89orsag,picone91orszag} is often used to test a numerical method \cite{TODO}. 
It consists in a vortex system where turbulent structures and shocks develop.



\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{orszag-rho_1024-2048}
		%\input{tikz/orszag-rho_1024-2048.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/orszag-rho_1024-2048.pdf}
		%\tikzsetnextfilename{orszag-rho_1024-4096}
		%\input{tikz/orszag-rho_1024-4096.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/orszag-rho_1024-4096.pdf}
		\caption{Snapshots of $\rho$ for the Orszag-Tang configuration recorded at times $t=0.1$ s and $t=0.2$ s. Grid size is $Nx\times Ny=1024\times1024$.}
		\label{fig:orszag-1}
	\end{center}
\end{figure}


\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{orszag-rho_1024-6144}
		%\input{tikz/orszag-rho_1024-6144.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/orszag-rho_1024-6144.pdf}
		%\tikzsetnextfilename{orszag-rho_1024-8192}
		%\input{tikz/orszag-rho_1024-8192.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/orszag-rho_1024-8192.pdf}
		\caption{Snapshots of $\rho$ for the Orszag-Tang configuration recorded at times $t=0.3$ s and $t=0.4$ s. Grid size is $Nx\times Ny=1024\times1024$.}
		\label{fig:orszag-2}
	\end{center}
\end{figure}


\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{orszag-rho_256-2560}
		%\input{tikz/orszag-rho_256-2560.tikz}
		\includegraphics[width=0.7\linewidth]{tikz-to-pdf/orszag-rho_256-2560.pdf}
		%\tikzsetnextfilename{orszag-rho_1024-10240}
		%\input{tikz/orszag-rho_1024-10240.tikz}
		\includegraphics[width=0.7\linewidth]{tikz-to-pdf/orszag-rho_1024-10240.pdf}
		%\tikzsetnextfilename{orszag-rho_4096-40960}
		%\input{tikz/orszag-rho_4096-40960.tikz}
		\includegraphics[width=0.7\linewidth]{tikz-to-pdf/orszag-rho_4096-40960.pdf}
		\caption{Snapshots of $\rho$ for the Orszag-Tang configuration recorded at time $t=0.5$ s and $t=0.2$ s. Grid sizes are $Nx\times Ny= 256\times256, \, 1024\times1024$ and $4096\times4096$.}
		\label{fig:orszag-3}
	\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Tilt instability}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The tilt instability has been studied in \cite{richard1990tilt,strauss1998adaptive,lankalapalli2007adaptive} 
and consists of two magnetic islands aligned vertically with currents of opposite directions. These islands 
may be kept in an unstable equilibrium through a strong magnetic field. However, when perturbed,
an instability develops that leads the islands to be aligned horizontally and to be expelled away.
The tilt instability is illustrated for a simulation on a $1024\times1024$
grid in Figures \ref{fig:current-1} to \ref{fig:current-3} for different times and the numerical 
approach presented above. A detailed description of the different stages of this instability can
be found in \cite{richard1990tilt}.
In \cite{richard1990tilt}, the study of the tilt instability was also made for a compressible MHD system,
while in \cite{strauss1998adaptive,lankalapalli2007adaptive} the equations are incompressible, 
leading to slightly different results.

In the following, we will consider a squared spatial domain of dimensions $\mathcal{D} = [-3,3] \times [-3,3]$ and $r^2 = x^2 + y^2$.
The initial condition for the equilibrium is given by:
\begin{align*}
	\rho & = 1.0, \quad
	\bu  = \bZero, \quad
	p_0  = 1.0, \quad \psi = 0, \\
	k & = 3.8317059702, \quad
	K  = \frac{-2}{k J_0(k)}, \quad
	\varphi  = K J_1(kr)\frac{y}{r}, \\
	p & = \begin{cases}
		p_0 & \text{if} \quad r\geq 1, \\
		p_0 + \frac{k^2}{2} \varphi^2 & \text{else}, \\
	\end{cases} \\
	B_1 & = \begin{cases}
		\frac{x^2 - y^2}{r^4} - 1 & \text{if} \quad r\geq 1, \\
		K \left[ \frac{ky^2}{r^2} J_0(kr) + \frac{x^2-y^2}{r^3}J_1(kr) \right] & \text{else}, \\
		\end{cases} \\
	B_2 & = \begin{cases}
		\frac{2xy}{r^4}  & \text{if} \quad r\geq 1, \\
		-K \left[ \frac{kxy}{r^2} J_0(kr) - \frac{2xy}{r^3}J_1(kr) \right]  & \text{else}, \\
	\end{cases}
\end{align*}
where $J_0$ and $J_1$ are the Bessel functions of the first kind of orders zero and one respectively.
A perturbation of this equilibrium initially reads:
\[
	\bu = 2 \epsilon \exp{(-r^2)} \left( 
	\begin{matrix}
		-y \\
		x \\
		0
	\end{matrix}
	\right),
\]
with $\epsilon=1.0\cdot10^{-3}$.
\todofd[inline]{
	describe (Dirichlet) boundary conditions and how they are implemented: do never change the boundary cells
}
Moreover, for the analysis of the simulation results, we recall that the current density is given by:
\[
	\bC = \nabla \times \bB,
\]
and that we are interested in the third component of $\bC$, \textit{i.e.} $C_3$. We will also look
at the kinetic energy of the system, which is computed as the integral over the spatial domain
of the kinetic energy density:
\[
	\Ekin = \int_{\mathcal{D}} \frac{1}{2} \rho |\bu|^2.
\]
Finally, the numerical scheme involves the following parameters:
\[
	\Delta x = L_x/N_x, \quad c_h = 6 \quad \lambda = 20, \quad \Delta t = \Delta x / \lambda,
\]
and $\omega = 1.9$ for all variables except from the kinetic variables associated with $\psi$ where
$\omega = 1$ (see section \ref{sec:div-clean-results}). In our numerical tests, we have noticed
that choosing $\omega = 2$ leads to an unstable numerical solution, due to the lack of dissipation.


\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{current_1024-3405}
		%\input{tikz/current_1024-3405.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current_1024-3405.pdf}
		%\tikzsetnextfilename{current_1024-6810}
		%\input{tikz/current_1024-6810.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current_1024-6810.pdf}
		\caption{Snapshots of the magnetic current density $C_3$ recorded at times $t=1$ s and $t=2$ s. Grid size is $Nx\times Ny=1024\times1024$.}
		\label{fig:current-1}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{current_1024-10215}
		%\input{tikz/current_1024-10215.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current_1024-10215.pdf}
		%\tikzsetnextfilename{current_1024-13620}
		%\input{tikz/current_1024-13620.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current_1024-13620.pdf}
		\caption{Snapshots of the magnetic current density $C_3$ recorded at times $t=3$ s and $t=4$ s. Grid size is $Nx\times Ny=1024\times1024$.}
		\label{fig:current-2}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{current_1024-17025}
		%\input{tikz/current_1024-17025.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current_1024-17025.pdf}
		%\tikzsetnextfilename{current_1024-20430}
		%\input{tikz/current_1024-20430.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current_1024-20430.pdf}
		\caption{Snapshots of the magnetic current density $C_3$ recorded at times $t=5$ s and $t=6$ s. Grid size is $Nx\times Ny=1024\times1024$.} 
		\label{fig:current-3}
	\end{center}
\end{figure}


%====================================================================================================
\subsubsection*{Current sheets and current peak}
%====================================================================================================

Currents sheets develop at the edges of the magnetic islands.
The width of these current sheets as well as the maximum intensity of the
current density depend on the numerical resolution, as studied in \cite{lankalapalli2007adaptive},
where specific adaptive resolution (adaptive grid and order of resolution) 
was performed near strong current gradients.  In Figure \ref{fig:current-zoom}, we show an enlargement of the 
part of the spatial domain that lies around the current sheet lying rightside for
simulations with different grid sizes and at time $t=6$ s. The finest current structures
are obtained with the finest grid (here the $4096\times4096$ grid). In Figure \ref{fig:current-conv}, 
we present the ratio of the maximum current density $C_{3,max}$ over the initial current density $C_{3,0}$.
This ratio increases with the numerical resolution and should be infinite in the case of a totally ideal MHD
system (\cite{lankalapalli2007adaptive}). The maximum ratio we can obtain is $14.2$ with the 
$7000\times7000$ grid. In comparison, \cite{lankalapalli2007adaptive} obtained a ratio of $5$
for the simulation with $32,768$ triangles and first-order method, and a ratio of $41$ for 
adaptive grids near the current sheets. In conclusion, our method seems to have a higher numerical
resistivity, which limits the maximum current peak.

\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{current-zoom_256-5096}
		%\input{tikz/current-zoom_256-5096.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current-zoom_256-5096.pdf}
		%\tikzsetnextfilename{current-zoom_1024-20430}
		%\input{tikz/current-zoom_1024-20430.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current-zoom_1024-20430.pdf}
		%\tikzsetnextfilename{current-zoom_4096-81900}
		%\input{tikz/current-zoom_4096-81900.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current-zoom_4096-81900.pdf}
		\caption{Snapshots of the magnetic current density recorded at time $t=6$ s in a spatial zone around the current sheets. Grid sizes are $Nx\times Ny=256\times256$, $Nx\times Ny=1024\times1024$, $Nx\times Ny=4096\times4096$.}
		\label{fig:current-zoom}
	\end{center}
\end{figure}



\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{current_conv-dirichlet}
		%\input{tikz/current_conv-dirichlet.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/current_conv-dirichlet.pdf}
		\caption{Ratio of the peak current and the initial current density for simulation grids 
		ranging from $128\times128$ to $7000\times7000$.}
		\label{fig:current-conv}
	\end{center}
\end{figure}


%====================================================================================================
\subsubsection*{Kinetic energy growth rate}
%====================================================================================================


During the development of the instability, the kinetic energy of the system increases 
exponentially. The growth rate of the kinetic energy has been studied in 
\cite{richard1990tilt} and \cite{lankalapalli2007adaptive}, where different values
have been found. In \cite{richard1990tilt}, two simulations with two values
of $\beta$ (the ratio between the hydrodynamic and the magnetic pressures) give growth
rate values of 1.44 for low $\beta$ case and of 1.27 for high $\beta$ case. In \cite{lankalapalli2007adaptive},
the study is focused on the convergence of the kinetic energy growth rate according to 
the grid and the order of the numerical method. The converged value for the growth rate
is near 1.3 for an initial perturbation $\epsilon = 1.0\cdot10^{-3}$.
\todofd[inline]{
	try to explain the causes of the different growth rates...
}

The simulations of the tilt instability that we perform are post-processed at regular time intervals.
In Figures \ref{fig:kinetic-1} and \ref{fig:kinetic-2}, we show the time evolution of the total kinetic energy 
$\Ekin (t)$. One can note that the growth of the kinetic energy happens in two stages: in a first stage, 
the growth is faster than exponentially, then slows down and after two seconds the second stage begins
with exponential growth. When the numerical resolution is better, the first stage tends to vanish and
the second stage begins sooner.
\todofd[inline]{
	comment the curves, especially the first part of the simulation
	where an important growth of the kinetic energy occurs for coarser
	grids. This effect seems to be attenuated for finer grids. \\
	moreover, the finer the grid, the sooner the exponential growth happens \\
	try to explain why....
	}
	
In a logarithmic scale, the linear regression of the kinetic energy growth is performed
with the \Scipy{} function \linregress{} and the regression line is illustrated in Figures \ref{fig:kinetic-1} and \ref{fig:kinetic-2}.
The results of the linear regression are reported in Table \ref{tab:kinetic} and Figure \ref{fig:kinetic-2}.
The converged growth rate is close to 1.45 for our simulations. It is higher than the results of \cite{richard1990tilt} 
and \cite{lankalapalli2007adaptive}.
\todofd[inline]{
	comment the convergence results
	}



\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{kinetic_512-dirichlet}
		%\input{tikz/kinetic_512-dirichlet.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/kinetic_512-dirichlet.pdf}
		%\tikzsetnextfilename{kinetic_2048-dirichlet}
		%\input{tikz/kinetic_2048-dirichlet.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/kinetic_2048-dirichlet.pdf}
		\caption{Growth of the kinetic energy during the simulation.
			Measures that have been accounted for for the linear regression
			are identified with green marks and the regression line is in red. Results for grid sizes $Nx \times Ny = 512\times512$ and $Nx \times Ny = 2048\times2048$.}
		\label{fig:kinetic-1}
	\end{center}
\end{figure}


\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{kinetic_7000-dirichlet}
		%\input{tikz/kinetic_7000-dirichlet.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/kinetic_7000-dirichlet.pdf}
		%\tikzsetnextfilename{kinetic_conv-dirichlet}
		%\input{tikz/kinetic_conv-dirichlet.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/kinetic_conv-dirichlet.pdf}
		\caption{Growth of the kinetic energy during the simulation.
			Measures that have been accounted for for the linear regression
			are identified with green marks and the regression line is in red. Results for grid size $Nx \times Ny = 7000\times7000$.
			Growth rates from $Nx = Ny = 128$ ($16384$ degrees of freedom) to $Nx = Ny = 7000$ ($49\times10^6$ degrees of freedom).}
		\label{fig:kinetic-2}
	\end{center}
\end{figure}


In table \ref{tab:kinetic}, we summarize the characteristics
of the linear regression for each mesh size.

\begin{table}
\begin{tabular}{|l|c|c|c|c|c|c|c|}
	\hline
	Mesh size & $128^2$ & $256^2$ & $512^2$ & $1024^2$ & $2048^2$ & $4096^2$ & $7000^2$ \\
	\hline
	Regression range (s) & $[5.6, 6.7]$ & $[4.9, 5.9]$ & $[4.3, 5.3]$ & $[3.9, 4.9]$ & $[3.7, 4.7]$ & $[3.0, 4.5]$ & $[3.0, 4.5]$  \\
	\hline
	Growth rate & 0.390 & 0.904 & 1.082 & 1.330 & 1.403 & 1.433 & 1.450 \\
	\hline
\end{tabular}
	\caption{Characteristics of the linear regression for the kinetic
		energy growth rate for each mesh size: range of values that have
		been accounted for and growth rate.}
	\label{tab:kinetic}
\end{table}




%====================================================================================================
\subsubsection*{Divergence cleaning effect}
\label{sec:div-clean-results}
%====================================================================================================

\todofd[inline]{
	test without divergence cleaning and compare the results \\
	comparison of all results ?
}

In order to ensure a magnetic field which is close to a divergence-free
field, we have presented the divergence cleaning procedure in section \ref{sec:div-clean}. Here we compare two strategies for the numerical 
resolution of the divergence cleaning equation:
\begin{enumerate}
	\item the kinetic variables associated with the macro-variable $\psi$
	are solved numerically like any other kinetic variable. This means
	that in the relaxation step, the relaxation coefficient $\omega$ is set
	to $\omega = 1.9$ and the order of resolution is close to 2,
	\item during the relaxation step, the kinetic variables associated with the macro-variable $\psi$ are relaxed with a coefficient $\omega = 1$, which comes down to set 
	\begin{align*}
	\v f_{k,i,j}^{n+1} &= \omega \v f_{k}^{eq}(\v w_{i,j}^{n+1})- (\omega -1)\v f_{k,i,j}^{n+1,-} \\
		& =  \v f_{k}^{eq}(\v w_{i,j}^{n+1}).
	\end{align*}
	Now, the order of resolution is $1$ and the waves associated with the perturbations of the divergence-free constraint are better damped than
	with $\omega=1.9$.
\end{enumerate}
These two options are compared in Figures \ref{fig:divB-1} to \ref{fig:divB-3} for different times. At time $t=0.5$ s, one can 
notice the effects of the boundary conditions that have propagated 
towards the center of the domain. The perturbations of the divergence-free
constraint have been more attenuated with the second strategy than with
the first one. At time $t=1$ s, the divergence-free constraint is mainly
violated at the edges of the magnetic vortices and close to the domain
boundaries. Once more, the results are better with the second divergence
cleaning strategy. Finally, at time $t=5$ s, when the vortices have begun
to align, the divergence-free constraint is strongly perturbed, but results
are still better with the second strategy.


\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{divB-uniform-omega_1024-1816}
		%\input{tikz/divB-uniform-omega_1024-1816.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/divB-uniform-omega_1024-1816.pdf}
		%\tikzsetnextfilename{divB_1024-1816}
		%\input{tikz/divB_1024-1816.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/divB_1024-1816.pdf}
		\caption{Snapshots of the divergence of the magnetic field recorded at times $t=0.5$ s for both strategies of divergence cleaning. Grid size is $Nx\times Ny=1024\times1024$.}
		\label{fig:divB-1}
	\end{center}
\end{figure}


\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{divB-uniform-omega_1024-3405}
		%\input{tikz/divB-uniform-omega_1024-3405.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/divB-uniform-omega_1024-3405.pdf}
		%\tikzsetnextfilename{divB_1024-3405}
		%\input{tikz/divB_1024-3405.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/divB_1024-3405.pdf}
		\caption{Snapshots of the divergence of the magnetic field recorded at times $t=1$ s for both strategies of divergence cleaning. Grid size is $Nx\times Ny=1024\times1024$.}
		\label{fig:divB-2}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		%\tikzsetnextfilename{divB-uniform-omega_1024-17025}
		%\input{tikz/divB-uniform-omega_1024-17025.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/divB-uniform-omega_1024-17025.pdf}
		%\tikzsetnextfilename{divB_1024-17025}
		%\input{tikz/divB_1024-17025.tikz}
		\includegraphics[width=0.8\linewidth]{tikz-to-pdf/divB_1024-17025.pdf}
		\caption{Snapshots of the divergence of the magnetic field recorded at times $t=5$ s for both strategies of divergence cleaning. Grid size is $Nx\times Ny=1024\times1024$.}
		\label{fig:divB-3}
	\end{center}
\end{figure}


\todofd[inline]{
	write a conclusion
	}
	
	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\bibliographystyle{plain}
\bibliography{mhd_lbm}

\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%====================================================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
