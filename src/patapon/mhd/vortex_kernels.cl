//  Pieced together from Boost C++ and Cephes by
//  Andreas Kloeckner (C) 2012
//
//  Pieces from:
//
//  Copyright (c) 2006 Xiaogang Zhang, John Maddock
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See
//  http://www.boost.org/LICENSE_1_0.txt)
//
// Cephes Math Library Release 2.8:  June, 2000
// Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
// What you see here may be used freely, but it comes with no support or
// guarantee.

//#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#define real _real_
#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _DT _dt_
#define _M _m_
#define _N _n_
#define _VX _vx_
#define _VY _vy_
#define _NCPR _ncpr_

#define _LAMBDA _lambda_

#ifndef M_PI
#define M_PI (3.14159265358979323846264338328)
#endif

#define _VOL (_DX * _DY)

#define eRho 0
#define eU 1
#define eE 2
#define eV 3
#define eW 4
#define eBy 5
#define eBz 6
#define eBx 7
#define ePsi 8
#define eP 2

#define eX 0
#define eY 1
#define eZ 2

//#define dirichlet
//#define dirichlet_updown
//#define dirichlet_leftright

void flux_phy(const real *W, const real *vn, real *flux);

__constant int dir[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

__constant real ds[4] = {_DY, _DY, _DX, _DX};

//#define _NEW_DIV_CLEAN

void flux_phy(const real *W, const real *vn, real *flux) {

  real gam = 1.6666666666;

  real un = (W[eU] * vn[eX] + W[eV] * vn[eY] + W[eW] * vn[eZ]) / W[eRho];
  real bn = W[eBx] * vn[eX] + W[eBy] * vn[eY] + W[eBz] * vn[eZ];

  real p =
      (gam - 1) *
      (W[eE] - (W[eU] * W[eU] + W[eV] * W[eV] + W[eW] * W[eW]) / 2 / W[eRho] -
       (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2);

  flux[eRho] = W[eRho] * un;
  flux[eU] =
      un * W[eU] +
      (p + (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2) * vn[eX] -
      bn * W[eBx];
  flux[eE] =
      (W[eE] + p + (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2) *
          un -
      (W[eBx] * W[eU] + W[eBy] * W[eV] + W[eBz] * W[eW]) * bn / W[eRho];
  flux[eV] =
      un * W[eV] +
      (p + (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2) * vn[1] -
      bn * W[eBy];
  flux[eW] =
      un * W[eW] +
      (p + (W[eBx] * W[eBx] + W[eBy] * W[eBy] + W[eBz] * W[eBz]) / 2) * vn[2] -
      bn * W[eBz];

#ifdef _NEW_DIV_CLEAN

  flux[eBy] = -bn * W[eV] / W[eRho] + un * W[eBy];
  flux[eBz] = -bn * W[eW] / W[eRho] + un * W[eBz];
  flux[eBx] = -bn * W[eU] / W[eRho] + un * W[eBx];

  flux[ePsi] = bn;
#else

  flux[eBy] = -bn * W[eV] / W[eRho] + un * W[eBy] + W[ePsi] * vn[eY];
  flux[eBz] = -bn * W[eW] / W[eRho] + un * W[eBz] + W[ePsi] * vn[eZ];
  flux[eBx] = -bn * W[eU] / W[eRho] + un * W[eBx] + W[ePsi] * vn[eX];

  real c_h = 6;

  flux[ePsi] = c_h * c_h * bn;
  // flux[8] = 0.0; //!!!! TEST DEBUG

#endif
}

// equilibrium "maxwellian" from macro data w
void w2f(const real *w, real *f) {
  for (int d = 0; d < 4; d++) {
    real flux[_M];
    real vnorm[3] = {(real)dir[d][0], (real)dir[d][1], (real)0};
    flux_phy(w, vnorm, flux);
    for (int iv = 0; iv < _M; iv++) {
#ifdef _NEW_DIV_CLEAN
      int c = (iv != ePsi);
#else
      int c = 1;
#endif
      f[d * _M + iv] = c * w[iv] / 4 + flux[iv] / 2 / _LAMBDA;
    }
  }
}

// macro data w from micro data f
void f2w(const real *f, real *w) {
  for (int iv = 0; iv < _M; iv++)
    w[iv] = 0;
  for (int d = 0; d < 4; d++) {
    for (int iv = 0; iv < _M; iv++) {
      w[iv] += f[d * _M + iv];
    }
  }
}

// mhd
void conservatives(real *y, real *w) {
  real gam = 1.6666666666;

  w[eRho] = y[eRho];       // rho
  w[eU] = y[eRho] * y[eU]; // rho u
  w[eE] = y[eE] / (gam - 1) +
          y[eRho] * (y[eU] * y[eU] + y[eV] * y[eV] + y[eW] * y[eW]) / 2 +
          (y[eBx] * y[eBx] + y[eBy] * y[eBy] + y[eBz] * y[eBz]) / 2; // rho E
  w[eV] = y[eRho] * y[eV];                                           // rho v
  w[eW] = y[eRho] * y[eW];                                           // rho w
  w[eBy] = y[eBy];                                                   // By
  w[eBz] = y[eBz];                                                   // Bz
  w[eBx] = y[eBx];                                                   // Bx
  w[ePsi] = y[ePsi];                                                 // psi
}

// orszag tang init data
void exact_sol(real *x, real t, real *w) {
  real gam = 1.6666666666;
  real yL[_M];

  yL[eRho] = gam * gam;             // rho
  yL[eU] = -sin(2 * M_PI * x[eY]);  // u
  yL[eP] = gam;                     // p
  yL[eV] = sin(2 * M_PI * x[eX]);   // v
  yL[eW] = 0;                       // w
  yL[eBy] = sin(4 * M_PI * x[eX]);  // By
  yL[eBz] = 0;                      // Bz
  yL[eBx] = -sin(2 * M_PI * x[eY]); // Bx
  yL[ePsi] = 0;                     // psi

  conservatives(yL, w);
}

real gauss(real r) { return exp(-r * r / 2); }

void exact_smooth_vortex(real *x, real t, real *w) {

  const real gam = 1.6666666666;
  const real rref2 = 1;
  const real sigmam2 = 0.5;
  const real kappa = 1;
  const real uref = 0.2;
  const real bref = 0.2;
  const real mu = 1;
  const real udrift[2] = {1.0, 1.0};
  const real xstart[2] = {10., 10.};
  //
  real xrel[3] = {x[0] - uref * udrift[0] * t - xstart[0],
                  x[1] - uref * udrift[1] * t - xstart[1], 0.0};
  //
  real r = sqrt(xrel[0] * xrel[0] + xrel[1] * xrel[1]);
  real expf = exp(sigmam2) * gauss(r);

  // rho
  w[eRho] = 1.0;
  // qx = rho * ux
  w[eU] = uref * (udrift[0] - xrel[1] * kappa * expf);
  // qy = rho * uy
  w[eV] = uref * (udrift[1] + xrel[0] * kappa * expf);
  // Bx
  w[eBx] = -bref * xrel[1] * mu * expf;
  // By
  w[eBy] = bref * xrel[0] * mu * expf;
  // psi
  w[ePsi] = 0.0;
  //
  // real T = 1.0;
  // real e = T/(gam-1.0);
  //
  real p = 1.0 - 0.5 * bref * bref * mu * mu * r * r * expf * expf;
  real qdotq = w[eU] * w[eU] + w[eV] * w[eV];
  real BdotB = w[eBx] * w[eBx] + w[eBy] * w[eBy];
  //
  w[eE] = p / (gam - 1.0) + 0.5 * (qdotq / w[eRho] + BdotB);

  w[eW] = 0.;
  w[eBz] = 0.;
}

// initial condition on the macro data
__kernel void init_sol(__global real *fn) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real wnow[_M];

  real t = 0;
  real xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};

  // exact_sol(xy, t, wnow);
  exact_smooth_vortex(xy, t, wnow);

  real fnow[_N];
  w2f(wnow, fnow);

  // printf("x=%f, y=%f \n",xy[0],xy[1]);
  // load middle value
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fn[imem] = fnow[ik];
    // fn[imem] = j;
  }
}

// one time step of the LBM scheme
__kernel void time_step(__global const real *fn, __global real *fnp1) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real fnow[_N];

  // shift of values in domain
  for (int d = 0; d < 4; d++) {
    int iR = (i - dir[d][0] + _NX) % _NX;
    int jR = (j - dir[d][1] + _NY) % _NY;

    for (int iv = 0; iv < _M; iv++) {
      int ik = d * _M + iv;
      int imem = iR + jR * _NX + ik * ngrid;
      // #ifdef dirichlet_updown
      //       // dirichlet condition on up and down borders
      //       // (values of border cells are unchanged)
      //       if ((j == 0) || (j == _NY - 1)) {
      //         imem = i + j * _NX + ik * ngrid;
      //       }
      // #elif defined dirichlet_leftright
      //       // dirichlet condition on left and right borders
      //       // (values of border cells are unchanged)
      //       if ((i == 0) || (i == _NX - 1)) {
      //         imem = i + j * _NX + ik * ngrid;
      //       }
      // #elif defined dirichlet
      //       // dirichlet condition on all borders
      //       // (values of border cells are unchanged)
      //       if ((i == 0) || (i == _NX - 1) || (j == 0) || (j == _NY - 1)) {
      //         imem = i + j * _NX + ik * ngrid;
      //       }
      // #endif
      fnow[ik] = fn[imem];
    }
  }

  real wnow[_M];
  f2w(fnow + 0, wnow + 0);

  real fnext[_N];
  //first order relaxation
  w2f(wnow + 0, fnext + 0);

  real om = 2;
  // second order relaxation
  for (int iv = 0; iv < _M; iv++) {
    for (int d = 0; d < 4; d++) {
      int ik = d * _M + iv;
      // if (iv == ePsi) {
      // fnext[ik] = 1.0 * fnext[ik];
      fnext[ik] = om * fnext[ik] - (om - 1) * fnow[ik];
      //fnext[ik] = fnow[ik];
      //} else {
      // fnext[ik] = 1.9 * fnext[ik] - 0.9 * fnow[ik];
      // fnext[ik] = 2.0 * fnext[ik] - 1.0 * fnow[ik];
      // fnext[ik] = 1.0 * fnext[ik] - 0.0 * fnow[ik];
      //}
    }
  }

  // save
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fnp1[imem] = fnext[ik];
    //fnp1[imem] = fnow[ik];
  }
}


// one time step of the LBM scheme
__kernel void time_shift(__global const real *fn, __global real *fnp1) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real fnow[_N];

  // shift of values in domain
  for (int d = 0; d < 4; d++) {
    int iR = (i - dir[d][0] + _NX) % _NX;
    int jR = (j - dir[d][1] + _NY) % _NY;

    for (int iv = 0; iv < _M; iv++) {
      int ik = d * _M + iv;
      int imem = iR + jR * _NX + ik * ngrid;
      // #ifdef dirichlet_updown
      //       // dirichlet condition on up and down borders
      //       // (values of border cells are unchanged)
      //       if ((j == 0) || (j == _NY - 1)) {
      //         imem = i + j * _NX + ik * ngrid;
      //       }
      // #elif defined dirichlet_leftright
      //       // dirichlet condition on left and right borders
      //       // (values of border cells are unchanged)
      //       if ((i == 0) || (i == _NX - 1)) {
      //         imem = i + j * _NX + ik * ngrid;
      //       }
      // #elif defined dirichlet
      //       // dirichlet condition on all borders
      //       // (values of border cells are unchanged)
      //       if ((i == 0) || (i == _NX - 1) || (j == 0) || (j == _NY - 1)) {
      //         imem = i + j * _NX + ik * ngrid;
      //       }
      // #endif
      fnow[ik] = fn[imem];
    }
  }

  real fnext[_N];

  // transfer
  for (int iv = 0; iv < _M; iv++) {
    for (int d = 0; d < 4; d++) {
      int ik = d * _M + iv;
      // if (iv == ePsi) {
      // fnext[ik] = 1.0 * fnext[ik];
      fnext[ik] = fnow[ik];
      //fnext[ik] = fnow[ik];
      //} else {
      // fnext[ik] = 1.9 * fnext[ik] - 0.9 * fnow[ik];
      // fnext[ik] = 2.0 * fnext[ik] - 1.0 * fnow[ik];
      // fnext[ik] = 1.0 * fnext[ik] - 0.0 * fnow[ik];
      //}
    }
  }

  // save
  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    fnp1[imem] = fnext[ik];
    //fnp1[imem] = fnow[ik];
  }
}



// numerical divergence of B
__kernel void div_b(__global real *fn, __global real *div_b) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real fR[_N];

  real flux[_M];

  // loop on all directions:
  // idir = 0 (east)
  // idir = 1 (west)
  // idir = 2 (north)
  // idir = 3 (south)
  div_b[i + j * _NX] = 0;
  for (int idir = 0; idir < 4; idir++) {
    real vn[3];
    vn[eX] = dir[idir][0];
    vn[eY] = dir[idir][1];
    vn[eZ] = 0;
    int iR = (i + dir[idir][0] + _NX) % _NX;
    int jR = (j + dir[idir][1] + _NY) % _NY;
    // load neighbour values
    real wR[_M];
    for (int ik = 0; ik < _N; ik++) {
      int imem = ik * ngrid + iR + jR * _NX;
      fR[ik] = fn[imem];
    }
    f2w(fR + 0, wR + 0);

    real bn = wR[eBx] * vn[eX] + wR[eBy] * vn[eY] + wR[eBz] * vn[eZ];
    // flux_phy(wR, vn, flux);

    // update div b
    div_b[i + j * _NX] += bn / 2 / ds[idir];
  }
}

// kinetic error
__kernel void kinetic(const real t, __global real *fn, __global real *kin) {

  int id = get_global_id(0);

  int i = id % _NX;
  int j = id / _NX;
  int ngrid = _NX * _NY;

  real floc[_N];

  // save
  int imemk = i + j * _NX;
  kin[imemk] = 0;

  for (int ik = 0; ik < _N; ik++) {
    int imem = i + j * _NX + ik * ngrid;
    floc[ik] = fn[imem];
  }
  real wn[_M];

  f2w(floc + 0, wn + 0);
  real xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};
  real wnow[_M];
  exact_smooth_vortex(xy, t, wnow);
  // real kinloc = (wn[eU]-wnow[eU]) * (wn[eU]-wnow[eU]);
  real kinloc = fabs(wn[eU] - wnow[eU]);
  // kinloc += wn[eV] * wn[eV];
  // kinloc *= 0.5 / wn[eRho];
  kin[imemk] += kinloc * _DX * _DY;
}
