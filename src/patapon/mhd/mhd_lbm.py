#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Resolution of a transport equation by the finite volume method on regular grid
"""

import pyopencl as cl
import numpy as np
import matplotlib.pyplot as plt
import h5py


from patapon.utils import Figure, load_kernel, get_ite_title

# Default values

# number of conservative variables
_m = 9

# number of kinetic variables
_n = 4 * _m

_ivplot = 2

# grid size
_nx = 256
_ny = 256

_Lx = 1  # 12 #6 #1
_Ly = 1  # 12 #6 #1

# transport velocity
vel = np.array([1., 1.])

_Tmax = 1.


def solve_ocl(m=_m, n=_n, nx=_nx, ny=_ny, Lx=_Lx, Ly=_Ly, Tmax=_Tmax,
              animate=False, precision="single", savekineticdata="False",
              savemacrodata="False", print_source=False, **kwargs):

    dx = Lx / nx
    dy = Ly / ny

    # lattice speed
    vmax = 20.
    # 10.
    vmax = 20.

    # time stepping
    cfl = 1
    dt = cfl * dx / vmax

    # compression for output data
    ncpr = 10000  # 1000 #1024
    testname = "smooth-vortex_"
    #testname = "orszag_"
    #testname = "tilt-p1_"
    foldername = "../h5/"
    ncprx = nx
    ncpry = ny
    if (nx > ncpr):
        ncprx = ncpr
        ncpry = ncpr

    # For plotting
    x = np.linspace(0., Lx, num=ncprx)
    y = np.linspace(0., Ly, num=ncpry)

    parameters = {'nx': nx,
                  'ny': ny,
                  'dx': dx,
                  'dy': dy,
                  'dt': dt,
                  'm': m,
                  'n': n,
                  'vx': vel[0],
                  'vy': vel[1],
                  'lambda': vmax,
                  'ncpr': ncpr,
                  }

    np_real, source = load_kernel("lbm_kernels.cl", parameters,
                                  precision=precision,
                                  print_source=print_source,
                                  module_file=__file__)

    # OpenCL init
    ctx = cl.create_some_context()
    mf = cl.mem_flags

    # compile OpenCL C program
    prg = cl.Program(ctx, source).build(options="")

    # create OpenCL buffers
    buffer_size = 4 * m * nx * ny * np.dtype(np_real).itemsize
    kinetic_buffer_size = ncprx * ncpry * np.dtype(np_real).itemsize
    fn_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    kinetic_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=kinetic_buffer_size)
    fnp1_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    #divb_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)

    if (nx > ncpr):
        fn_cpr_gpu = cl.Buffer(ctx, mf.READ_WRITE,
                               size=(4 * m * ncprx * ncpry * np.dtype(np_real).itemsize))

    # create a queue (for submitting opencl operations)
    queue = cl.CommandQueue(
        ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # init data
    event = prg.init_sol(queue, (nx * ny, ), None, fn_gpu)
    #event = prg.init_nappes(queue, (nx * ny, ), None, fn_gpu)
    #event = prg.init_tilt(queue, (nx * ny, ), None, fn_gpu)
    #event = prg.init_smooth_vortex(queue, (nx * ny, ), None, fn_gpu)
    event.wait()

    event = prg.init_sol(queue, (nx * ny, ), None, fnp1_gpu)
    #event = prg.init_nappes(queue, (nx * ny, ), None, fnp1_gpu)
    #event = prg.init_tilt(queue, (nx * ny, ), None, fnp1_gpu)
    #event = prg.init_smooth_vortex(queue, (nx * ny, ), None, fnp1_gpu)
    event.wait()
    queue.finish()

    # number of animation frames
    nbplots = 10  # 120
    itermax = int(np.floor(Tmax / dt))
    iterplot = int(itermax / nbplots)
    macrotobedump = [0, 2, 5, 7, 8]  # [0,1,2,3,5,7,8]

    # time loop
    t = 0
    ite = 0
    elapsed = 0.

    fn_cpu = np.zeros((4 * m * ncprx * ncpry, ), dtype=np_real)
    kinetic_cpu = np.zeros((1 * ncprx * ncpry, ), dtype=np_real)
    #divb_cpu = np.zeros((1 * nx * ny, ), dtype=np_real)

    if animate:
        plot_title = r"$n_x = {}, n_y = {}$".format(nx, ny)

        #outputname = "fig/"+testname+str(nx)+"_0.png"
        # plt.savefig(outputname)
        fig = Figure(title=plot_title,
                     levels=np.linspace(0., 12., 64))
        #fig = Figure(title=plot_title)

    print("start OpenCL computations...")
    while t < Tmax + dt:

        #print(itermax, iterplot, ite, " :: ", t)
        ite_title = get_ite_title(ite, t, elapsed)

        # plt.ioff()

        if animate:
            if ite % iterplot == 0:
                if (nx > ncpr):
                    event = prg.compress(
                        queue, (nx*ny, ), None, fn_gpu, fn_cpr_gpu)
                    event.wait()
                    cl.enqueue_copy(queue, fn_cpu, fn_cpr_gpu).wait()

                else:
                    cl.enqueue_copy(queue, fn_cpu, fn_gpu).wait()
                wplot = np.reshape(fn_cpu, (4, m, ncprx, ncpry))
                #event = prg.div_b(queue, (nx * ny, ), None, fn_gpu, divb_gpu).wait()
                #cl.enqueue_copy(queue, divb_cpu, divb_gpu).wait()

                # plt.clf()
                ##plt.imshow(np.sum(wplot, axis = 0),vmin=0, vmax=1)
                # fig.suptitle(ite_title)
                #plt.imshow(np.sum(wplot[:, _ivplot, :, :], axis=0), cmap=cm.jet, extent=[0, Lx, 0, Ly])
                # plt.colorbar()
                #
                ##cset = contour(np.sum(wplot[:, _ivplot, :, :], axis=0),np.arange(-1,1,0.2),linewidths=1, colors='k')
                # clabel(cset,inline=True,fmt='%1.1f',fontsize=10)

                #strm = streamplot(np.arange(0,Lx,dx), np.arange(0,Ly,dy), -np.sum(wplot[:, 7, :, :], axis=0), np.sum(wplot[:, 5, :, :], axis=0), linewidth=1, color='k', arrowstyle='-', density=6)
                #        #start_points=strm_points.T)
                #
                ##plt.imshow(np.reshape(divb_cpu,(nx, ny)))
                # plt.gca().invert_yaxis()
                # plt.colorbar()
                # plt.title(plot_title)
                # plt.clim(0,6)
                #
                # if rcParams['backend'] == "nbAgg":
                #    fig.canvas.draw()
                # else:
                #    plt.pause(1e-6)

                #outputname = "fig/"+testname+str(nx)+"_"+str(ite)+".png"
                # plt.savefig(outputname)

                fig.update(x, y, np.sum(
                    wplot[:, _ivplot, :, :], axis=0), suptitle=ite_title, cb=ite == 0)

        else:
            print(ite_title, end='\r')

        if (savekineticdata or savemacrodata):
            if ite % iterplot == 0:
                event = prg.kinetic(queue, (nx * ny, ), None,
                                    fn_gpu, kinetic_gpu).wait()
                cl.enqueue_copy(queue, kinetic_cpu, kinetic_gpu).wait()
                if (nx > ncpr):
                    event = prg.compress(
                        queue, (nx*ny, ), None, fn_gpu, fn_cpr_gpu)
                    event.wait()
                    cl.enqueue_copy(queue, fn_cpu, fn_cpr_gpu).wait()

                else:
                    cl.enqueue_copy(queue, fn_cpu, fn_gpu).wait()

                filename = testname+str(nx)+"_"+str(ite)
                h5filename = filename+".h5"
                h5outputname = foldername+h5filename

                fdata = h5py.File(h5outputname, 'w')

                fdata.attrs['file_name'] = h5filename
                fdata.attrs['file_time'] = t
                #heavydata = f.create_group('/')

                dataplot = np.reshape(fn_cpu, (4, m, ncprx, ncpry))

                if (savekineticdata):
                    for iv in range(4):
                        for iw in range(m):
                            indic = iw*4 + iv
                            indicname = 'f'+str(indic)
                            fielddata = fdata.create_dataset(
                                indicname, data=dataplot[iv, iw, :, :])

                if (savemacrodata):
                    for iw in macrotobedump:
                        iwdata = np.sum(dataplot[:, iw, :, :], axis=0)
                        indicname = 'w'+str(iw)
                        fielddata = fdata.create_dataset(
                            indicname, data=iwdata)
                    kineticplot = np.reshape(kinetic_cpu, (ncprx, ncpry))
                    indicname = 'kinetic'
                    fielddata = fdata.create_dataset(
                        indicname, data=kineticplot)

                fdata.close()

                xdmffilename = filename + ".xmf"
                xdmfoutputname = foldername + xdmffilename

                ffield = open(xdmfoutputname, 'w')

                ffield.write('<?xml version="1.0" ?> \n')
                ffield.write('<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []> \n')
                ffield.write(
                    '<Xdmf Version="2.0" xmlns:xi="[http://www.w3.org/2001/XInclude]"> \n')
                ffield.write(' <Domain> \n')
                ffield.write('  <Grid Name="TheMesh" GridType="Uniform"> \n')
                nextline = '   <Time Value="'+str(t)+'"/> \n'
                ffield.write(nextline)
                nextline = '   <Topology TopologyType="2DCORECTMesh" Dimensions="' + \
                    str(ncprx)+' '+str(ncpry)+'" /> \n'
                ffield.write(nextline)
                ffield.write('   <Geometry GeometryType="ORIGIN_DXDY" > \n')
                ffield.write(
                    '    <DataItem Name ="Origin" Dimensions="2" NumberType="Float" Format="XML"> \n')
                ffield.write('     0 0 \n')
                ffield.write('    </DataItem> \n')
                ffield.write(
                    '    <DataItem Name ="Spacing" Dimensions="2" NumberType="Float" Format="XML"> \n')
                dcprx = Lx/ncprx
                dcpry = Ly/ncpry
                nextline = '     '+str(dcprx) + ' ' + str(dcpry)+' \n'
                ffield.write(nextline)
                ffield.write('    </DataItem> \n')
                ffield.write('   </Geometry> \n')
                if (savekineticdata):
                    for i in range(4*m):
                        nextline = '   <Attribute Name="f' + \
                            str(i)+'" center="node" > \n'
                        ffield.write(nextline)
                        nextline = '    <DataItem Format="hdf5" NumberType="Float" Dimensions="' + \
                            str(ncprx) + ' ' + str(ncpry) + '"> \n'
                        ffield.write(nextline)
                        nextline = '     '+h5filename+'://f'+str(i)+' \n'
                        ffield.write(nextline)
                        ffield.write('    </DataItem> \n')
                        ffield.write('   </Attribute> \n')
                    for iw in range(m):
                        nextline = '   <Attribute Name="w' + \
                            str(iw)+'" center="node" > \n'
                        ffield.write(nextline)
                        ffield.write('    <DataItem ItemType="Function" \n')
                        ffield.write('     Function="$0+$1+$2+$3" \n')
                        nextline = '     Dimensions="' + \
                            str(ncprx)+' '+str(ncpry)+'"> \n'
                        ffield.write(nextline)
                        for iv in range(4):
                            indic = iw*4+iv
                            nextline = '     <DataItem Format="hdf5" NumberType="Float" Dimensions="' + \
                                str(ncprx)+' '+str(ncpry)+'"> \n'
                            ffield.write(nextline)
                            nextline = '      '+h5filename + \
                                '://f'+str(indic)+' \n'
                            ffield.write(nextline)
                            ffield.write('     </DataItem> \n')
                        ffield.write('    </DataItem> \n')
                        ffield.write('   </Attribute> \n')

                if (savemacrodata):
                    for iw in macrotobedump:
                        nextline = '   <Attribute Name="w' + \
                            str(iw)+'" center="node" > \n'
                        ffield.write(nextline)
                        nextline = '    <DataItem Format="hdf5" NumberType="Float" Dimensions="' + \
                            str(ncprx) + ' ' + str(ncpry) + '"> \n'
                        ffield.write(nextline)
                        nextline = '     '+h5filename+'://w'+str(iw)+' \n'
                        ffield.write(nextline)
                        ffield.write('    </DataItem> \n')
                        ffield.write('   </Attribute> \n')

                    nextline = '   <Attribute Name="kinetic" center="node" > \n'
                    ffield.write(nextline)
                    nextline = '    <DataItem Format="hdf5" NumberType="Float" Dimensions="' + \
                        str(ncprx) + ' ' + str(ncpry) + '"> \n'
                    ffield.write(nextline)
                    nextline = '     '+h5filename+'://kinetic \n'
                    ffield.write(nextline)
                    ffield.write('    </DataItem> \n')
                    ffield.write('   </Attribute> \n')

                ffield.write('  </Grid> \n')
                ffield.write(' </Domain> \n')
                ffield.write('</Xdmf> \n')

                ffield.close()

        t += dt
        #event = prg.time_step(queue, (nx * ny, ), None, wn_gpu, wnp1_gpu)
        event = prg.time_step(queue, (nx * ny, ), None, fn_gpu, fnp1_gpu)
        #event = prg.time_step(queue, (nx * ny, ), None, wn_gpu, wnp1_gpu, wait_for = [event])
        event.wait()
        queue.finish()
        elapsed += 1e-9 * (event.profile.end - event.profile.start)
        # exchange buffer references for avoiding a copy

        #fn_gpu, fnp1_gpu = fnp1_gpu, fn_gpu

        fn_sauv = fnp1_gpu
        fnp1_gpu = fn_gpu
        fn_gpu = fn_sauv

        ite += 1

    # copy OpenCL data to CPU and return the results
    cl.enqueue_copy(queue, fn_cpu, fnp1_gpu).wait()
    queue.finish()

    wplot_gpu = np.reshape(fn_cpu, (4, m, ncprx, ncpry))
    plt.show(block=True)

    return x, y, wplot_gpu


def main(**kwargs):

    # gpu solve
    x, y, wplot_gpu = solve_ocl(**kwargs)


if __name__ == '__main__':
    main()
