# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 15:00:49 2018

@author: drui
"""

import numpy as np
import matplotlib.pyplot as plt
import h5py
import os
import fnmatch
import re
from scipy import stats
from tikzplotlib import save as tikz_save

#from Xdmf import *
#import XdmfReader

eRho = 0
eRhoU = 1
eRhoE = 2
eRhoV = 3
eBy = 4
eBx = 5
ePsi = 6

numberpath = '4096'


def read_data_file(filename, indic):

    f = h5py.File(filename, 'r')

    # List all groups
    #print("Keys: %s" % f.keys())
    a_group_key = list(f.keys())[indic]

    # print(a_group_key)

    # Get the data
    data = list(f[a_group_key])
    datavec = np.asarray(data)

    # print(data[50])

    return datavec


def get_data_time(n, dt):  # filename):
    #reader = XdmfReader.New()
    #dom = XdmfReader.read(filename)
    # print(dom)
    return n*dt


def get_files_list():
    path = 'h5/'
    flist = fnmatch.filter(os.listdir(path), '*.h5')

    return flist


def get_file_number(filename):
    nname = re.search('_(.*).h5', filename)
    n = nname.group(1)

    while (nname != None):
        n = nname.group(1)
        nname = re.search('_(.*)', n)

    return int(n)


def insert_in_list(kinlist, timelist, ekin, time):
    n = np.size(kinlist)
    # print(n)

    j = 0

    while ((j < n) and (time > timelist[j])):
        j += 1

    kinlist = np.insert(kinlist, j, ekin)
    timelist = np.insert(timelist, j, time)

    return kinlist, timelist


def cut_list(lista, listb, bmin, bmax):

    n = np.size(lista)

    j = 0

    while ((j < n) and (listb[j] < bmin)):
        j += 1

    jmin = j

    while ((j < n) and (listb[j] < bmax)):
        j += 1

    jmax = j

    return lista[jmin:jmax], listb[jmin:jmax]


def compute_e_kin(dx, dt):
    flist = get_files_list()
    timelist = []
    kinlist = []
    for name in flist:
        n = get_file_number(name)
        allname = 'h5/'+name
        dataRhoU = read_data_file(allname, eRhoU)
        dataRhoV = read_data_file(allname, eRhoV)
        dataRho = read_data_file(allname, eRho)

        [nvecx, nvecy] = np.shape(dataRhoU)
        nstart = nvecx/2-nvecx/4
        nend = nvecx/2+nvecx/4

        ekinvec = 0.5*(dataRhoU[nstart:nend]*dataRhoU[nstart:nend] +
                       dataRhoV[nstart:nend]*dataRhoV[nstart:nend])/dataRho[nstart:nend]
        ekin = np.sum(ekinvec)*dx*dx
        #ekin = np.max(ekinvec)*dx*dx
        ekin = np.log(ekin)

        time = get_data_time(n, dt)

        # kinlist=np.append(kinlist,[ekin])
        # timelist=np.append(timelist,[time])

        kinlist, timelist = insert_in_list(kinlist, timelist, ekin, time)

    plt.plot(timelist, kinlist, '.')
    # plt.xscale('log')
    # plt.yscale('log')

    plt.grid()

    return kinlist, timelist


def compute_linear_reg(dx, dt):

    kinlist, timelist = compute_e_kin(dx, dt)

    kinlistshort, timelistshort = cut_list(kinlist, timelist, 4.0, 5.0)

    slope, intercept, r_value, p_value, std_err = stats.linregress(
        timelistshort, kinlistshort)
    #slope, base = np.polyfit(timelistshort, kinlistshort, 1)

    timelistplot, timelistplot = cut_list(timelist, timelist, 2.0, 7.0)

    print(r_value, p_value)

    # slopelabel=str(slope)+'x+'+str(intercept)
    slopelabel = "y = %.2f x %.2f" % (slope, intercept)
    plotname = 'kinetic_'+numberpath+'.png'
    tikzname = 'kinetic_'+numberpath+'.tikz'

    plt.plot(timelistshort, kinlistshort, '.', label='ekin(t)')
    plt.plot(timelistplot, slope*timelistplot+intercept, label=slopelabel)
    plt.xlabel('time')
    plt.ylabel('log(kinetic energy)')
    plt.legend(loc='best')
    plt.savefig(plotname)

    tikz_save(tikzname)

    return slope/2.0


def test():
    flist = get_files_list()
    for name in flist:
        n = get_file_number(name)
        print(name, n)
