/*
 * Grid.cpp
 *
 *  Created on: Jul 31, 2017
 *      Author: lukas
 */

#include "Grid.h"

Grid::Grid(int numberOfDirections,int cellsInX,int cellsInY,double lengthInX,double lengthInY,int numConsVar,double defaultRelaxParam) {

	numberOfdirections = numberOfDirections;
	Nx = cellsInX;
	Ny = cellsInY;
	Lx = lengthInX;
	Ly = lengthInY;

	dx = Lx / Nx;
	dy = Ly / Ny;

	if(dx != dy){
		printf("Cells are not Squares check L_y or N_y \n");
		exit(EXIT_FAILURE);
	}

	numberOfConsVar = numConsVar;
	defaultRelaxation = defaultRelaxParam;

	conservativeVariables = new double**[Nx];
	kineticVariables = new double**[Nx];
	relaxationOrderParameter= new double*[Nx];

	for(int i=0; i < Nx; i++){
		conservativeVariables[i] = new double*[Ny];
		kineticVariables[i] = new double*[Ny];
		relaxationOrderParameter[i]= new double[Ny] ;

		for(int j=0; j < Ny; j++){
			relaxationOrderParameter[i][j] = defaultRelaxation;
			conservativeVariables[i][j]= new double [numberOfConsVar];
			kineticVariables[i][j]= new double [numberOfConsVar * numberOfdirections];
		}
	}
	std::cout<<"Grid successfully created"<<std::endl;
	std::cout<<"-> Type: 2D"<<std::endl;
}

double* Grid::get2DPosition(int i,int j){

	double* position = new double[2];
	position[indexDimensionX] = (i) * dx;
	position[indexDimensionY]=  (j) * dy;
	return position;

}

Grid::~Grid(){

	for(int i=0; i < Nx; i++){

		for(int j=0; j < Ny; j++){

			delete[] conservativeVariables[i][j];
			delete[] kineticVariables[i][j];

		}

		delete[] conservativeVariables[i];
		delete[] kineticVariables[i];
		delete[] relaxationOrderParameter[i];

	}

	delete[] conservativeVariables;
	delete[] kineticVariables;
	delete[] relaxationOrderParameter;

}

