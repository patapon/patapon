/*
 * Grid.h
 *
 *  Created on: Jul 31, 2017
 *      Author: lukas
 */

#ifndef GRID_H_
#define GRID_H_
#include <iostream>
#include <iostream>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>

class Grid {

	public:
		static const int indexDimensionX = 0;
		static const int indexDimensionY = 1;
		static const int indexDimensionZ = 2;

		static const int indexDirectionRight = 0;
		static const int indexDirectionLeft = 1;
		static const int indexDirectionUp = 2;
		static const int indexDirectionDown = 3;
		static const int indexDirectionForward= 4;
		static const int indexDirectionBackward = 5;

		int numberOfdirections;
		int Nx, Ny;
		double Lx, Ly;
		double dx, dy;
		double defaultRelaxation;
		double*** conservativeVariables;
		double*** kineticVariables;
		double** relaxationOrderParameter;

		Grid(int NumberOfDirections,int NX,int NY,double LenghtInX,double LengthInY,int NumberOfConsVar,double defaultRelaxParam);
		~Grid();
		double* get2DPosition(int i,int j);

	private:

		int numberOfConsVar;
};

#endif /* GRID_H_ */
