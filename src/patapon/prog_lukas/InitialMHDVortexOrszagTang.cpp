/*
 * InitialVortexOrszagTang.cpp
 *
 *  Created on: Aug 25, 2017
 *      Author: lukas
 */

#include "InitialMHDVortexOrszagTang.h"

#include <cmath>

InitialMHDVortexOrszagTang::InitialMHDVortexOrszagTang(Grid* grid,EquationsMHD* equations,double p,double rho,double BZero):InitialMHD(grid,equations) {

	pressure = p;
	density = rho;
	magneticAmplitude = BZero;
	gamma = equations->Gamma;

	std::cout<<"-> Type: 'Orszag-Tang Vortex'"<<std::endl;
	std::cout<<"-> 2D"<<std::endl;

}

double* InitialMHDVortexOrszagTang::getInitial(double* Position){

	double* wCons = new double[9];

	double u = sin(2* M_PI * Position[1]);
	double v = sin(2* M_PI * Position[0]);
	double w = 0.0;

	double Bx = - magneticAmplitude * sin(2 * M_PI * Position[1]);
	double By = magneticAmplitude * sin(4 * M_PI * Position[0]);
	double Bz = 0.0;

	wCons[indexDensity] = density;
	wCons[indexVelocityX] = density * u;
	wCons[indexVelocityY] = density * v;
	wCons[indexVelocityZ] = density * w;
	wCons[indexEnergy] = pressure / (gamma - 1) +
	density * (u * u + v * v + w * w) / 2 +
	(Bx * Bx + By * By + Bz * Bz) / 2;
	wCons[indexMagneticFieldX] = Bx;
	wCons[indexMagneticFieldY] = By;
	wCons[indexMagneticFieldZ] = Bz;
	wCons[indexDivergenceCleaningPotential] = 0.0;

	return wCons;






}
