/*
 * Solver.h
 *
 *  Created on: Jul 31, 2017
 *      Author: lukas
 */

#ifndef SOLVERMHD_H_
#define SOLVERMHD_H_


#include "Solver.h"
#include "Grid.h"
#include "Boundary.h"
#include <iostream>
#include "EquationsMHD.h"
#include "OutputMHD.h"

class SolverMHD: public Solver {

	public:

		SolverMHD(Grid* grid, EquationsMHD* equations, double CourantFriedrichLevi,double DeltaT, double currentTime);
		void solve(Grid* grid, EquationsMHD* equations, OutputMHD* output,Boundary* boundary, double tMax,double tTillPlot,bool doPlot);
		void crash_check(Grid* grid, EquationsMHD* mhd);
		virtual ~SolverMHD() {};

	protected:
		virtual void setRelaxationParam(Grid* grid, EquationsMHD* equations, Boundary* boundary);

};

#endif /* SOLVERMHD_H_ */
