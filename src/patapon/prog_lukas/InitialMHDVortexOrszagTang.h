/*
 * InitialVortexOrszagTang.h
 *
 *  Created on: Aug 25, 2017
 *      Author: lukas
 */

#ifndef INITIALMHDVORTEXORSZAGTANG_H_
#define INITIALMHDVORTEXORSZAGTANG_H_

#include "Grid.h"
#include <cmath>

#include "InitialMHD.h"

class InitialMHDVortexOrszagTang: public InitialMHD {

	public:

		InitialMHDVortexOrszagTang(Grid* grid,EquationsMHD* equations,double p,double rho,double BZero);
		double* getInitial(double* Position);

	private:

		double gamma;
		double pressure;
		double density;
		double magneticAmplitude;

};

#endif /* INITIALMHDVORTEXORSZAGTANG_H_ */
