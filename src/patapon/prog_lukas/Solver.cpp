/*
 * Solver.cpp
 *
 *  Created on: Oct 4, 2017
 *      Author: lukas
 */

#include "Solver.h"

Solver::Solver(Grid* grid, Equations* equations, double CFL, double deltaT, double initialTime){

	cfl = CFL;
	numberOfConsVar = equations->numberOfConsVar;
	dt = deltaT;
	tNow = initialTime;
	std::cout<<"Solver successfully created"<<std::endl;
}

void Solver::advect(Grid* grid, Boundary* boundary){
	//std::cout<<"Kinetic Variables in Cell 5 before advection: f_1 = "<<grid->kineticVariables[5][0][0]<<"     f_2 = "<<grid->kineticVariables[5][0][1]<<std::endl;
	//std::cout<<"Kinetic Variables in Cell 5:5 before advection: f_1 = "<<grid->kineticVariables[5][5][0]<<"     f_2 = "<<grid->kineticVariables[5][5][1]<<"     f_3 = "<<grid->kineticVariables[5][5][2]<<"     f_4 = "<<grid->kineticVariables[5][5][3]<<std::endl;
	for(int iv = 0; iv < numberOfConsVar; iv++){
        #pragma omp parallel for
		for(int j = 0; j < grid->Ny; j++){
			double kineticDataRightOfDomain = boundary->getKineticDataRightOfDomain(grid,j,iv);
			for(int i = 0; i < grid->Nx - 1; i++){
				grid->kineticVariables[i][j][iv * grid->numberOfdirections + grid->indexDirectionRight] =
					cfl * grid->kineticVariables[i + 1][j][iv * grid->numberOfdirections + grid->indexDirectionRight] + (1-cfl) * grid->kineticVariables[i][j][iv * grid->numberOfdirections + grid->indexDirectionRight];
			}
			grid->kineticVariables[grid->Nx - 1][j][iv * grid->numberOfdirections + grid->indexDirectionRight] =
				cfl * kineticDataRightOfDomain + (1-cfl) *grid->kineticVariables[grid->Nx - 1][j][iv * grid->numberOfdirections + grid->indexDirectionRight];
		}
	}

	for(int iv = 0; iv < numberOfConsVar; iv++){
       #pragma omp parallel for
		for(int j = 0; j < grid->Ny; j++){
			double kineticDataLeftOfDomain = boundary->getKineticDataLeftOfDomain(grid,j,iv);
			for(int i = grid->Nx - 1; i > 0; i--){
				grid->kineticVariables[i][j][iv * grid->numberOfdirections + grid->indexDirectionLeft] =
					cfl * grid->kineticVariables[i - 1][j][iv * grid->numberOfdirections + grid->indexDirectionLeft]+ (1-cfl) *grid->kineticVariables[i][j][iv * grid->numberOfdirections + grid->indexDirectionLeft];
			}
			grid->kineticVariables[0][j][iv * grid->numberOfdirections + grid->indexDirectionLeft] =
				cfl * kineticDataLeftOfDomain+ (1-cfl) *grid->kineticVariables[0][j][iv * grid->numberOfdirections + grid->indexDirectionLeft];
		}
	}

	if(grid->numberOfdirections >= 4){

		for(int iv = 0; iv < numberOfConsVar; iv++){
		#pragma omp parallel for
			for(int i = 0; i < grid->Nx; i++){
				double kineticDataAboveDomain = boundary->getKineticDataAboveDomain(grid,i,iv);
				for(int j = 0; j < grid->Ny - 1; j++){
					grid->kineticVariables[i][j][iv * grid->numberOfdirections + grid->indexDirectionUp] =
						cfl * grid->kineticVariables[i][j + 1][iv * grid->numberOfdirections + grid->indexDirectionUp]+ (1-cfl) *grid->kineticVariables[i][j][iv * grid->numberOfdirections + grid->indexDirectionUp];
				}
				grid->kineticVariables[i][grid->Ny - 1][iv * grid->numberOfdirections + grid->indexDirectionUp] =
					cfl * kineticDataAboveDomain+ (1-cfl) * grid->kineticVariables[i][grid->Ny - 1][iv * grid->numberOfdirections + grid->indexDirectionUp];
			}
		}

		for(int iv = 0; iv < numberOfConsVar; iv++){
		#pragma omp parallel for
			for(int i = 0; i < grid->Nx; i++){
				double kineticDataBelowDomain = boundary->getKineticDataBelowDomain(grid,i,iv);
				for(int j = grid->Ny - 1; j > 0; j--){
					grid->kineticVariables[i][j][iv * grid->numberOfdirections + grid->indexDirectionDown] =
						cfl * grid->kineticVariables[i][j - 1][iv * grid->numberOfdirections + grid->indexDirectionDown]+ (1-cfl) *grid->kineticVariables[i][j][iv * grid->numberOfdirections + grid->indexDirectionDown];
				}
				grid->kineticVariables[i][0][iv * grid->numberOfdirections + grid->indexDirectionDown] =
					cfl * kineticDataBelowDomain+ (1-cfl) *grid->kineticVariables[i][0][iv * grid->numberOfdirections + grid->indexDirectionDown];
			}
		}
	}
	//std::cout<<"Kinetic Variables in Cell 5 after advection: f_1 = "<<grid->kineticVariables[5][0][0]<<"     f_2 = "<<grid->kineticVariables[5][0][1]<<std::endl;
	//std::cout<<"Kinetic Variables in Cell 5:5 after advection: f_1 = "<<grid->kineticVariables[5][5][0]<<"     f_2 = "<<grid->kineticVariables[5][5][1]<<"     f_3 = "<<grid->kineticVariables[5][5][2]<<"     f_4 = "<<grid->kineticVariables[5][5][3]<<std::endl;
}

void Solver::kinetic2Cons(Grid* grid){
	#pragma omp parallel for
	for(int i = 0; i < grid->Nx; i++){
		for(int j = 0; j < grid->Ny; j++){
			for(int iv = 0; iv < numberOfConsVar; iv++){
				grid->conservativeVariables[i][j][iv] = 0.0;
				for(int k = 0; k < grid->numberOfdirections; k++){
					grid->conservativeVariables[i][j][iv] += grid->kineticVariables[i][j][iv * grid->numberOfdirections + k];
				}
			}
		}
	}

}

void Solver::relax(Grid* grid, Equations* equations){

	double* feq = 0;
	//std::cout<<"Kinetic Variables in Cell 5 before relax: f_1 = "<<grid->kineticVariables[5][0][0]<<"     f_2 = "<<grid->kineticVariables[5][0][1]<<std::endl;
	//std::cout<<"Kinetic Variables in Cell 5:5 before relax: f_1 = "<<grid->kineticVariables[5][5][0]<<"     f_2 = "<<grid->kineticVariables[5][5][1]<<"     f_3 = "<<grid->kineticVariables[5][5][2]<<"     f_4 = "<<grid->kineticVariables[5][5][3]<<std::endl;
	for(int i = 0; i < grid->Nx; i++){
		for(int j = 0; j < grid->Ny; j++){
			feq = equations->Cons2EquilibriumFunc(grid,i,j);
			#pragma omp parallel for
			for(int l = 0; l < grid->numberOfdirections * numberOfConsVar; l++){
				grid->kineticVariables[i][j][l] = grid->relaxationOrderParameter[i][j] * feq[l] - (grid->relaxationOrderParameter[i][j] -1.0)* grid->kineticVariables[i][j][l];
			}
			delete[] feq;
			feq = 0;
		}
	}
	//std::cout<<"Kinetic Variables in Cell 5 afterrelax: f_1 = "<<grid->kineticVariables[5][0][0]<<"     f_2 = "<<grid->kineticVariables[5][0][1]<<std::endl;
	//std::cout<<"Kinetic Variables in Cell 5:5 after relax: f_1 = "<<grid->kineticVariables[5][5][0]<<"     f_2 = "<<grid->kineticVariables[5][5][1]<<"     f_3 = "<<grid->kineticVariables[5][5][2]<<"     f_4 = "<<grid->kineticVariables[5][5][3]<<std::endl;

}
