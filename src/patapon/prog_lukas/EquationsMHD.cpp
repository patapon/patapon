#include "EquationsMHD.h"

EquationsMHD::EquationsMHD(int NumConVar,double Lambda,double DCS,double G,double* VN1,double* VN2):Equations(NumConVar,Lambda,VN1,VN2) {

	divergenceCleaningSpeed =DCS;
	Gamma = G;

	std::cout<<"Equations successfully created"<<std::endl;
	std::cout<<"-> Type: Magnetohydrodynamics (MHD)"<<std::endl;

}

double* EquationsMHD::Flux(Grid* grid,double* normalVector,int cellIndexI,int cellIndexJ){

	double* flux = new double[numberOfConsVar];

	double un = grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumX] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] * normalVector[grid->indexDimensionX] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumY] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] * normalVector[grid->indexDimensionY] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumZ] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] * normalVector[grid->indexDimensionZ];

	double bn = grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] * normalVector[grid->indexDimensionX] + grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] * normalVector[grid->indexDimensionY] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ] * normalVector[grid->indexDimensionZ];

	double p = (Gamma - 1.0) *(grid->conservativeVariables[cellIndexI][cellIndexJ][indexEnergy] -  (grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumX] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumX] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumY] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumY] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumZ] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumZ] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity]) / 2.0 -
			(grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] +
	   		grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ]) / 2.0);

	flux[indexDensity] = grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] * un;

	flux[indexMomentumX] = un * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumX] +
			(p + (grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ]) / 2.0) * normalVector[grid->indexDimensionX] -
			bn * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX];

	flux[indexMomentumY] = un * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumY] +
			(p + (grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ]) / 2.0) * normalVector[grid->indexDimensionY] -
			bn * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY];

	flux[indexMomentumZ] = un * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumZ] +
			(p + (grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ]) / 2.0) * normalVector[grid->indexDimensionZ] -
			bn * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ];


	flux[indexEnergy] = (grid->conservativeVariables[cellIndexI][cellIndexJ][indexEnergy] + p + (grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ]) / 2.0) * un -
			(grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumX] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumY] +
			grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ] * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumZ]) / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] * bn;

	flux[indexMagneticFieldX] = -bn * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumX] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] +
			un * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldX] + grid->conservativeVariables[cellIndexI][cellIndexJ][indexDivergenceCleaningPotential] * normalVector[grid->indexDimensionX];

	flux[indexMagneticFieldY] = -bn * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumY] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] +
			un * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldY] + grid->conservativeVariables[cellIndexI][cellIndexJ][indexDivergenceCleaningPotential] * normalVector[grid->indexDimensionY];

	flux[indexMagneticFieldZ] = -bn * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMomentumZ] / grid->conservativeVariables[cellIndexI][cellIndexJ][indexDensity] +
			un * grid->conservativeVariables[cellIndexI][cellIndexJ][indexMagneticFieldZ] + grid->conservativeVariables[cellIndexI][cellIndexJ][indexDivergenceCleaningPotential] * normalVector[grid->indexDimensionZ];

	flux[indexDivergenceCleaningPotential] = divergenceCleaningSpeed * divergenceCleaningSpeed * bn;

	return flux;
}


double EquationsMHD::computeAbsoluteSpeedSquared(double* conservativeVariables){

	double u = conservativeVariables[indexMomentumX]/conservativeVariables[indexDensity];
	double v = conservativeVariables[indexMomentumY]/conservativeVariables[indexDensity];
	double w = conservativeVariables[indexMomentumZ]/conservativeVariables[indexDensity];

	return u*u+v*v+w*w;
}

double EquationsMHD::computeAbsoluteMagneitcFieldSquared(double* conservativeVariables){

	return	conservativeVariables[indexMagneticFieldX]*conservativeVariables[indexMagneticFieldX]+
			conservativeVariables[indexMagneticFieldY]*conservativeVariables[indexMagneticFieldY]+
			conservativeVariables[indexMagneticFieldZ]*conservativeVariables[indexMagneticFieldZ];

}

double EquationsMHD::computeAbsoluteSpeed(double* conservativeVariables){return sqrt(computeAbsoluteSpeedSquared(conservativeVariables));}
double EquationsMHD::computeAbsoluteMagneticField(double* conservativeVariables){return sqrt(computeAbsoluteMagneitcFieldSquared(conservativeVariables));}

double EquationsMHD::computeAbsoluteMomentum(double* conservativeVariables){return computeAbsoluteSpeed(conservativeVariables) * conservativeVariables[indexDensity];}

double EquationsMHD::computePressure(double* conservativeVariables){

	double u = conservativeVariables[indexMomentumX]/conservativeVariables[indexDensity];
	double v = conservativeVariables[indexMomentumY]/conservativeVariables[indexDensity];
	double w = conservativeVariables[indexMomentumZ]/conservativeVariables[indexDensity];
	double absoluteSpeedSquared = u*u+v*v+w*w;
	double absoluteMagneticFieldSquared = conservativeVariables[indexMagneticFieldX]*conservativeVariables[indexMagneticFieldX]+conservativeVariables[indexMagneticFieldY]*conservativeVariables[indexMagneticFieldY]+conservativeVariables[indexMagneticFieldZ]*conservativeVariables[indexMagneticFieldZ];

	return (conservativeVariables[indexEnergy] - (conservativeVariables[indexDensity] * absoluteSpeedSquared/2.0)-(absoluteMagneticFieldSquared/2.0))  *(Gamma -1.0);
}

double EquationsMHD::computeSpeedOfSound(double* conservativeVariables){return sqrt(Gamma * computePressure(conservativeVariables) / conservativeVariables[indexDensity]);}
double EquationsMHD::computeMach(double* conservativeVariables){return computeAbsoluteSpeed(conservativeVariables)/computeSpeedOfSound(conservativeVariables);}

double EquationsMHD::computeCharacteristicVariableEuler1(double* conservativeVariables){return computePressure(conservativeVariables)+ computeAbsoluteMomentum(conservativeVariables) * computeSpeedOfSound(conservativeVariables);}
double EquationsMHD::computeCharacteristicVariableEuler2(double* conservativeVariables){return computePressure(conservativeVariables)- computeAbsoluteMomentum(conservativeVariables) * computeSpeedOfSound(conservativeVariables);}
double EquationsMHD::computeCharacteristicVariableEuler3(double* conservativeVariables){return computePressure(conservativeVariables)- conservativeVariables[indexDensity] * computeSpeedOfSound(conservativeVariables) * computeSpeedOfSound(conservativeVariables);}

double* EquationsMHD::computeCharacteristicVariablesEuler(double* conservativeVariables){

	double* characteristicVariables = new double[3];

	characteristicVariables[0] = computeCharacteristicVariableEuler1(conservativeVariables);
	characteristicVariables[1] = computeCharacteristicVariableEuler2(conservativeVariables);
	characteristicVariables[2] = computeCharacteristicVariableEuler3(conservativeVariables);

	return characteristicVariables;
}

double EquationsMHD::computeEulerEVi(double* conservativeVariables,int MomentumIndex,int i){
	double eigenvalue = 0.0;
	if(i == 1){eigenvalue = computeEulerEV1(conservativeVariables,MomentumIndex);}
	if(i == 2){eigenvalue = computeEulerEV2(conservativeVariables,MomentumIndex);}
	if(i == 3){eigenvalue = computeEulerEV3(conservativeVariables,MomentumIndex);}
	return eigenvalue;
}

double EquationsMHD::computeEulerEV1(double* conservativeVariables,int MomentumIndex){

	return conservativeVariables[MomentumIndex]/conservativeVariables[indexDensity]
				+ computeSpeedOfSound(conservativeVariables);

}

double EquationsMHD::computeEulerEV2(double* conservativeVariables,int MomentumIndex){

	return conservativeVariables[MomentumIndex]/conservativeVariables[indexDensity];

}

double EquationsMHD::computeEulerEV3(double* conservativeVariables,int MomentumIndex){

	return conservativeVariables[MomentumIndex]/conservativeVariables[indexDensity]
				- computeSpeedOfSound(conservativeVariables);

}

double* EquationsMHD::averageConservativeVariables(double* conservativeVariablesLeft, double* conservativeVariablesRight){
	double* averageConsVar = new double[numberOfConsVar];
	for(int i=0;i < numberOfConsVar;i++){
		averageConsVar[i] = (conservativeVariablesLeft[i] + conservativeVariablesRight[i])/2.0;
	}
	return averageConsVar;
}
