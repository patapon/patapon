/*
 * InitialAndExactVortex2DStatonaryMHD.cpp
 *
 *  Created on: Aug 1, 2017
 *      Author: lukas
 */

#include "InitialMHDAndExactVortex2DStatonaryMHD.h"

InitialMHDAndExactVortex2DStatonaryMHD::InitialMHDAndExactVortex2DStatonaryMHD(Grid* grid,EquationsMHD* equations,double Kappa,double Mu):InitialMHDAndExactVortex2D(grid,equations,Kappa,Mu) {

	std::cout<<"-> Moving: no"<<std::endl;
	std::cout<<"-> MHD: yes"<<std::endl;

}

double* InitialMHDAndExactVortex2DStatonaryMHD::getPositionWRTMovement(double* Position,double t){return Position;}
double InitialMHDAndExactVortex2DStatonaryMHD::getAdditionalMovementU(double u){return u;};
double InitialMHDAndExactVortex2DStatonaryMHD::getAdditionalMovementV(double v){return v;};
double InitialMHDAndExactVortex2DStatonaryMHD::getAdditionalMovementW(double w){return w;};
double InitialMHDAndExactVortex2DStatonaryMHD::getDifferentsInMagneticStatusX(double Bx){return Bx;};
double InitialMHDAndExactVortex2DStatonaryMHD::getDifferentsInMagneticStatusY(double By){return By;};
double InitialMHDAndExactVortex2DStatonaryMHD::getDifferentsInMagneticStatusZ(double Bz){return Bz;};
