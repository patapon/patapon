/*
 * Solver.cpp
 *
 *  Created on: Jul 31, 2017
 *      Author: lukas
 */

#include "SolverMHD.h"

#include <stdio.h>
#include <cmath>
#include <stdlib.h>
#include <algorithm>
#include <iostream>


SolverMHD::SolverMHD(Grid* grid, EquationsMHD* equations, double CFL, double deltaT, double initialTime):Solver(grid,static_cast<Equations*>(equations),CFL,deltaT,initialTime){
	std::cout<<"-> Type: SolverMHD"<<std::endl;
}

void SolverMHD::solve(Grid* grid, EquationsMHD* equations, OutputMHD* output, Boundary* boundary, double tMax, double tTillPlot,bool doPlot){

	double time_since_last_plot = 0.0;
	int plotcounter = 0;
	double tEnd = ceil(tMax/dt)*dt;
	printf("tnow=%f dt=%f tmax=%f | progress: %f %c \n",tNow, dt, tMax, 0.0,37);
	if(doPlot == true){
		output->plotCurrentData(grid,equations,tNow);
		printf("->Initial data was successfully plotted \n");
		plotcounter += 1;
	}
	while(tNow < tEnd){
		advect(grid, boundary);
		kinetic2Cons(grid);
		setRelaxationParam(grid,equations,boundary);
		relax(grid, equations);
		tNow += dt;
		printf("tnow=%f dt=%f tmax=%f | progress: %f %c \n",tNow, dt, tMax, tNow/tEnd * 100,37);

		if(doPlot == true){
			time_since_last_plot+=dt;
			if(time_since_last_plot >= tTillPlot){
				output->plotCurrentData(grid,equations,tNow);
				printf("->Current data was successfully plotted \n");
				//crash_check(grid,equations);
				time_since_last_plot -= tTillPlot;
				plotcounter +=1;
			}
		}
	}
	printf("Simulation finished %i plots created \n",plotcounter);
}



void SolverMHD::setRelaxationParam(Grid* grid, EquationsMHD* equations, Boundary* boundary){}

void crash_check(Grid* grid, EquationsMHD* equations){
	double u = 0.0;
	double v = 0.0;
	double w = 0.0;
	double absolutspeed_squared = 0.0;
	double absolutmagneticfield_squared = 0.0;
	double absolutspeed = 0.0;
	double absolutmagneticfield = 0.0;
	double p = 0.0;
	double speedofsound = 0.0;
	double mach_Euler = 0.0;
	bool crashed = false;
	double crashdetection [grid->Nx] [grid->Ny] = {};
	for(int i = 0; i < grid->Nx; i++){
			for(int j = 0; j < grid->Ny; j++){
				u = grid->conservativeVariables[i][j][equations->indexMomentumX]/grid->conservativeVariables[i][j][equations->indexDensity];
				v = grid->conservativeVariables[i][j][equations->indexMomentumY]/grid->conservativeVariables[i][j][equations->indexDensity];
				w = grid->conservativeVariables[i][j][equations->indexMomentumZ]/grid->conservativeVariables[i][j][equations->indexDensity];
				absolutspeed_squared = u*u+v*v+w*w;
				absolutmagneticfield_squared = grid->conservativeVariables[i][j][equations->indexMagneticFieldX]*grid->conservativeVariables[i][j][equations->indexMagneticFieldX]+
						grid->conservativeVariables[i][j][equations->indexMagneticFieldY]*grid->conservativeVariables[i][j][equations->indexMagneticFieldY]+
						grid->conservativeVariables[i][j][equations->indexMagneticFieldZ]*grid->conservativeVariables[i][j][equations->indexMagneticFieldZ];
				absolutspeed = sqrt(absolutspeed_squared);
				absolutmagneticfield = sqrt(absolutmagneticfield_squared);
				p = (grid->conservativeVariables[i][j][equations->indexEnergy] -
						(grid->conservativeVariables[i][j][equations->indexDensity] * absolutspeed_squared/2)-(absolutmagneticfield_squared/2))  *(equations->Gamma -1.0);
				speedofsound = sqrt(equations->Gamma *p /grid->conservativeVariables[i][j][equations->indexDensity]);
				mach_Euler = absolutspeed/speedofsound;
				if(p<0 || absolutspeed != absolutspeed || absolutmagneticfield!= absolutmagneticfield){
					printf("Simulation Crashed:  \n");
					printf("Pressure was %f \n", p);
					printf("Absolutspeed was %f \n", absolutspeed);
					printf("Absolutmagneticfield was %f \n", absolutmagneticfield);
					printf("Speed of sound was %f \n", speedofsound);
					printf("Mach number was %f \n", mach_Euler);
					crashed = true;
					crashdetection [i][j] = 1.0;
				}
			}
	}
	if(crashed == true){
		FILE *crashf;
		crashf = fopen("crash.dat","w");
		for(int i = 0; i < grid->Nx; i++){
			for(int j = 0; j < grid->Ny; j++){
				fprintf(crashf, "%f %f %f \n",
						  (i) * grid->dx,
						  (j) * grid->dy,
						  crashdetection[i][j]);
			}
		}
	fclose(crashf);
	exit(EXIT_FAILURE);
	}
}
