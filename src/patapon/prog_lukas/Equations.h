/*
 * Equations.h
 *
 *  Created on: Oct 4, 2017
 *      Author: lukas
 */

#ifndef EQUATIONS_H_
#define EQUATIONS_H_

#include "Grid.h"

class Equations {

	public:
		Equations(int NumConVar,double Lambda,double* VN1,double* VN2);

		int numberOfConsVar;
		double* normalVector1;
		double* normalVector2;
		double lambda;

		virtual double* Flux(Grid* Grid, double* NormalVector, int CellIndexI, int CellIndexJ) =0;
		double* FluxInDirection1(Grid* Grid, int CellIndexI, int CellIndexJ);
		double* FluxInDirection2(Grid* Grid, int CellIndexI, int CellIndexJ);

		double* Cons2EquilibriumFunc(Grid* Grid, int CellIndexI, int CellIndexJ);
		void computeInitialKineticData(Grid* grid);
		virtual ~Equations() {};


};

#endif /* EQUATIONS_H_ */
