/*
 * Solver.h
 *
 *  Created on: Oct 4, 2017
 *      Author: lukas
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include "Grid.h"
#include "Boundary.h"
#include "Equations.h"

class Solver {

	public:
		Solver(Grid* grid, Equations* equations, double CourantFriedrichLevi,double DeltaT, double currentTime);
		double cfl;
		double tNow;
		double dt;

	protected:
		void advect(Grid* grid,Boundary* boundary);
		void kinetic2Cons(Grid* grid);
		void relax(Grid* grid, Equations* equations);

	private:
		int numberOfConsVar;
};

#endif /* SOLVER_H_ */
