"""PATAPON: A set of two-dimensional PDE solvers using PyOpenCL"""

import argparse
import importlib.util
from pathlib import Path
import sys
import matplotlib

__version__ = '0.0.1'


problems = dict(
    acoustic_m1='acoustic/m1_cl.py',
    acoustic_kinetic='acoustic/kinetic_cl.py',
    mhd_lbm_cl_light='mhd/lbm_cl_light.py',
    mhd_lbm='mhd/mhd_lbm.py',
    mhd_vortex_lbm='mhd/vortex_lbm.py',
    mhd_profile='mhd/profile_lbm_cl_light.py',
    transport='transport/transport_cl.py',
    wave='leapfrog/wave.py'
)


def parse_args(args):
    """
    Parse command line arguments a Namespace for solve_ocl()
    setting a square domain
    """
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('problem', type=str, choices=problems.keys(),
                        help='problem to solve')
    parser.add_argument('-a', '--animate', action='store_true',
                        help='real-time 2D plots')
    parser.add_argument('-sk', '--savekineticdata', action='store_true',
                        help='save kinetic data of simulation')
    parser.add_argument('-sm', '--savemacrodata', action='store_true',
                        help='save macro data of simulation')
    parser.add_argument('-n', '--resolution', type=int,
                        default=256, help='space resolution (nx = ny)')
    parser.add_argument('-L', '--length', type=float,
                        default=1.0, help='domain size (Lx = Ly)')
    parser.add_argument('-t', '--Tmax', type=float, default=1.0,
                        help='final simulation time')
    parser.add_argument('-p', '--precision', type=str, default='single',
                        choices=['single', 'double'],
                        help='floating point precision')
    parser.add_argument('-ps', '--print-source', action='store_true',
                        help='print OpenCL source')
    parser.add_argument('-nd', '--no-display', action='store_true',
                        help='print OpenCL source')
    args = parser.parse_args(args)

    # Prepare to build a square domain with nx = ny = n
    arg_dict = vars(args)
    arg_dict['nx'] = arg_dict['ny'] = arg_dict['resolution']
    del arg_dict['resolution']
    arg_dict['Lx'] = arg_dict['Ly'] = arg_dict['length']
    del arg_dict['length']
    return args


def main():
    args = parse_args(sys.argv[1:])

    module_name = args.problem
    file_path = Path(__file__).resolve().parent / problems[module_name]
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)

    del args.problem

    if args.no_display:
        matplotlib.use('Agg')

    module.main(**vars(args))
