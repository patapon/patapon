#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Resolution of the euler equations by the finite volume method on regular grid
"""


from __future__ import absolute_import, print_function
import h5py
import sys
sys.path.append('..')
from pylab import contour, cm, clabel, streamplot
import matplotlib.pyplot as plt
import matplotlib
from time import sleep
import numpy as np
import pyopencl as cl
from utils import Figure, float2str, load_kernel, parse_cl_args, get_ite_title

# matplotlib.use('Agg')
#from matplotlib import rcParams

prec = {'single': 'float', 'double': 'double'}

# Default values

# number of conservative variables
_m = 3

# number of kinetic variables
_n = 4 * _m

_ivplot = 0

_minplot = 0.99
_maxplot = 2.01

# _minplot = -3
# _maxplot = 3


# grid size
_nx = 1024

_Lx = 16  # 12 #6 #1

_dx = _Lx / _nx


_ny = 256

_Ly = _ny * _dx  # 12 #6 #1

# transport velocity
vel = np.array([1., 1.])

_Tmax = 1.


def solve_ocl(m=_m, n=_n, nx=_nx, ny=_ny, Lx=_Lx, Ly=_Ly, Tmax=_Tmax,
              animate=False, precision="single", savekineticdata="False",
              savemacrodata="False"):



    dx = Lx / nx
    dy = dx

    # lattice speed
    vmax = 2

    # time stepping
    cfl = 1
    dt = cfl * dx / vmax

    # compression for output data
    ncpr = 10000  # 1000 #1024
    testname = "smooth-vortex_"
    #testname = "orszag_"
    #testname = "tilt-p1_"
    foldername = "../h5/"
    ncprx = nx
    ncpry = ny
    # if (nx > ncpr):
    #     ncprx = ncpr
    #     ncpry = ncpr

    # For plotting
    x = np.linspace(0., Lx, num=ncprx)
    y = np.linspace(0., Ly, num=ncpry)

    parameters = {'nx': nx,
                  'ny': ny,
                  'dx': dx,
                  'dy': dy,
                  'dt': dt,
                  'm': m,
                  'n': n,
                  'vx': vel[0],
                  'vy': vel[1],
                  'lambda': vmax,
                  'ncpr': ncpr,
                  }


    np_real, source = load_kernel("euler_kernels.cl", parameters, precision=precision, print_source=False,
                                  module_file=__file__)


    dt = np_real(dt)
    Tmax = np_real(Tmax)

    # OpenCL init
    ctx = cl.create_some_context()
    mf = cl.mem_flags

    #opt = "-cl-denorms-are-zero -cl-fast-relaxed-math"
    opt = []
    
    if precision == "single":
        opt.append("-cl-single-precision-constant")

    print(opt)
    # compile OpenCL C program
    prg = cl.Program(ctx, source).build(options=opt)

    # create a queue (for submitting opencl operations)
    queue = cl.CommandQueue(
        ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)



    # number of animation frames
    nbplots = 10  # 120
    itermax = int(np.floor(Tmax / dt))
    iterplot = int(itermax / nbplots)
    macrotobedump = [0, 2, 5, 7, 8]  # [0,1,2,3,5,7,8]

    # time loop
    t = np_real(0)
    ite = 0
    elapsed = 0.

    fn_cpu = np.zeros((4 * m * ncprx * ncpry, ), dtype=np_real)
    fnp1_cpu = np.zeros((4 * m * ncprx * ncpry, ), dtype=np_real)
    kinetic_cpu = np.zeros((1 * ncprx * ncpry, ), dtype=np_real)

    # create OpenCL buffers
    #buffer_size = 4 * m * nx * ny * np.dtype(np_real).itemsize
    #kinetic_buffer_size = ncprx * ncpry * np.dtype(np_real).itemsize
    fn_gpu = cl.Buffer(ctx, mf.READ_WRITE| mf.COPY_HOST_PTR, hostbuf = fn_cpu)
    fnp1_gpu = cl.Buffer(ctx, mf.READ_WRITE| mf.COPY_HOST_PTR, hostbuf = fnp1_cpu)
    kinetic_gpu = cl.Buffer(ctx, mf.READ_WRITE| mf.COPY_HOST_PTR, hostbuf = kinetic_cpu)
    #fnp1_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    #divb_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)

    # init data
    event = prg.init_sol(queue, (nx * ny, ), None, fn_gpu)
    event.wait()

    #event = prg.init_smooth_vortex(queue, (nx * ny, ), None, fnp1_gpu)
    #event.wait()
    #queue.finish()



    #divb_cpu = np.zeros((1 * nx * ny, ), dtype=np_real)

    if True:
        plot_title = r"$n_x = {}, n_y = {}$".format(nx, ny)

        #outputname = "fig/"+testname+str(nx)+"_0.png"
        # plt.savefig(outputname)
        fig = Figure(title=plot_title,
                     levels=np.linspace(_minplot, _maxplot, 16))
        #fig = Figure(title=plot_title)

    compute = True
    print("Compute:")
    print(compute)

    print("start OpenCL computations...")
    while t < Tmax + dt:

        #print(itermax, iterplot, ite, " :: ", t)
        ite_title = get_ite_title(ite, t, elapsed)

        # plt.ioff()

        if animate:
            if ite % iterplot == 0:
                event = cl.enqueue_copy(queue, fn_cpu, fn_gpu)
                event.wait()
                event = prg.kinetic(queue, (nx * ny, ), None,
                                    t, fn_gpu, kinetic_gpu).wait()
                cl.enqueue_copy(queue, kinetic_cpu, kinetic_gpu).wait()

                wplot = np.reshape(fn_cpu, (4, m, ncprx, ncpry))

                if np.isnan(np.sum(kinetic_cpu)):
                    exit(f"Nan in kinetic_cpu at ite {ite}")
                else:
                    #print("t=",t," L2 norm =", np.sqrt(np.sum(kinetic_cpu)/Lx/Ly))
                    print("t=",t," L1 norm =", np.sum(kinetic_cpu))

                fig.update(x, y, np.sum(
                    wplot[:, _ivplot, :, :], axis=0), suptitle=ite_title, cb=ite == 0)


        else:
            print(ite_title, end='\r')

        t += dt
        #event = prg.time_step(queue, (nx * ny, ), None, wn_gpu, wnp1_gpu)
        if compute :        
            event = prg.time_step(queue, (nx * ny, ), None, fn_gpu, fnp1_gpu)
        else :
            event = prg.time_shift(queue, (nx * ny, ), None, fn_gpu, fnp1_gpu)
        #event = prg.time_step(queue, (nx * ny, ), None, wn_gpu, wnp1_gpu, wait_for = [event])
        event.wait()
        queue.finish()
        elapsed += 1e-9 * (event.profile.end - event.profile.start)
        # exchange buffer references for avoiding a copy

        #cl.enqueue_copy(queue, fn_gpu, fnp1_gpu).wait()

        fn_gpu, fnp1_gpu = fnp1_gpu, fn_gpu
        queue.finish()

        #fn_sauv = fnp1_gpu
        #fnp1_gpu = fn_gpu
        #fn_gpu = fn_sauv

        ite += 1

    # copy OpenCL data to CPU and return the results
    cl.enqueue_copy(queue, fn_cpu, fnp1_gpu).wait()
    queue.finish()
    print("fin")

    wplot_gpu = np.reshape(fn_cpu, (4, m, ncprx, ncpry))
    fig.update(x, y, np.sum(
        wplot_gpu[:, _ivplot, :, :], axis=0), suptitle=ite_title, cb=ite == 0)
    plt.show(block=True)

    return x, y, wplot_gpu


if __name__ == '__main__':

    args = parse_cl_args(n=_nx, L=_Lx, tmax=_Tmax,
                         description='Solve orszag-tang using LBM on PyOpenCL')

    # gpu solve
    x, y, wplot_gpu = solve_ocl(**vars(args))
