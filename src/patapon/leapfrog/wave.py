#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Resolution of a wave equation by the leap frog method
on regular grid
"""

from __future__ import absolute_import, print_function
from patapon.utils import Figure, load_kernel, get_ite_title
import pyopencl as cl
import numpy as np


# grid size
_nx = 128
_ny = 128

_Lx = 1
_Ly = 1

_Tmax = 1.


def solve_ocl(nx=_nx, ny=_ny, Lx=_Lx, Ly=_Ly, Tmax=_Tmax, animate=False,
              precision="single", print_source=False, **kwargs):

    # non periodic !
    dx = Lx / (nx - 1)
    dy = Ly / (ny - 1)

    # periodic
    #dx = Lx / nx
    #dy = Ly / ny

    # For plotting
    x = np.linspace(0., Lx, num=nx)
    y = np.linspace(0., Ly, num=ny)

    # sound speed
    cson = np.sqrt(1.)

    # time stepping
    cfl = 0.4

    dt = cfl * np.sqrt(dx * dx + dy * dy) / cson

    parameters = {'nx': nx,
                  'ny': ny,
                  'dx': dx,
                  'dy': dy,
                  'dt': dt,
                  'cson': cson
                  }
    # loc = locals()
    # parameters = dict(((name, loc[name]) for name in ('nx', 'ny', 'dx', 'dy', 'dt', 'cson')))
    np_real, source = load_kernel(
        "wave_kernels.cl", parameters,
        precision=precision,
        print_source=print_source,
        module_file=__file__)

    # OpenCL init
    ctx = cl.create_some_context()
    mf = cl.mem_flags

    # compile OpenCL C program
    prg = cl.Program(ctx, source).build(options="")

    # set the type of tnow in kernel

    kernel_time_step = prg.time_step
    kernel_time_step.set_scalar_arg_dtypes([np_real, None, None, None])

    # create OpenCL buffers

    # leapfrog data
    buffer_size = nx * ny * np.dtype(np_real).itemsize
    un_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    unp1_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    unm1_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)

    # create a queue (for submitting opencl operations)
    queue = cl.CommandQueue(
        ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # init data
    event = prg.init_sol(queue, (nx * ny, ), None, un_gpu, unm1_gpu)
    event.wait()

    # number of animation frames
    #nbplots = 10
    #itemax = int(np.floor(Tmax / dt))
    #iteplot = int(itemax / nbplots)
    iteplot = 100

    # time loop
    t = 0
    ite = 0
    elapsed = 0.
    un_cpu = np.zeros(nx * ny, dtype=np_real)

    # max isoline value
    max_wave = 1e-3

    if animate:
        fig = Figure(title=r"$n_x = {}, n_y = {}$".format(nx, ny),
                     levels=np.linspace(-max_wave, max_wave, 64))

    print("start OpenCL computations...")

    while t < Tmax:
        t += dt
        # event = prg.apply_bc(queue, (nx * ny, ), None,
        #                     un_gpu, u_west, u_east, u_south, u_north)
        event = kernel_time_step(queue, (nx * ny, ), None,
                                 t, unm1_gpu, un_gpu, unp1_gpu)
        event.wait()
        elapsed += 1e-9 * (event.profile.end - event.profile.start)
        # exchange buffer references for avoiding a copy

        unm1_gpu, un_gpu, unp1_gpu = un_gpu, unp1_gpu, unm1_gpu

        ite_title = get_ite_title(ite, t, elapsed)

        if animate:
            if ite % iteplot == 0:
                #event = prg.div_b(queue, (nx * ny, ), None, fn_gpu, divb_gpu).wait()
                cl.enqueue_copy(queue, un_cpu, un_gpu).wait()
                wplot = np.reshape(un_cpu, (nx, ny))
                wrange = ",   {:f}--{:f}".format(un_cpu.min(), un_cpu.max())
                fig.update(x, y, wplot, suptitle=ite_title +
                           wrange, cb=ite == 0)
        else:
            print(ite_title, end='\r')

        ite += 1

    # copy OpenCL data to CPU and return the results
    cl.enqueue_copy(queue, un_cpu, unp1_gpu).wait()
    queue.finish()

    wplot = np.reshape(un_cpu, (nx, ny))
    return x, y, wplot


def main(**kwargs):

    # gpu solve
    x, y, wplot_gpu = solve_ocl(**kwargs)

    print("\nFinal plot")
    fig = Figure(title=r"$p$")
    fig.update(x, y, wplot_gpu, cb=True, show=True)


if __name__ == '__main__':
    main()
