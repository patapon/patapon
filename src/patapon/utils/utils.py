"""A Figure class to plot animations or final plots"""

import argparse
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
import os

FF = "_F"  # string to be replaced in OpenCL kernel source


class Figure:
    """A 2D colormap figure using contourf"""

    def __init__(self, title='', levels=64, figsize=(10, 8)):
        self.fig = plt.figure(figsize=figsize)
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlabel(r"$x_1$", size=20)
        self.ax.set_ylabel(r"$x_2$", size=20)
        self.ax.tick_params(labelsize=20, size=10, width=2)
        self.ax.set_aspect('equal')
        self.ax.set_title(title)
        self.levels = levels

    def update(self, x, y, wplot, suptitle='', cb=False, show=False):
        self.fig.suptitle(suptitle)
        # Empty old plot before new plot
        for collection in plt.gca().collections:
            plt.gca().collections.remove(collection)
        ctf = self.ax.contourf(x, y, wplot, self.levels)
        if cb:
            cbar = self.fig.colorbar(ctf, format='%.2g')
            cbar.ax.tick_params(labelsize=20, size=10, width=2)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
        if rcParams['backend'] == "nbAgg":
            self.fig.canvas.draw()
        else:
            plt.pause(1e-6)
        if show:
            plt.show()

    @staticmethod
    def show():
        plt.show()


def float2str(real):
    """Return an exponential-formatted string from float"""
    return '{:e}{}'.format(real, FF)


def load_kernel(kernel_filename, parameters, precision="double", print_source=False, module_file='.'):
    """
    Args:
        kernel_filename: OpenCL kernel filename without full path
        parameters (dict): a dictionary of {variable name: value} for processing kernel source
        precision (str): single or double
        print_source (bool): print the actual kernel source
        module_file (str): required to run a main python script from outside its directory
    Returns:
        np_real (numpy type): numpy float according to precision
        source (str): kernel source using defined parameters dictionary

    """

    module_path = os.path.dirname(module_file)
    kernel_path = os.path.join(module_path, kernel_filename)
    print(module_file, kernel_path)
    with open(kernel_path, "r") as f:
        source = f.read()

    for k, v in parameters.items():
        val = float2str(v) if type(v) is not int else v
        source = source.replace('_{}_'.format(k), '({})'.format(val))

    if precision == "double":
        source = source.replace(FF, "")
        source = source.replace("_real_", "double")
        np_real = np.float64
    else:
        print("prec:", precision)
        source = source.replace(FF, "f")
        source = source.replace("_real_", "float")
        np_real = np.float32

    if print_source:
        print(source)

    return np_real, source


def get_ite_title(ite, t, elapsed):
    """Return a formatted string giving iteration inforamtion"""
    return "ite = {}, t = {:f}, elapsed (s) = {:f}".format(ite, t, elapsed)


def parse_cl_args(n=256, L=1.0, tmax=1.0, description="Solve problem"):
    """
    Parse command line arguments to build a dictionary for solve_ocl() setting a square domain
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-a', '--animate', action='store_true',
                        help='real-time 2D plots')
    parser.add_argument('-sk', '--savekineticdata', action='store_true',
                        help='save kinetic data of simulation')
    parser.add_argument('-sm', '--savemacrodata', action='store_true',
                        help='save macro data of simulation')
    parser.add_argument('-n', '--resolution', metavar='nx=ny', type=int,
                        default=n, help='space resolution')
    parser.add_argument('-L', '--length', metavar='Lx=Ly', type=float,
                        default=L, help='Domain size')
    parser.add_argument('-t', '--Tmax', type=float, default=tmax,
                        help='final simulation time')
    parser.add_argument('-p', '--precision', type=str, default='single',
                        choices=['single', 'double'],
                        help='floating point precision')
    args = parser.parse_args()

    # Prepare to build a square domain with nx = ny = n
    arg_dict = vars(args)
    arg_dict['nx'] = arg_dict['ny'] = arg_dict['resolution']
    del (arg_dict['resolution'])
    arg_dict['Lx'] = arg_dict['Ly'] = arg_dict['length']
    del (arg_dict['length'])

    return args
